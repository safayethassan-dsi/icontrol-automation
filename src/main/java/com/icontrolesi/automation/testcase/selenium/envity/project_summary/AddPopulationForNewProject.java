package com.icontrolesi.automation.testcase.selenium.envity.project_summary;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.LeftPanelForProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfo;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALGeneralConfigurationPage;

public class AddPopulationForNewProject extends TestHelper {

	@Test(description = "Add population for a new project")
	public void test_387_AddPopulationForNewProject() {
		checkPopulationAddingWorksForSAL();
		
		softAssert.assertAll();
	} 

	void checkPopulationAddingWorksForSAL(){
		ProjectInfo salInfo = new ProjectInfoLoader("sal").loadSALInfo();

		SALGeneralConfigurationPage.createSALProject(salInfo);;
		
		LeftPanelForProjectPage.gotoTaskQueuePage();
		
		boolean isProjectCreationOk = SALGeneralConfigurationPage.isPorjectCreated();
		
		softAssert.assertTrue(isProjectCreationOk, "Simple CAL project creation FAILED:: ");

		SALGeneralConfigurationPage.addPopulation("1 of 11k");

		boolean isPopulationAdded = SALGeneralConfigurationPage.isPopulationAdded();

		softAssert.assertTrue(isPopulationAdded, "SAL Population Adding FAILED:: ");
	}
}

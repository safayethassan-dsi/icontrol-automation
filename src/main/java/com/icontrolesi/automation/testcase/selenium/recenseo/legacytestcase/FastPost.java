package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;

import com.icontrolesi.automation.platform.util.Logger;


public class FastPost extends TestHelper{
	@Test
	public void test_c1648_FastPost(){
		handleFastPost(driver);
	}


	private void handleFastPost(WebDriver driver){
			new SelectRepo(AppConstant.DEM0);
			
			DocView.openPreferenceWindow();
			DocView.checkAllColumns();
			DocView.clickCrossBtn();
			
			SearchPage.setWorkflow( "Search");
		   
			SearchPage.addSearchCriteria("Relevant");
			
			SearchPage.setSelectBox(By.className("CriterionOperator") , "is not empty");
			
			TagAccordion.open();
			TagAccordion.selectTag( "Hot Docs", false);
			TagAccordion.addSelectedToSearch();
            
            driver.switchTo().defaultContent();
            waitForFrameToLoad( Frame.MAIN_FRAME);
            
            SearchPage.addSearchCriteria("Workflow");
            
            List<WebElement> operators= getElements(By.className("CriterionOperator")); 
            
            new Select(operators.get(2)).selectByVisibleText("in");
            
            new Select(operators.get(3)).selectByVisibleText("Privilege Review");
            
            performSearch();
            
            //SearchPage.waitForElement( "td[aria-describedby='grid_cb']", "css-selector");
            
             //Store three pairs of values for Relevant and Tags
             List<WebElement> relevance= getElements(By.cssSelector("td[aria-describedby='grid_relv']"));
             String[] relv = new String[100];
             relv[0]=relevance.get(0).getText();
             relv[1]=relevance.get(1).getText();
             System.out.println( Logger.getLineNumber() + relv[0]);

             List<WebElement> tags= getElements(By.cssSelector("td[aria-describedby='grid_tags']"));
             String[] tagNames = new String[100];
             tagNames[0]=tags.get(0).getText();
             tagNames[1]=tags.get(1).getText();
             System.out.println( Logger.getLineNumber() + tagNames[0]+"*****");

             //For each doc click the "Document Done" button
             click(SearchPage.waitForElement( "btnDocDone", "id"));
             waitFor( 4);
             
             List<WebElement> docs= getElements(By.cssSelector("td[aria-describedby='grid_cb']"));
             for(int i=1;i<10;i++){
            	 click(docs.get(i));
            	 System.out.println( Logger.getLineNumber() + "Document no.: " + (i + 1) + " clicked.");
            	 waitFor( 10);
            	 waitForVisibilityOf( By.id("btnDocDone"));
            	 getElement(By.id("btnDocDone")).click();
            	 //click(SearchPage.waitForElement( "btnDocDone", "id"));
            	 waitFor( 1);
             }
            
             
             relevance= getElements(By.cssSelector("td[aria-describedby='grid_relv']"));
             
             tags= getElements(By.cssSelector("td[aria-describedby='grid_tags']"));
             
             boolean isTaglistUnchanged = relv[0].equalsIgnoreCase(relevance.get(0).getText()) 
            		 						&& tagNames[0].equalsIgnoreCase(tags.get(0).getText());
             
             
             Assert.assertTrue(isTaglistUnchanged, "(4) Verify that Recenseo has NOT altered any of the Relevant or Tags List data:: ");
     }
}

package com.icontrolesi.automation.testcase.selenium.envity.pagination;

import org.testng.SkipException;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class PreviousButtonWorksProperly extends TestHelper{
	
	@Test
	public void test_c353_PreviousButtonWorksProperly(){
		
		if(ProjectPage.getTotalPageCount() == 1){
			throw new SkipException("Not avaiable pages for performing this test.");
		}
		

		int totalProjectCount = ProjectPage.getTotalProjectCount();
		
		int totalPagesCalculated = totalProjectCount / 5 + (totalProjectCount % 5 == 0 ? 0 : 1);
		int totalPages = ProjectPage.getTotalPageCount();
		
		softAssert.assertEquals(totalPages, totalPagesCalculated);
		
		
		
		
		if(totalProjectCount > 5){
			softAssert.assertEquals(getAttribute(ProjectPage.PREV_LINK, "class"), "paginate_button previous disabled", "*** Previous button disabled at first loading time:: ");
			softAssert.assertEquals(getAttribute(ProjectPage.NEXT_LINK, "class"), "paginate_button next", "*** Next Link works perfectly:: ");
		}else{
			for(int i = 0; i < totalPagesCalculated; i++){
				int totalProjectOnCurrentPage = getTotalElementCount(ProjectPage.projectNameLocator);
				
				if(i == totalPagesCalculated - 1){
					softAssert.assertTrue(totalProjectOnCurrentPage <= 5);
				}else{
					softAssert.assertEquals(totalProjectOnCurrentPage, 5);
				}
				
				tryClick(ProjectPage.NEXT_LINK, 1);
			}
		}
		
		softAssert.assertAll();
	}
}

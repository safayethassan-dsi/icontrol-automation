package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
//import com.google.common.base.Stopwatch;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


/*
 * 
 * 	DB: DEM0

1) Run a search that returns 100K documents (Search for doc id between 1200000 and 1321000 may work well)
2) Global Edit two fields in the documents (Comments and Issues fields work well)
3) Confirm that the edit completes in less than 4 minutes. In client version (on 10/15/2013, this global edit took 2 minutes and 50 seconds).
 * 
 */

public class ActionsGlobalEdit extends TestHelper{
	@Test
	public void test_c177_ActionsGlobalEdit(){
		handleActionsGlobalEdit(driver);
	}
	
	private void handleActionsGlobalEdit(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.setSelectBox(By.id("addList"), "Document ID");
	
		SearchPage.setSelectBox(By.className("CriterionOperator"), "is between");
		
		SearchPage.waitForElement("VALUE-2-1", "id").sendKeys("1200000");
		
		SearchPage.waitForElement("VALUE-2-2", "id").sendKeys("1321000");
		
		performSearch();
		
		DocView.openGlobalEditWindow();
				 
	    selectFromDrowdownByText(By.id("editField"), "Issues (issu)");
		tryClick(By.cssSelector("ul[class^='jstree-container-ul'] > li > a > i"));
		 
		 SearchPage.setSelectBox(By.id("editField"), "Comment (comm)");
		 selectFromDrowdownByText(By.id("editField"), "Comment (comm)");
		 
		 editText(By.name("comm"), "Hello Recenseo");
		
		 tryClick(By.id("nextButton"), 1);
		 
		 tryClick((By.id("confirmOk")), 2);
		 
		 long time = System.nanoTime();
		 long endTime = 0;
		 WebElement editingComplete = getElement(By.cssSelector("span[id='messagePlaceHolder']"));
		 
		 int counter = 1;
		 while (editingComplete.getText().equalsIgnoreCase("Your request has been submitted, please wait...")){
			 waitFor(1);
			 if(counter == 240 )break;
			 counter++;
		 }
		 
		 endTime = System.nanoTime();
		 int timeInSeconds = (int) ((long) (endTime - time) / 1000000000);
		 System.out.println( Logger.getLineNumber() + timeInSeconds);
		 
		 Assert.assertTrue(timeInSeconds < 240, "(3)Confirm that the edit completes in less than 4 minutes:: ");
	 }
}

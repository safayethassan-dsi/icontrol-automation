package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;


/*
 * "DB: DEM4
    (1) While in Search Workflow, run the following Saved Search:
    ztest-dem4_061 - Complicated Logical Group
    (2) Add Criteria for in and out of Issue Review workflow states
    (3) Change your workflow state to Issue Review.
    (4) Run search and confirm Query info (""i"" icon) display shows right number of documents in state.
    (5) Log Out.
    (6) Return and confirm your workflow state was preserved from session to session.
    (7) Confirm folder page correctly counts total documents in current state.
    (8) Open any folder while in that state and confirm Query info (""i"" icon) display shows right number of documents in state.
    (9) Post documents in that workflow state and make sure it moves to the correct next document. Find some folders or searches where consecutive documents are not in the same workflow state and be sure post skips to the next document in the state (not the consecutive doc). If its the last document in the state, it should display a message (note that you should NEVER get that message in search workflow no matter how many you post)."

 *
 *
 */

public class DbdWorkflowStateFunctionality extends TestHelper{
	//String savedSearchName = "ztest-dem4_061 - Complicated Logical Group";
	String savedSearchName = "SavedSearch_Author";
	
    @Test
    public void test_c228_DbdWorkflowStateFunctionality(){
    	//loginToRecenseo();
    	handleDbdWorkflowStateFunctionality(driver);
    }


    protected void handleDbdWorkflowStateFunctionality(WebDriver driver){
    	SoftAssert softAssert = new SoftAssert();
        new SelectRepo(AppConstant.DEM4);

        SearchPage.setWorkflow(driver, "Issue Review");
        //Starting of test
        
       SearchesAccordion.open();
       SearchesAccordion.selectFilter("All");
       SearchesAccordion.selectSavedSearch(savedSearchName);
       //SavedSearchAccordion.selectSavedSearch(savedSearchName);
       SearchesAccordion.addSelectedToSearch();
       
       switchToDefaultFrame();
       waitForFrameToLoad(driver, Frame.MAIN_FRAME);
       SearchPage.addSearchCriteria("Workflow");
       selectFromDrowdownByText(By.cssSelector("div.CriterionContent.ui-helper-reset > select:nth-child(4)"), "Issue Review");
       
       performSearch();
       
       int docCountInGridView = DocView.getDocmentCount();
       
       DocView.openQueryInfo();
       
       int docsInCurrentWorkflowState = Integer.parseInt(getText(By.id("qiTotalWfState")));
       
       softAssert.assertEquals(docCountInGridView, docsInCurrentWorkflowState, "(4) Run search and confirm Query info (\"i\" icon) display shows right number of documents in state:: ");
       
       DocView.closeQueryInfo();
       
       SearchPage.logout();
       performLoginToRecenseo();
       
       new SelectRepo(AppConstant.DEM4);
       
       String currentWorkflow = SearchPage.getCurrentWorkFlow();
       
       softAssert.assertEquals(currentWorkflow, "Issue Review", "(6) Return and confirm your workflow state was preserved from session to session:: ");
       
       /*FolderAccordion.open();
       selectFromDrowdownByText(By.id("folder-showPermission"), "Current Workflow");*/
       
       softAssert.assertAll();
    }
}
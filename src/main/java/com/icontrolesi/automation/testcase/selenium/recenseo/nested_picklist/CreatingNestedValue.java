package com.icontrolesi.automation.testcase.selenium.recenseo.nested_picklist;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.WorkflowFieldManager;

public class CreatingNestedValue extends TestHelper{
	String picklistName = "CreatingNestedValue_c1687";
	String picklistName02 = picklistName + "_2";
	
	@Test
	public void test_c1687_CreatingNestedValue(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		SprocketMenu.openWorkflowAndFieldManager();
		
		WorkflowFieldManager.selectFieldFromFieldSetup("Priv Reason");
		
		WorkflowFieldManager.createParentPicklistItem(picklistName);
		
		hoverOverElement(WorkflowFieldManager.picklistItemLocator);
		
		int totalItemsBefore = getTotalElementCount(WorkflowFieldManager.picklistItemLocator);
		
		List<String> hoveredItems = getListOfItemsFrom(WorkflowFieldManager.hoveredItemLocator, i -> getAttribute(i, "title"));
		
		softAssert.assertEquals(hoveredItems, Arrays.asList("Add Child Item", "Delete"), "2. The user can see a \"+\" icon and a trash icon:: ");
		
		WorkflowFieldManager.createChildPicklistItem(picklistName, picklistName02);
		
		int totalItemAfter = getTotalElementCount(WorkflowFieldManager.picklistItemLocator);
		
		WorkflowFieldManager.deletePicklistItem(picklistName);
		
		softAssert.assertEquals(totalItemAfter, totalItemsBefore + 1, "*** A new nested item is added to the list:: "); 
		
		softAssert.assertAll();
	}
}

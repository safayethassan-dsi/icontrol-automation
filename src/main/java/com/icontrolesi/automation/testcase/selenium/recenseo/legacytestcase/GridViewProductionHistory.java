package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;
/**
 *
 * @author Shaheed Sagar
 *
 *  "DB: DEM0 
 
	1) Run a search where Bates = All Produced 
	2) In Grid view, if the Bates column is not visible, add it using the Show/Hide Column button 
	3) In the Bates column, click the Production History link (Lower case (i) next to the bates information in the bates column) 
	4) Verify the Production History pop-up windows appears and shows the past production information (Prefix, Start, End, Suffix, Production Set, and Date)"
*/

public class GridViewProductionHistory extends TestHelper{
   @Test
	public void test_c172_GridViewProductionHistory(){
		handleGridViewProductionHistory(driver);
	}

    private void handleGridViewProductionHistory(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
    	
    	DocView.openPreferenceWindow();
        DocView.checkSelectedColumns("Bates Number", "Document ID");
        DocView.clickCrossBtn();
         
        SearchPage.setWorkflow("Search");
       
        SearchPage.addSearchCriteria("Bates Number");
        new Select(getElement(By.className("CriterionOperator"))).selectByVisibleText("All Produced");
        
        performSearch();
        
        WebElement bates = Utils.waitForElement(driver, "td[aria-describedby='grid_bates']", "css-selector");
        click(bates.findElement(By.tagName("a")));
        Utils.waitShort();
        WebElement historyElement = Utils.waitForElement(driver, "historyDialog", "id");
        
        Assert.assertTrue(historyElement.isDisplayed(), "4) Verify the Production History pop-up windows appears and shows the past production information (Prefix, Start, End, Suffix, Production Set, and Date)");
    }
}
package com.icontrolesi.automation.testcase.selenium.recenseo.dashboard_gadgets;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class ClosingDownGadgets extends TestHelper{
	private String gadgetName = "RunReportsFromDashboardGadgetsTest"; 
	@Test
	 public void test_c182_ClosingDownGadgets() {
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.createGadgetByName(gadgetName);
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		SearchPage.Gadgets.removeGadgetWithName(gadgetName);
		
		boolean isGadgetRemoved = !SearchPage.isGadgetExistWithName(gadgetName);
		
		SearchPage.logout();
		
		performLoginWithCredential(AppConstant.USER, AppConstant.PASS);
		
		new SelectRepo(AppConstant.DEM0);
		
		boolean isSessionRecored = !SearchPage.isGadgetExistWithName(gadgetName);
		
		softAssert.assertTrue(isGadgetRemoved, "Gadget removed/closed:: ");
		softAssert.assertTrue(isSessionRecored, "The event of closing the Gadget is recored:: ");
		
		softAssert.assertAll();
	 }
}

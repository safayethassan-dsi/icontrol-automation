package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
	public class GridActionGeneralTest extends TestHelper{
	public static final int SORTING_TIME = 50;
	public static final String SORTING_MESSAGE = "Confirm each time that the re-sort works properly(sorting done in an expected amount of time):";

	@Test
	public void test_c1669_GridActionGeneralTest(){
		handleGridActionGeneralTest(driver);
	}

	private void handleGridActionGeneralTest(WebDriver driver){
      	new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		performSearch();
		
        new DocView().clickActionMenuItem("Manage Review Assignments");
        
    	switchToFrame(1);
    	waitFor(5);
    	
    	boolean isCancelBtnWorkedForCreatingNewAssignment = isAdminPanelEditContainersCancelBtnWorked(1);
    	
    	softAssert.assertTrue(isCancelBtnWorkedForCreatingNewAssignment, "3) Verify Cancel button works when creating a new assignment:: ");
    	
    	tryClick(By.cssSelector("li[style$='block;'] > input"));
    	
    	boolean isCancelBtnWorkedForCloningAssignment = isAdminPanelEditContainersCancelBtnWorked(2);
    	
    	softAssert.assertTrue(isCancelBtnWorkedForCloningAssignment, "4) Verify Cancel button works when cloning an assignment:: "); 
    	
    	boolean isCancelBtnWorkedForEditingAssignment = isAdminPanelEditContainersCancelBtnWorked(3);
    	
    	softAssert.assertTrue(isCancelBtnWorkedForEditingAssignment, "5) Verify Cancel button works when editing an assignment:: ");
    	
    	softAssert.assertAll();
	}

	public boolean isAdminPanelEditContainerVisible(){
		boolean isBtnAdminContainerInVisible = getCssValue(By.id("btnAdminContainer"), "display").equals("none");
		boolean isBtnAdminEditContainerVisible = getCssValue(By.id("btnAdminEditContainer"), "display").equals("block");
		
		return isBtnAdminContainerInVisible && isBtnAdminEditContainerVisible;
	}
	
	public boolean isAdminPanelEditContainerInVisible(){
		boolean isBtnAdminContainerInVisible = getCssValue(By.id("btnAdminContainer"), "display").equals("block");
		boolean isBtnAdminEditContainerVisible = getCssValue(By.id("btnAdminEditContainer"), "display").equals("none");
		
		return isBtnAdminContainerInVisible && isBtnAdminEditContainerVisible;
	}
	
	/**
	 * 	@param position refers to the position of Admin panel's button, ie. - 1 for 'New', 2 for 'Clone' and so on...
	 */
	public boolean isAdminPanelEditContainersCancelBtnWorked(int position){
		waitForElementToBeClickable(By.cssSelector("#btnAdminContainer > button:nth-child(" + position + ")"));
		tryClick(By.cssSelector("#btnAdminContainer > button:nth-child(" + position + ")"), 1);
		boolean isEditContainerVisible = isAdminPanelEditContainerVisible();
		tryClick(By.name("btnAdminCancel"), 1);
		boolean isEditContainerInVisible = isAdminPanelEditContainerInVisible();
		
		return isEditContainerInVisible && isEditContainerVisible;
	}
}

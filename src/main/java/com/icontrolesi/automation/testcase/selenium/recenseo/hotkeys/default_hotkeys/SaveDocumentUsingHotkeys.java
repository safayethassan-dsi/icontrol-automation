package com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.default_hotkeys;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.Hotkeys;

public class SaveDocumentUsingHotkeys extends TestHelper{
	@Test
	public void test_c1703_SaveDocumentUsingHotkeys(){
		new SelectRepo(AppConstant.DEM0);
		
		Hotkeys.open();
		Hotkeys.enableHotkeys();
		Hotkeys.setDefaultHotkeys();
		Hotkeys.close();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Author");
		SearchPage.setCriterionValue(0, "jeff");
		
		performSearch();
		
		String previousComment = DocView.getComment();
		
		editText(DocView.commentInCodingTemplate, previousComment + "_EditedCommentForTestCase_c1703.");
		
		Hotkeys.pressHotkey(true, "s");
		
		String commentAfterEditing = DocView.getComment();
		
		editText(DocView.commentInCodingTemplate, previousComment);
		
		Assert.assertEquals(commentAfterEditing, previousComment + "_EditedCommentForTestCase_c1703.", "*** The document has been saved:: ");
	}
}

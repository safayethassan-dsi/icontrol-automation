package com.icontrolesi.automation.testcase.selenium.envity.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CALSimpleGeneralConfigurationPage extends GeneralSettingsForProjectCreationPage{
	public static final By ENTIRE_WORKSPACE_CHOICE_LOCATOR = By.cssSelector("input[value='0'][name='docSelection']");
	public static final By SAVED_SEARCH_CHOICE_LOCATOR = By.cssSelector("input[value='1'][name='docSelection']");
	public static final By SAVED_SEARCHES_DROPDOWN_CONTAINER_LOCATOR = By.id("select2-savedSearches-container");
	public static final By SAVED_SEARCH_FINDER_LOCATOR = By.className("select2-search__field");
	public static final By SAVED_SEARCHES_DROPDOWN_ITEM_LOCATOR = By.cssSelector("#select2-savedSearches-results li");
	
	public static final By STOPPING_POINT_LOCATOR = By.id("stoppingPoint");
	public static final By SYNTHETIC_DOCUMENT_LOCATOR = By.name("docDescription");
	
	public static final By CREATE_PROJECT_BTN_LOCATOR = By.cssSelector("input[value='Start Review']");
	public static final By CANCEL_PROJECT_BTN_LOCATOR = By.id("cancle");
	
	
	public static final CALSimpleGeneralConfigurationPage cscp = new CALSimpleGeneralConfigurationPage();	
	
	public static void selectSavedSearch(String savedSearchName) {
		cscp.tryClick(SAVED_SEARCH_CHOICE_LOCATOR);
		
		cscp.tryClick(SAVED_SEARCHES_DROPDOWN_CONTAINER_LOCATOR);
		cscp.enterText(SAVED_SEARCH_FINDER_LOCATOR, savedSearchName);
		
		List<WebElement> savedSearchList = cscp.getElements(SAVED_SEARCHES_DROPDOWN_ITEM_LOCATOR);
		
		for(WebElement savedSearch : savedSearchList) {
			if(savedSearch.getText().trim().equals(savedSearchName)) {
				savedSearch.click();
				break;
			}
		}
		
		System.out.printf("\nSaved Search with name %s selected...\n", savedSearchName);
	}
	
	public static void createProject(String projectName) {
		ProjectPage.gotoProjectCreationPage("Create Simplified CAL Project");
		populateGeneralConfiguration(projectName, "Envize default connection", "Relativity Instance", "iControl SvP");		
		selectSavedSearch("1k Docs");
		populateFieldSettingsForSAL("dsi_review_field_05", "Very Good", "Very Bad");
		cscp.selectFromDrowdownByIndex(STOPPING_POINT_LOCATOR, 0);
		
		cscp.enterText(SYNTHETIC_DOCUMENT_LOCATOR, "Testing CAL Simple project creation.");
		
		cscp.tryClick(CREATE_PROJECT_BTN_LOCATOR, PROJECT_CREATION_WINDOW);
		cscp.waitForInVisibilityOf(CREATE_PROJECT_BTN_LOCATOR, 300);
		cscp.waitForVisibilityOf(OverviewPage.DASHBOARD_LOCATOR);	
		
		cscp.waitFor(2);
		
		System.out.println("\nCAL Simple project created with name '" + projectName + "'...");
	}

	public static void createProject(CALSmplInfo calSmplInfo) {
		ProjectPage.gotoProjectCreationPage("Create Simplified CAL Project");
		populateGeneralConfiguration(calSmplInfo.getProjectName(), calSmplInfo.getEnvizeInstance(), calSmplInfo.getRelativityInstance(), calSmplInfo.getWorkspace());
		selectSavedSearch(calSmplInfo.getSavedSearchName());
		populateFieldSettingsForSAL(calSmplInfo.getPredictionField(), calSmplInfo.getTruePositive(), calSmplInfo.getTrueNegative());
		cscp.selectFromDrowdownByIndex(STOPPING_POINT_LOCATOR, 0);

		cscp.enterText(SYNTHETIC_DOCUMENT_LOCATOR, calSmplInfo.getSyntheticDocText());

		cscp.tryClick(CREATE_PROJECT_BTN_LOCATOR, PROJECT_CREATION_WINDOW);
		cscp.waitForInVisibilityOf(CREATE_PROJECT_BTN_LOCATOR, 300);
		cscp.waitForVisibilityOf(OverviewPage.DASHBOARD_LOCATOR);

		cscp.waitFor(2);

		System.out.println("\nCAL Simple project created with name '" + calSmplInfo.getProjectName() + "'...");
	}

	public static void cancelProjectCreation() {
		cscp.tryClick(CANCEL_PROJECT_BTN_LOCATOR);
	}
}

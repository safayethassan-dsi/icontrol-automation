package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.selection_summary;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class AddingSingleParameterFromInteractiveDataVisualToSelectedField extends TestHelper{
	@Test
	public void test_c89_AddingSingleParameterFromInteractiveDataVisualToSelectedField(){
		 
		new SelectRepo(AppConstant.DEM0);
		 
		GeneralReport.openFileTypesReport();
		
		String selectionSummaryBefore = getText(GeneralReport.reportSummaryLocator);
		
		softAssert.assertEquals("No selections made", selectionSummaryBefore, "5) .Recenseo displays a contracted (closed) Selection Summary Drawer(Summary Text Before Selection):: ");
		
		List<WebElement> bars = getElements(By.cssSelector(".highcharts-series-group > g rect"));
		int itemPositionToBeSelected;
		
		if(Configuration.getConfig("selenium.browser").equals("ie11")){
			itemPositionToBeSelected = 0;
		}else{
			itemPositionToBeSelected = getRandomNumberInRange(1, bars.size()-1);
		}
		
		String fillColorBeforeSelection = getColorFromElement(bars.get(itemPositionToBeSelected), "fill");
		
		bars.get(itemPositionToBeSelected).click();
		waitFor(2);
		
		String fillColorAfterSelection = getColorFromElement(bars.get(itemPositionToBeSelected), "fill");
		
		softAssert.assertNotEquals(fillColorAfterSelection, fillColorBeforeSelection, "**) Recenseo highlights the selected component(Color has changed):: ");
		softAssert.assertEquals(fillColorAfterSelection, "#AA0000", "***) Recenseo highlights the selected component (Color is Red):: ");
		
		softAssert.assertAll();	
	}
}

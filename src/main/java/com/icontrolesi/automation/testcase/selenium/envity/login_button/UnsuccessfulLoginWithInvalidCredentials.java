package com.icontrolesi.automation.testcase.selenium.envity.login_button;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.EnvitySupport;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class UnsuccessfulLoginWithInvalidCredentials extends TestHelper{
	private final String EXPECTED_ALERT_MSG = "Invalid username or password"; 
	@Test
	public void test_c261_UnsuccessfulLoginWithInvalidCredentials(){
		ProjectPage.performLogout();
		
		inputLoginCredentials(AppConstant.USER, "AnInvalidPassword.");
		
		tryClick(EnvitySupport.loginBtnLocator);
		
		String invalidPasswordMsg = getText(EnvitySupport.loginErrorMsgLocator);
		
		softAssert.assertEquals(invalidPasswordMsg, EXPECTED_ALERT_MSG);
		
		softAssert.assertAll();
	}
}

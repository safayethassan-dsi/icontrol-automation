package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.LoginPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

import com.icontrolesi.automation.platform.util.Logger;
/**
 * 
 * 
 *
 */
public class TeamMembershipVerification extends TestHelper{
	@Test(enabled = false)
	public void test_c1681_TeamMembershipVerification(){
		handleTeamMembershipVerification(driver);
	}
	
	private void handleTeamMembershipVerification(WebDriver driver){
		new SelectRepo(Driver.getDriver(), "Recenseo Demo DB");
		
		SprocketMenu.openUserManager();

		UserManager um= new UserManager();
		UserManager.switchTab("teams");
		Utils.waitShort();
		um.selectTeam("team1");
		
		
		//Remove all users from the loaded team
		um.removeAllUsersFromLoadedTable();
		
		//Close the interface
		um.clickCloseButton();
		
		SprocketMenu.openUserManager();

		//Check for alert message
		try {
	        WebDriverWait wait = new WebDriverWait(driver, 15);
	        wait.until(ExpectedConditions.alertIsPresent());
	        
	        Alert alert = driver.switchTo().alert();
	        
	        System.out.println( Logger.getLineNumber() + alert.getText());
	        
	        if (alert.getText().contains("but have no team assignment yet") && alert.getText().contains("ikamal")){
	        	//System.out.println( Logger.getLineNumber() + "Error message is as expected");
	        	//result.record("(6) Verify Manage Users provides an alert listing the repository users that are not currently part of a team:pass");
	            Logger.log("(6) Verify Manage Users provides an alert listing the repository users that are not currently part of a team:pass");
	        } else{
	        	//result.record("(6) Verify Manage Users provides an alert listing the repository users that are not currently part of a team:fail");
	            Logger.log("(6) Verify Manage Users provides an alert listing the repository users that are not currently part of a team:fail");
	            //result.fail();
	        }
	        
	  	    alert.accept();
	    } catch (Exception e) {
	        //exception handling
	    }
		
		
		//Log out and try to login as ikamal
		SearchPage.logOut(driver);
		
		LoginPage.setUserName("ikamal");
		LoginPage.setPassword("Kamal123");
		
		LoginPage lp= new LoginPage();
		
	    lp.clickOnLoginButton();
	    
	    //Check for error message
	    boolean messagePresent=lp.checkAuthenticationFailure();
	    if(messagePresent){
	    	//result.record("(8) Confirm access to DEM0 is denied because the user is not on a team:pass");
            Logger.log("(8) Confirm access to DEM0 is denied because the user is not on a team:pass");
        }else{
	    	//result.record("(8) Confirm access to DEM0 is denied because the user is not on a team:fail");
            Logger.log("(8) Confirm access to DEM0 is denied because the user is not on a team:fail");
            //result.fail();
        }
	    
	    
	    //ReLogin with iceautotest
		//performLogin();

		SprocketMenu.openUserManager();
		um.selectTeam("team1");
		
		//Assign 
		um.selectUserCaseTable("ikamal", "team1", null, null, 3);
		
		um.clickCloseButton();
		
		//Reopen the User Manager
		SprocketMenu.openUserManager();
		
		try {
	        WebDriverWait wait = new WebDriverWait(driver, 15);
	        wait.until(ExpectedConditions.alertIsPresent());
	        
	       // result.record("(13) Verify that it does NOT present any alerts for users not on a team:fail");
            //Logger.log("(13) Verify that it does NOT present any alerts for users not on a team:fail");
            //result.fail();
            
            
            Alert alert = driver.switchTo().alert();
            
            if (alert.getText().contains("but have no team assignment yet") && !alert.getText().contains("ikamal")){
			        	//System.out.println( Logger.getLineNumber() + "Error message is as expected");
			        	//result.record("(13) Verify that it does NOT present any alerts for users not on a team or if alert present, then the recently added user is not mentioned in the alert:pass");
			            Logger.log("(13) Verify that it does NOT present any alerts for users not on a team or if alert present, then the recently added user is not mentioned in the alert:pass");
			        } else{
			        	//result.record("(13) Verify that it does NOT present any alerts for users not on a team or if alert present, then the recently added user is not mentioned in the alert:fail");
			            Logger.log("(13) Verify that it does NOT present any alerts for users not on a team or if alert present, then the recently added user is not mentioned in the alert:fail");
			            //result.fail();
			        }
         } catch (Exception e) {
	        //exception handling
	    	
	    	//result.record("(13) Verify that it does NOT present any alerts for users not on a team or if alert present, then the recently added user is not mentioned in the alert:pass");
            Logger.log("(13) Verify that it does NOT present any alerts for users not on a team or if alert present, then the recently added user is not mentioned in the alert:pass");
	    }
		
		//Check for alert message
		//Check for alert message
	}	
}

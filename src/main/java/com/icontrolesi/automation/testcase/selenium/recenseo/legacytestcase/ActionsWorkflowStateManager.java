package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;

import com.icontrolesi.automation.platform.util.Logger;

/**
 *
 * @author Shaheed Sagar
 *
 *         "DB: DEM0
 * 
 *         1) On the Search screen, open the folder Email\Foster, Ryan Email. If
 *         all of the documents are not in the current review queue (Workflow
 *         state), add them, before continuing with this test. 2) From Grid View
 *         go to Actions\Workflow state manager 3) In Manage Workflow, REMOVE
 *         all documents from the "privRev" AND the "issueReview"
 *         workflows and click Next 4) Verify the Summary screen reflects the
 *         number of documents and workflow state changes to be made and click
 *         Go 5) Return to Gridview to confirm all documents are removed from
 *         these Workflows. Change Workflows using the top-center drop down menu
 *         to either "privRev" or "issueReview". All documents
 *         should have a grey background and data in all columns should be in
 *         italics. 6) Repeat process of selecting Actions\Workflow state
 *         manager 7) In Manage Workflow, ADD all documents from the ""Privilege
 *         Review"" AND the "issueReview" Workflows and click Next 8) Verify
 *         the Summary screen reflects the number of documents and workflow
 *         state changes to be made and click Go 9) Return to Grid to confirm
 *         all documents have been added to these Workflows Change Workflows
 *         using the top-center drop down menu to either "privRev" or
 *         "issueReview". All documents should have a white background and
 *         the text should not be in italics. "
 */

public class ActionsWorkflowStateManager extends TestHelper{
	String privRev = "Privilege Review";
	String issueReview = "Issue Review";
	
	@Test
	public void test_c180_ActionsWorkflowStateManager(){
		handleActionsWorkflowStateManager(driver);
	}

	private void handleActionsWorkflowStateManager(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		List<String> workFlowList = getListOfItemsFrom(By.cssSelector("#workflows a"), wf -> getText(wf));
		
		if(!workFlowList.stream().anyMatch(wf -> wf.equals("Issue Review"))){
			SprocketMenu.openUserManager();
			UserManager.loadUserSettings(AppConstant.USER);
			UserManager.setWorkflowStateAccess("Issue Review", UserManager.ASSIGN);
			UserManager.setWorkflowStateAccess("Privilege Review", UserManager.ASSIGN);
			SearchPage.goToDashboard();
			SelectRepo.selectRepoFromSecondaryMenu("Recenseo Demonstration Database");
			
			SelectRepo.selectRepoFromSecondaryMenu("Recenseo Demo DB");
		}
		
		switchToDefaultFrame();
		SearchPage.setWorkflow("Search");
		
		FolderAccordion.open();
		FolderAccordion.expandFolder("Email");
		FolderAccordion.selectFolder("Foster, Ryan e-mail");
		FolderAccordion.addSelectedToSearch();
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		performSearch();
		
		System.err.println(getCssValue(DocView.gridRowLocator, "font-style")+"************");
		
		int totalDocCount = DocView.getDocmentCount();
		
		//boolean isDocumentOutOfState = getElements(By.cssSelector("#grid tr[id]")).stream().anyMatch(row -> row.getAttribute("class").endsWith("NotInState"));
		boolean isDocumentOutOfState = DocView.isAllDocumentsOutOfState();
		
		new DocView().clickActionMenuItem("Workflow state manager");
		switchToFrame(1);
		
		List<WebElement> summaryRowList = getElements(DocView.wfsSummaryRowLocator);
		
		Map<String, String> wfsWiseDocCount = new HashMap<>();
		summaryRowList.stream().forEach(row -> {wfsWiseDocCount.put(row.findElement(By.tagName("td")).getText(), row.findElement(By.cssSelector("td:nth-child(2)")).getText());});
		
		if(isDocumentOutOfState){
			DocView.addDocsToWorkflow("Search");
		}
		
		List<String> expectedMsg = new ArrayList<>();
		for(Entry<String, String> entry: wfsWiseDocCount.entrySet()){
			String key = entry.getKey();
			String value = entry.getValue();
			
			if((key.equals(privRev) || key.equals(issueReview)) && !value.equals("0")){
				String msg = "Remove " + value + " documents from " + key;
				expectedMsg.add(msg);
			}
		}
		
		
		if(expectedMsg.isEmpty()){
			System.out.println( Logger.getLineNumber() + "No documents to remove from workflows...");
		}else{
			DocView.selecteRemoveOptionFor(privRev);
			DocView.selecteRemoveOptionFor(issueReview);
			
			tryClick(DocView.nextBtnInWFS, DocView.goBtnInWFS);
			
			List<String> confirmationList = getListOfItemsFrom(By.cssSelector("#confirmList li"), item -> getText(item));
			
			softAssert.assertEquals(confirmationList, expectedMsg, "4. Verify the Summary screen reflects the number of documents and workflow state changes to be made:: ");
			
			tryClick(DocView.goBtnInWFS, DocView.goBackSummaryBtnLocator);
		}
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		List<WebElement> closeBtnList = getElements(By.xpath("//span[text()='Close']"));
		closeBtnList.get(closeBtnList.size() - 1).click();
		waitFor(10);
		
		switchToDefaultFrame();
		SearchPage.setWorkflowFromGridView(privRev);
		
		List<WebElement> documentRowList = getElements(DocView.gridRowLocator);
		documentRowList.remove(0);
		
		//boolean isAllDocumentsRemovedFromSelectedWF = documentRowList.stream().allMatch(row -> row.getAttribute("class").contains("NotInState"));
		
		boolean isAllDocumentsRemovedFromSelectedWF = true;
		for(WebElement row : documentRowList){
			String rowClass = getAttribute(row, "class");
			System.out.println( Logger.getLineNumber() + rowClass + "@@@@@@@@@@@@*");
			if(rowClass.contains("NotInState") == false){
				isAllDocumentsRemovedFromSelectedWF = false;
				break;
			}
		}
		
		softAssert.assertTrue(isAllDocumentsRemovedFromSelectedWF, "5. All documents have been removed from the selected Workflow state (Privilege Review):: ");
		
		switchToDefaultFrame();
		
		SearchPage.setWorkflowFromGridView(issueReview);
		
		List<WebElement> documentRowList2 = getElements(DocView.gridRowLocator);
		documentRowList2.remove(0);
		
		isAllDocumentsRemovedFromSelectedWF = true;
		for(WebElement row : documentRowList2){
			String rowClass = getAttribute(row, "class");
			System.out.println( Logger.getLineNumber() + rowClass + "********************************");
			if(rowClass.contains("NotInState") == false){
				isAllDocumentsRemovedFromSelectedWF = false;
				break;
			}
		}
		
		//isAllDocumentsRemovedFromSelectedWF = documentRowList2.stream().allMatch(row -> row.getAttribute("class").contains("NotInState"));
		
		softAssert.assertTrue(isAllDocumentsRemovedFromSelectedWF, "5. All documents have been removed from the selected Workflow state (Issue Review):: ");
		
		switchToDefaultFrame();
		SearchPage.setWorkflowFromGridView("Search");
		
		summaryRowList.clear();
		summaryRowList.stream().forEach(row -> {wfsWiseDocCount.put(row.findElement(By.tagName("td")).getText(), row.findElement(By.cssSelector("td:nth-child(2)")).getText());});
		
		new DocView().clickActionMenuItem("Workflow state manager");
		List<WebElement> frameList = getElements(By.cssSelector("iframe[src*='javascript']"));
		switchToFrame(frameList.get(frameList.size() - 1));
		
		expectedMsg.clear();
		for(Entry<String, String> entry: wfsWiseDocCount.entrySet()){
			String key = entry.getKey();
			String value = entry.getValue();
			
			if((key.equals(privRev) || key.equals(issueReview)) && !value.equals(totalDocCount+"")){
				String msg = "Remove " + value + " documents from " + key;
				expectedMsg.add(msg);
			}
		}

		
		if(expectedMsg.isEmpty()){
			System.out.println( Logger.getLineNumber() + "No documents to add to workflows...");
		}else{
			DocView.selecteAddOptionFor(privRev);
			DocView.selecteAddOptionFor(issueReview);
			
			tryClick(DocView.nextBtnInWFS, DocView.goBtnInWFS);
			
			List<String> confirmationList = getListOfItemsFrom(By.cssSelector("#confirmList li"), item -> getText(item));
			
			softAssert.assertEquals(confirmationList, expectedMsg, "8. Verify the Summary screen reflects the number of documents and workflow state changes to be made:: ");
			
			tryClick(DocView.goBtnInWFS, DocView.goBackSummaryBtnLocator);
		}
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		closeBtnList = getElements(By.xpath("//span[text()='Close']"));
		closeBtnList.get(closeBtnList.size() - 1).click();
		waitFor(10);
		
		switchToDefaultFrame();
		SearchPage.setWorkflowFromGridView(privRev);
		
		documentRowList = getElements(DocView.gridRowLocator);
		
		documentRowList.remove(0);
		boolean isAllDocumentsAddedToSelectedWF = documentRowList.stream().allMatch(row -> !row.getAttribute("class").contains("NotInState"));
		
		softAssert.assertTrue(isAllDocumentsAddedToSelectedWF, "9. All documents have been added to the selected Workflow state (Privilege Review):: ");
		
		switchToDefaultFrame();
		
		SearchPage.setWorkflowFromGridView(issueReview);
		
		documentRowList = getElements(DocView.gridRowLocator);
		
		documentRowList.remove(0);
		isAllDocumentsAddedToSelectedWF = documentRowList.stream().allMatch(row -> !row.getAttribute("class").contains("NotInState"));
		
		softAssert.assertTrue(isAllDocumentsAddedToSelectedWF, "9. All documents have been added to the selected Workflow state (Issue Review):: ");
		
		softAssert.assertAll();
	}
}

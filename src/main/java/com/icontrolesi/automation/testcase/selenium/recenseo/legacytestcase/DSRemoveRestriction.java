package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageDocumentSecurity;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;
/**
 *
 * @author Shaheed Sagar
 *
 *   "*Will Require Two Users (or two User IDs, an ""Admin"" ID and a ""Test User"" ID) 
	(1) Log into DEM0 and Select Administrative Settings under Settings Menu and Choose Manage Users 
	(2) Click on the Teams tab 
	(3) Confirm the ""Test User"" ID  is still in  ""Document Security"" Team  
	(4) Return to Search Page and Open the EDRM folder in DEM0 DB (193,360 docs)  
	(5) Select ""Manage Document Security"" in the Actions Menu and UnBlock 1000 documents in EDRM folder for ""Document Security"" Team  
	(6) Confirm the ""Test User"" ID can now access the folder but only finds 1000 documents in EDRM folder.  
	(7) Confirm that Folder Details Pop-Up on Search Page only shows 1000 documents for EDRM folder."
 */

public class DSRemoveRestriction extends TestHelper{
   int numberOfUnblockedDocument = 500;
   @Test
   public void test_c399_DSRemoveRestriction(){
	   //loginToRecenseo();
	   handleDSRemoveRestriction(driver);
   }

    private void handleDSRemoveRestriction(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
        
        SprocketMenu.openUserManager();
        UserManager.switchTab("Teams");
        UserManager.selectTeam("Document Security");
        
        boolean isUserExist = UserManager.isUserExists(AppConstant.USER2);
        
        softAssert.assertTrue(isUserExist, "(3) Confirm the \"Test User\" ID  is still in  \"Document Security\" Team:: ");
        
        SearchPage.goToDashboard();
        
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        FolderAccordion.open();
        FolderAccordion.openFolderForView("EDRM");
        
        int totalDocument = DocView.getDocmentCount();
        
        softAssert.assertEquals(totalDocument, 188882, "(4) Return to Search Page and Open the EDRM folder in DEM0 DB (193,360 docs) :: ");
        
        
        DocView.setNumberOfDocumentPerPage(numberOfUnblockedDocument);
        
        DocView docView = new DocView();
        
        docView.clickActionMenuItem("Manage Document Security");
        
        switchToFrame(1);
        
        ManageDocumentSecurity.blockTeam("Document Security");
        ManageDocumentSecurity.closeWindow();
        
        DocView.selectAllDocuments();
        
        docView.clickActionMenuItem("Manage Document Security");
        
        driver.switchTo().frame(getElements(By.cssSelector("iframe[src^='javascript']")).get(1));
        
        ManageDocumentSecurity.unBlockTeam("Document Security");
        
        ManageDocumentSecurity.closeWindow();      
        
        SearchPage.logout();
		
        performLoginWithCredential(AppConstant.USER2, AppConstant.PASS2);	
        
        new SelectRepo(AppConstant.DEM0);
       
        SearchPage.setWorkflow("Search");
        
        FolderAccordion.open();
        FolderAccordion.openFolderInfoDetail("EDRM");
        
        long totalDocumentInFolderInfo = FolderAccordion.getTotaDocCountInFolderInfo();
        
        FolderAccordion.closeFolderInfoDetail();
        
        FolderAccordion.openFolderForView("EDRM");
        
        long totalDocumentFromDocView = DocView.getDocmentCount();
        
        softAssert.assertEquals(totalDocumentFromDocView, numberOfUnblockedDocument, "(6) Confirm the \"Test User\" ID can now access the folder but only finds " + numberOfUnblockedDocument + " documents in EDRM folder:: ");
        softAssert.assertEquals(totalDocumentFromDocView, totalDocumentInFolderInfo, "(7) Confirm that Folder Details Pop-Up on Search Page only shows " + numberOfUnblockedDocument + " documents for EDRM folder:: ");
        
        SearchPage.logout();
        performLoginToRecenseo();
        new SelectRepo(AppConstant.DEM0);
        
        waitForFrameToLoad(Frame.MAIN_FRAME);
        FolderAccordion.open();
        FolderAccordion.openFolderForView("EDRM");
        
        docView.clickActionMenuItem("Manage Document Security");
        driver.switchTo().frame(getElement(By.cssSelector("iframe[src^='javascript']")));
        ManageDocumentSecurity.unBlockTeam("Document Security");
        ManageDocumentSecurity.closeWindow();  
        
        softAssert.assertAll();
    }
}
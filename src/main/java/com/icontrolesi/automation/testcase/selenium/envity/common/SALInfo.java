package com.icontrolesi.automation.testcase.selenium.envity.common;

public class SALInfo implements ProjectInfo{
    //  Optional Fields
    private String projectName = "";
    private String envizeInstance = "";
    private String relativityInstance = "";
    private String workspace = "";

    private String predictionField = "";
    private String truePositive = "";
    private String trueNegative = "";
    private String documentIdentifier = "";

    private String savedSearchName = "";
    private String batchSetName = "";
    private String batchPrefix = "";
    private String batchSize = "";

    private String judgmentalSampleName = "";
    private String savedSearchNameForPopulatioin = "";

    public String getProjectName() {
        return this.projectName;
    }

    public String getEnvizeInstance() {
        return this.envizeInstance;
    }

    public String getRelativityInstance() {
        return this.relativityInstance;
    }

    public String getWorkspace() {
        return this.workspace;
    }

    public String getPredictionField() {
        return this.predictionField;
    }

    public String getTruePositive() {
        return this.truePositive;
    }

    public String getTrueNegative() {
        return this.trueNegative;
    }

    public String getDocumentIdentifier() {
        return this.documentIdentifier;
    }

    public String getSavedSearchName() {
        return this.savedSearchName;
    }

    public String getBatchSetName() {
        return this.batchSetName;
    }

    public String getBatchPrefix() {
        return this.batchPrefix;
    }

    public String getBatchSize() {
        return this.batchSize;
    }

    public String getJudgementalSampleName(){
        return this.judgmentalSampleName;
    }

    public String getSavedSearchNameForPopulatioin(){
        return this.savedSearchNameForPopulatioin;
    }

    SALInfo(SALInfoBuilder sib){
        this.projectName =  sib.projectName;
        this.envizeInstance = sib.envizeInstance;
        this.relativityInstance = sib.relativityInstance;
        this.workspace = sib.workspace;

        this.predictionField =  sib.predictionField;
        this.truePositive = sib.truePositive;
        this.trueNegative = sib.trueNegative;
        this.documentIdentifier = sib.documentIdentifier;

        this.savedSearchName =  sib.savedSearchName;
        this.batchSetName = sib.batchSetName;
        this.batchPrefix = sib.batchPrefix;
        this.batchSize = sib.batchSize;

        this.judgmentalSampleName = sib.judgmentalSampleName;
        this.savedSearchNameForPopulatioin = sib.savedSearchNameForPopulatioin;
    }	

    public static class SALInfoBuilder{
        private String projectName = "";
        private String envizeInstance = "";
        private String relativityInstance = "";
        private String workspace = "";

        private String predictionField = "";
        private String truePositive = "";
        private String trueNegative = "";
        private String documentIdentifier = "";

        private String savedSearchName = "";
        private String batchSetName = "";
        private String batchPrefix = "";
        private String batchSize = "";
    
        private String judgmentalSampleName = "";
        private String savedSearchNameForPopulatioin = "";


        public SALInfoBuilder setProjectName(String projectName) {
            this.projectName = projectName;
            return this;
        }


        public SALInfoBuilder setEnvizeInstance(String envizeInstance) {
            this.envizeInstance = envizeInstance;
            return this;
        }

        public SALInfoBuilder setRelativityInstance(String relativityInstance) {
            this.relativityInstance = relativityInstance;
            return this;
        }


        public SALInfoBuilder setWorkspace(String workspace) {
            this.workspace = workspace;
            return this;
        }


        public SALInfoBuilder setPredictionField(String predictionField) {
            this.predictionField = predictionField;
            return this;
        }


        public SALInfoBuilder setTruePositive(String truePositive) {
            this.truePositive = truePositive;
            return this;
        }


        public SALInfoBuilder setTrueNegative(String trueNegative) {
            this.trueNegative = trueNegative;
            return this;
        }


        public SALInfoBuilder setDocumentIdentifier(String documentIdentifier) {
            this.documentIdentifier = documentIdentifier;
            return this;
        }

        public SALInfoBuilder setSavedSearchName(String savedSearchName) {
            this.savedSearchName = savedSearchName;
            return this;
        }


        public SALInfoBuilder setBatchSetName(String batchSetName) {
            this.batchSetName = batchSetName;
            return this;
        }


        public SALInfoBuilder setBatchPrefix(String batchPrefix) {
            this.batchPrefix = batchPrefix;
            return this;
        }


        public SALInfoBuilder setBatchSize(String batchSize) {
            this.batchSize = batchSize;
            return this;
        }

        public SALInfoBuilder setJudgmentalSampleName(String judgmentalSampleName) {
            this.judgmentalSampleName = judgmentalSampleName;
            return this;
        }

        public SALInfoBuilder setSavedSearchNameForPopulatioin(String savedSearchNameForPopulatioin){
            this.savedSearchNameForPopulatioin = savedSearchNameForPopulatioin;
            return this;
        }

        public SALInfo build(){
            return new SALInfo(this);
        }
    }
}
package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.LoginPage;

import com.icontrolesi.automation.platform.util.Logger;


/**
*
* @author Shaheed Sagar
*
*   (1) Open the test environment's Recenseo login page in Browser 
	(2) Enter your id/password and hit the "Login" button 
	(3) Verify Recenseo displays an "Authenticating.." message, that the Login button is disabled, and that hitting it during the authentication process does absolutely nothing 
	(4) Check to make sure the Repositories page lists only Repositories that you have access to (note this only works in client or on terminal servers, cannot be tested in floor or ictomcattest1)
*/
public class AuthenticateDbAccess extends TestHelper{

	@Test(enabled = false)
	public void test_c402_AuthenticateDbAccess(){
		landOnLoginPage();
		handleAuthenticateDbAccess(driver);
	}
	

	private void handleAuthenticateDbAccess(WebDriver driver){
		LoginPage.setUserName(Configuration.getConfig("recenseo.username"));

		LoginPage.setPassword(Configuration.getConfig("recenseo.password"));
			
		try {
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			waitFor(driver, 1);
			robot.keyPress(KeyEvent.VK_ESCAPE);
			robot.keyRelease(KeyEvent.VK_ESCAPE);
			System.out.println( Logger.getLineNumber() + "Escape key pressed...");
		} catch (AWTException e) {
			e.printStackTrace();
		}
		
		String waitMsg = getText(By.id("waitMsg"));
		
		System.out.println( Logger.getLineNumber() + waitMsg+"****");
		
		Assert.assertEquals(waitMsg, "Authenticating, please wait...", "(3) Verify Recenseo displays an \"Authenticating, please wait...\" message, that the Login button is disabled, and that hitting it during the authentication process does absolutely nothing:: ");
		
		driver.get(Configuration.getConfig("recenseo.url"));
		System.out.println( Logger.getLineNumber() + Configuration.getConfig("recenseo.url"));
		
		driver.switchTo().defaultContent();
		waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		List<WebElement> descriptionElements = getElements(By.className("description"));
		List<String> checkStrings = new ArrayList<>();
		checkStrings.add("Development Testing DB");
		checkStrings.add("Recenseo Demo DB");
		checkStrings.add("Recenseo Demonstration Database 2.0");
		
		boolean correct = true;
		for(WebElement e : descriptionElements){
			if(!checkStrings.contains(e.getText())){
				correct = false;
			}
		}
		
		Assert.assertTrue(correct, "(4) Check to make sure the Repositories page lists only Repositories that you have access to:: ");
	}
}

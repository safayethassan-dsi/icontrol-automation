package com.icontrolesi.automation.testcase.selenium.envity.pagination;

import org.testng.SkipException;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;

public class PreviousButtonDisabledOnFirstPage extends TestHelper{
	
	@Test
	public void test_c355_PreviousButtonDisabledOnFirstPage(){
		String urlBeforeClick = Driver.getDriver().getCurrentUrl();

		if(ProjectPage.getTotalPageCount() == 1){
			throw new SkipException("Not avaiable pages for performing this test.");
		}
				
		boolean isPreviousBtnEnabled = ProjectPage.isPreviousBtnEnabled();
		
		tryClick(ProjectPage.PREV_LINK);
		
		String urlAfterClick = Driver.getDriver().getCurrentUrl();
		
		softAssert.assertFalse(isPreviousBtnEnabled, "Previous button disabled (UI):: ");
		softAssert.assertTrue(urlBeforeClick.endsWith("landing"), "User stays on the first page:: ");
		softAssert.assertTrue(urlAfterClick.endsWith("landing"), "User stays on the first page after clicking on Previous button:: ");
		
		softAssert.assertAll();
	}
}

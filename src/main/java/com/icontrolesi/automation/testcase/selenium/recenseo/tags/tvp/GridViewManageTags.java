package com.icontrolesi.automation.testcase.selenium.recenseo.tags.tvp;

import java.util.Calendar;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

public class GridViewManageTags extends TestHelper{
	String errorMsgFormat = "The requested edit is not valid. One or more subtags of \"%s\" has security settings broader than \"%s.\"";
	String errorMsgForEditingWithBroaderSecurity = "The requested edit is not valid. The parent of the selected tag \"%s\" has security settings narrower than \"%s.\"";
	String errorMsgForCreatingWithBroaderSecurity = "Recenseo cannot create this tag: The sub tag cannot have a broader security setting \"%s\" than its parent \"%s.\"";
	
	String step3d = "3d. Verify an error message appears that states the edit is not valid and that the parent tag has security settings broader than the chosen accessibility level (%s):: ";
	String step4d = "4d. Verify an error message appears that states the edit is not valid and that the parent of the selected subtag has security settings narrower than the chosen accessibility level (%s):: ";
	String step5c = "5c. Verify an error message appears that states the edit is not valid and that the subtag cannot have broader security settings than its parent (%s):: ";
	
		
	@Test
	public void test_c1783_GridViewManageTags(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
	
		FolderAccordion.open();
		FolderAccordion.openFolderForView("Email");
		
		TagManagerInGridView.open();
		
		testEditingParentTagWithNarrowerSecurity();
		testEditingSubTagWithBroaderSecurity();
		testCreatingSubTagWithNarrowerSecurity();
		
		softAssert.assertAll();		
	}
	
	void openUpAndFillWithNewData(String oldTagName, String newTagName, String newPermission){
		TagManagerInGridView.openUpEditWindowFor(oldTagName);
		TagManagerInGridView.fillUpWithNewData(oldTagName, newTagName, newPermission);
	}
	
	void openUpAndFillSubtagWithNewData(String parentTagName, String newSubtagName, String subTagPermission){
		TagManagerInGridView.openUpSubtagCreationWindowFor(parentTagName);
		TagManagerInGridView.fillUpWithNewData(newSubtagName, newSubtagName, subTagPermission);
	}
	
	void testEditingParentTagWithNarrowerSecurity(){
		long timeStamp = Calendar.getInstance().getTimeInMillis();
		
		String topLevelTagName = "GrdiViewManageTags_1783_Top_" + timeStamp;
		String subTagName = "GrdiViewManageTags_1783_Sub_" + timeStamp;

		TagManagerInGridView.createTopLevelTag(topLevelTagName, "All");
		TagManagerInGridView.createSubTagFor(topLevelTagName, subTagName, "All");
		
		openUpAndFillWithNewData(topLevelTagName, topLevelTagName, "Internal");
		
		tryClick(TagManagerInGridView.updateBtnLocator);
		
		String errorMsgForInternal = getAlertMsg();
		
		softAssert.assertEquals(errorMsgForInternal, String.format(errorMsgFormat, topLevelTagName, "Internal"), String.format(step3d, "Internal"));	
		
		openUpAndFillWithNewData(topLevelTagName, topLevelTagName, "Mine");
		
		tryClick(TagManagerInGridView.updateBtnLocator);
		
		String errorMsgForMine = getAlertMsg();
		
		softAssert.assertEquals(errorMsgForMine, String.format(errorMsgFormat, topLevelTagName, "Mine"), String.format(step3d, "Mine"));	
		
		
		openUpAndFillWithNewData(topLevelTagName, topLevelTagName, "Team");
		
		tryClick(TagManagerInGridView.updateBtnLocator);
		
		String errorMsgForTeam = getAlertMsg();
		
		softAssert.assertEquals(errorMsgForTeam, String.format(errorMsgFormat, topLevelTagName, "Team"), String.format(step3d, "Team"));	
		
		TagManagerInGridView.deleteTag(topLevelTagName);
	}
	
	void testEditingSubTagWithBroaderSecurity(){
		long timeStamp = Calendar.getInstance().getTimeInMillis();
		
		String topLevelTagName = "GrdiViewManageTags_1783_Top_" + timeStamp;
		String subTagName = "GrdiViewManageTags_1783_Sub_" + timeStamp;

		TagManagerInGridView.createTopLevelTag(topLevelTagName, "Mine");
		TagManagerInGridView.createSubTagFor(topLevelTagName, subTagName, "Mine");
		
		TagManagerInGridView.openUpEditWindowFor(subTagName);
		TagManagerInGridView.fillUpWithNewData(subTagName, subTagName, "Team");
		
		tryClick(TagManagerInGridView.updateBtnLocator);
		
		String errorMsgForTeam = getAlertMsg();
		
		softAssert.assertEquals(errorMsgForTeam, String.format(errorMsgForEditingWithBroaderSecurity, subTagName, "Team"), String.format(step4d, "Team"));	
		
		TagManagerInGridView.openUpEditWindowFor(subTagName);
		TagManagerInGridView.fillUpWithNewData(subTagName, subTagName, "All");
		
		tryClick(TagManagerInGridView.updateBtnLocator);
		
		String errorMsgForAll = getAlertMsg();
		
		softAssert.assertEquals(errorMsgForAll, String.format(errorMsgForEditingWithBroaderSecurity, subTagName, "All"), String.format(step4d, "All"));	
		
		/*openUpAndFillWithNewData(topLevelTagName, subTagName, "Internal");
		
		tryClick(TagManagerInGridView.updateBtnLocator);
		
		String errorMsgForTeam = getAlertMsg();
		
		softAssert.assertEquals(errorMsgForTeam, String.format(errorMsgFormat, subTagName, "Internal"));	*/
		
		TagManagerInGridView.deleteTag(topLevelTagName);
	}
	
	void testCreatingSubTagWithNarrowerSecurity(){
		long timeStamp = Calendar.getInstance().getTimeInMillis();
		
		String topLevelTagName = "GrdiViewManageTags_1783_Top_" + timeStamp;
		String subTagName = "GrdiViewManageTags_1783_Sub_" + timeStamp;

		TagManagerInGridView.createTopLevelTag(topLevelTagName, "Mine");
		
		openUpAndFillSubtagWithNewData(topLevelTagName, subTagName, "Team");
		
		tryClick(TagManagerInGridView.createBtnLocator);
		
		String errorMsgForInternal = getAlertMsg();
		
		softAssert.assertEquals(errorMsgForInternal, String.format(errorMsgForCreatingWithBroaderSecurity, "Team", "Mine"), String.format(step5c, "Team"));	
		
		openUpAndFillSubtagWithNewData(topLevelTagName, subTagName, "All");
		
		tryClick(TagManagerInGridView.createBtnLocator);

		String errorMsgForMine = getAlertMsg();
		
		softAssert.assertEquals(errorMsgForMine, String.format(errorMsgForCreatingWithBroaderSecurity, "All", "Mine"), String.format(step5c, "All"));	
		
		/*openUpAndFillSubtagWithNewData(subTagName, subTagName, "Internal");
		
		tryClick(TagManagerInGridView.updateBtnLocator);
		
		String errorMsgForTeam = getAlertMsg();
		
		softAssert.assertEquals(errorMsgForTeam, String.format(errorMsgFormat, "Internal", topLevelTagName));*/	
		
		TagManagerInGridView.deleteTag(topLevelTagName);
	}
}

package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;
/**
 * 
 * @author Shaheed Sagar
 * "(1) Log into DEM0 and Select Administrative Settings under Settings Menu and Choose Manage Users 
	(2) On the USER tab, Select 5-10 users that ARE on the same team, and click "Load Settings." 
	(3) Verify Manage Users populates the "Team" selection on the right-hand side with the correct team, then click "Cancel" 
	(4) Select 5-10 users that ARE NOT on the same team 
	(5) Verify Manage Users does NOT populate a team selection on the right-hand side 
	(6) Select 5 users with varied access settings (may be able to use previous users), and click "Load Settings" 
	(7) Use the legend and color-coding diplayed to identify a General Access AND Workflow State access for each of the following situations (make note of the situations): 
	(a) None of the selected users have the access (Section will remain white) 
	(b) All of the selected users have the access (Section will be green) 
	(c) Some of the selected users have the access (Section will be yellow) 
	(d) Verify that the noted representations are correct by clicking on the "Info" button (upper-right "i" icon).  Select each user in the "User" drop down below the "Selected Users" table, and visually inspect the "General Access" table and "Workflow State Access" for the specific situations noted in (a) - (c), making note of whether the specific user has the access or not. 
	(e) Click Back and verify Recenseo takes you back to the Manage Users page. 
	(8) Click "Cancel"
 *
 */
public class MULoadAccessSettings extends TestHelper{
	@Test(enabled = false)
	public void test_c1673_MULoadAccessSettings(){
		handleMULoadAccessSettings(driver);
	}

	private void handleMULoadAccessSettings(WebDriver driver){
		Driver.setWebDriver(driver);
		
		SearchPage sp = new SearchPage();

		new SelectRepo(Driver.getDriver(), "Recenseo Demo DB");

		UserManager userManager = new UserManager();
		
		SprocketMenu sprocketMenu = new SprocketMenu();
		//sprocketMenu.selectSubMenu(Driver.getDriver(), sp, "Tools", "User Manager");
		
		userManager.checkUser("aself");
		userManager.checkUser("azaman");
		userManager.checkUser("krahman");
		userManager.checkUser("aawal");
		userManager.checkUser("iceautotest2");
		userManager.checkUser("iceautotest3");
		userManager.clickLoadSettings();
		
		String valueInBox = sp.checkValueOfSelectBox(Driver.getDriver(), "teamSelect");
		
		/*if(valueInBox.equalsIgnoreCase("DEM0 Admins")){
			Utils.handleResult("(3) Verify Manage Users populates the 'Team' selection on the right-hand side with the correct team, then click 'Cancel'", result, true);
		}else{
			Utils.handleResult("(3) Verify Manage Users populates the 'Team' selection on the right-hand side with the correct team, then click 'Cancel'", result, false);
		}*/
		
		userManager.clickCancelButton();
		
		userManager.checkUser("aself");
		userManager.checkUser("cjjtest3");
		userManager.checkUser("CDRtest4");
		userManager.checkUser("mdtest");
		userManager.checkUser("cisaac");
		userManager.checkUser("jjohnson");
		userManager.clickLoadSettings();
		
		valueInBox = sp.checkValueOfSelectBox(Driver.getDriver(), "teamSelect");
		/*if(valueInBox.equalsIgnoreCase("")){
			Utils.handleResult("(5) Verify Manage Users does NOT populate a team selection on the right-hand side", result, true);
		}else{
			Utils.handleResult("(5) Verify Manage Users does NOT populate a team selection on the right-hand side", result, false);
		}*/
		
		userManager.clickCancelButton();
		
		userManager.checkUser("cisaac");
		userManager.checkUser("cburgess");
		userManager.checkUser("mwalker");
		userManager.checkUser("CDRtest4");
		userManager.checkUser("mdtest");
		userManager.checkUser("cisaac");
		userManager.checkUser("jjohnson");
		userManager.clickLoadSettings();
		
		List<WebElement> tdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='generalAccesses_access']"));
		WebElement greenElement = tdElements.get(0);
		WebElement yellowElement = tdElements.get(1);
		WebElement whiteElement = tdElements.get(5);
		
		//List<WebElement> WtdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='workflowStateAccesses_no_access']"));
		WebElement WgreenElement = tdElements.get(3);
		WebElement WyellowElement = tdElements.get(1);
		WebElement WwhiteElement = tdElements.get(0);
		
		/*if(greenElement.getAttribute("class").equalsIgnoreCase("accessByAll") && yellowElement.getAttribute("class").equalsIgnoreCase("accessBySome") && whiteElement.getAttribute("class").equalsIgnoreCase("accessByNone") && WgreenElement.getAttribute("class").equalsIgnoreCase("accessByAll") && WyellowElement.getAttribute("class").equalsIgnoreCase("accessBySome") && WwhiteElement.getAttribute("class").equalsIgnoreCase("accessByNone")){
			Utils.handleResult("(7) Use the legend and color-coding diplayed to identify a General Access AND Workflow State access for each of the following situations (make note of the situations):(a) None of the selected users have the access (Section will remain white)(b) All of the selected users have the access (Section will be green)(c) Some of the selected users have the access (Section will be yellow)", result, true);
		}else{
			Utils.handleResult("(7) Use the legend and color-coding diplayed to identify a General Access AND Workflow State access for each of the following situations (make note of the situations):(a) None of the selected users have the access (Section will remain white)(b) All of the selected users have the access (Section will be green)(c) Some of the selected users have the access (Section will be yellow)", result, false);
		}*/
		
		userManager.clickInfoButton();
		
		//7d running
		//WtdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='generalAccessesForUpdateOriginal_access']"));
		WgreenElement = tdElements.get(0);
		WwhiteElement = tdElements.get(5);
		//WtdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='workflowStateAccessesForUpdateOriginal_access']"));
		WebElement workflowGreenElement = tdElements.get(0);
		/*if(greenElement.getAttribute("class").equalsIgnoreCase("accessByAll") && yellowElement.getAttribute("class").equalsIgnoreCase("accessBySome") && whiteElement.getAttribute("class").equalsIgnoreCase("accessByNone") && WgreenElement.getAttribute("class").equalsIgnoreCase("accessByAll") && WwhiteElement.getAttribute("class").equalsIgnoreCase("accessByNone") && workflowGreenElement.getAttribute("class").equalsIgnoreCase("accessByAll")){
			Utils.handleResult("(7)(d) Verify that the noted representations are correct by clicking on the 'Info' button (upper-right 'i' icon). Select each user in the 'User' drop down below the 'Selected Users' table, and visually inspect the 'General Access' table and 'Workflow State Access' for the specific situations noted in (a) - (c), making note of whether the specific user has the access or not.", result, true);
		}else{
			Utils.handleResult("(7)(d) Verify that the noted representations are correct by clicking on the 'Info' button (upper-right 'i' icon). Select each user in the 'User' drop down below the 'Selected Users' table, and visually inspect the 'General Access' table and 'Workflow State Access' for the specific situations noted in (a) - (c), making note of whether the specific user has the access or not.", result, false);
		}*/
		
		userManager.clickBackButton();
		
		try{
			//WebElement el = Driver.getDriver().findElement(By.id("gs_userID"));
			//Utils.handleResult("(e) Click Back and verify Recenseo takes you back to the Manage Users page.", result, true);
		}catch(NoSuchElementException e){
			//Utils.handleResult("(e) Click Back and verify Recenseo takes you back to the Manage Users page.", result, false);
		}
		
		userManager.clickCancelButton();
	}
}

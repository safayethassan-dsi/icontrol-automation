package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

import com.icontrolesi.automation.platform.util.Logger;

/**
 *
 * @author Shaheed Sagar
 *
 *  "DB: DEM0  
	(1) Open iControl_032 - Complicated Saved search in DEM0.  
	(2) Select the two addressee criteria in the middle group, right click, and click Create Search Group  
	(3) Make sure context menu displays with Create Search Group item on right click, when more than one criteria selected.  
	(4) Make sure the new group is placed before the 3rd group  
	(5) Remove the two criteria from the third group 
	(6) Make sure the third group is removed 
	"
	*/

public class SearchSearchGroups extends TestHelper{
   @Test
   public void test_c1651_SearchSearchGroups(){
	   //loginToRecenseo();
	   handleSearchSearchGroups(driver);
   }

    private void handleSearchSearchGroups(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
        
    	SearchPage.setWorkflow("Search");
         
        SearchesAccordion.open();
        SearchesAccordion.selectFilter("All");
        SearchesAccordion.addSelectedToSearch("iControl_032 - Complicated Saved Search");
        
        switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        List<WebElement> addresseeCriterionList = getElements(By.cssSelector(".CriterionField span"));
        WebElement contextClickPoint = null;
        for(WebElement addressee : addresseeCriterionList){
        	System.err.println(addressee.getText() + "***********");
        	if(getText(addressee).equals("Addressee")){
        		addressee.click();
        		System.out.println( Logger.getLineNumber() + "Addressee selected...");
        		contextClickPoint = addressee;
        	}
        }
        
        
        hoverOverElement(contextClickPoint);
        Actions actions = new Actions(driver);
        actions.contextClick().perform();
        
        boolean isContextMenuAppeared = getElements(By.tagName("a")).stream().anyMatch(a -> getText(a).equals("Create Search Group"));
        
        softAssert.assertTrue(isContextMenuAppeared, "(3) Make sure context menu displays with Create Search Group item on right click, when more than one criteria selected:: ");
        
        int groupCountBefore = getTotalElementCount(By.cssSelector(".GroupCriterion"));
        
        tryClick(By.xpath("//a[text()='Create Search Group']"));
        
        int groupCountAfter = getTotalElementCount(By.cssSelector(".GroupCriterion"));
        
        softAssert.assertEquals(groupCountAfter, groupCountBefore + 1, "*** Group created:: ");
        
        List<WebElement> groupList = getElements(By.cssSelector(".GroupCriterion"));
        
        int idOfNewGroup = Integer.parseInt(groupList.get(2).getAttribute("id").replaceAll("\\D+", ""));
        int idOf3rdGroup = Integer.parseInt(groupList.get(3).getAttribute("id").replaceAll("\\D+", ""));
        
        softAssert.assertTrue(idOfNewGroup > idOf3rdGroup, "4) New group is placed before the 3rd group:: ");
        
        hoverOverElement(By.id("criterion6"));
        click(Utils.waitForElement("criterion6", "id").findElement(By.tagName("div")).findElements(By.tagName("button")).get(2));
        
        hoverOverElement(By.id("criterion7"));
        click(Utils.waitForElement("criterion7", "id").findElement(By.tagName("div")).findElements(By.tagName("button")).get(2));
        
        boolean is3rdGroupRemoved = getElements(By.cssSelector(".GroupCriterion")).stream()
        								.anyMatch(a -> !a.getAttribute("id").equals("criterion" + idOf3rdGroup));
        
        
        softAssert.assertTrue(is3rdGroupRemoved, "6) Third group is removed:: ");
        
        softAssert.assertAll();
    }
}
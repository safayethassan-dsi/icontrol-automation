package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/**
 * 
 * @author Ishtiyaq Kamal
 *
 */
public class GridWorkFlowState extends TestHelper{
	@Test
	public void test_c163_GridWorkFlowState(){
		handleGridWorkFlowState(driver);
	}

	protected void handleGridWorkFlowState(WebDriver driver) {
		By workFlowLocator = By.cssSelector("#workflows > div > div > div > div > a");
		
		new SelectRepo(AppConstant.DEM0);
  		SearchPage.setWorkflow("Search");
  		
  		performSearch();
  		 
		driver.switchTo().defaultContent();

		String firstWorkFlow = "";
		String currentWorkflow = "";
		String newWorkFlow = "";

		currentWorkflow = SearchPage.getCurrentWorkFlow();
		firstWorkFlow = currentWorkflow;
		SearchPage.openUserMenu();
		//waitFor(2);
		List<WebElement> workFlowItems = getElements(workFlowLocator);
		boolean isCurrentWorkflowHighlighted = false;
		for(WebElement workFlow : workFlowItems){
			System.err.println(workFlow.getText() + " : " + workFlow.getAttribute("class")+"****" );
			if(workFlow.equals("")){
				//do nothing
			}else if(workFlow.getText().trim().equals(currentWorkflow) && workFlow.getAttribute("class").equals("selected-item")){
				isCurrentWorkflowHighlighted = true;
				break;
			}else{
				newWorkFlow =  currentWorkflow;
			}
		}
		
		softAssert.assertTrue(isCurrentWorkflowHighlighted, "2) Current workflow should be highlighted:: ");
		
		SearchPage.setWorkflowFromGridView(newWorkFlow);
	
		SearchPage.logout();

		performLoginToRecenseo();
		
		new SelectRepo(AppConstant.DEM0);

		currentWorkflow = SearchPage.getCurrentWorkFlow();
		
		softAssert.assertEquals(currentWorkflow, newWorkFlow, "5) Workflow stayed on the last workflow selected:: ");
	
		SearchPage.setWorkflow(firstWorkFlow);
		
		SearchPage.logout();
		
		performLoginToRecenseo();
		
		new SelectRepo(AppConstant.DEM0);

		currentWorkflow = SearchPage.getCurrentWorkFlow();
		
		softAssert.assertEquals(currentWorkflow, firstWorkFlow, "8) Workflow stayed on the last workflow selected:: ");
		
		softAssert.assertAll();
	}
}

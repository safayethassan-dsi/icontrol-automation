package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

public class Dbd30TagsWidget extends TestHelper{
	long date = new Date().getTime();
	String topLevelTagName = "Dbd30TagsWidget_TopLevelTag_" + date;
	String subTagName = "Dbd30TagsWidget_SubTag_" + date;
	String editedTagName = topLevelTagName + "_Edited";
	String tagNumRemoved="";//indicates which tag was removed
	String tagNumAdded="";//indicates which tag was added
	
	@Test(enabled = true)
	public void test_c220_Dbd30TagsWidget(){
		handleDbd30TagsWidget(driver);
	}

	private void handleDbd30TagsWidget(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);

		SearchPage.setWorkflow("Search");
		
		FolderAccordion.open();
		FolderAccordion.openFolderForView("Loose Files");

		switchToDefaultFrame();
		//SearchPage.setWorkflowFromGridView(driver, "Issue Review");
		SearchPage.setWorkflowFromGridView(driver, "Issue Review");
		
		boolean isTagWidgetDisplayed = ReviewTemplatePane.isTagWidgetDisplayed();
			
		softAssert.assertTrue(isTagWidgetDisplayed, "(2)Confirm Tags Widget is present in the coding template:: ");
	
		TagManagerInGridView.open();
		TagManagerInGridView.createTopLevelTag(topLevelTagName, "");
		TagManagerInGridView.addDocumentsForTag(topLevelTagName);
		TagManagerInGridView.createSubTagFor(topLevelTagName, subTagName, "");
		TagManagerInGridView.addDocumentsForTag(subTagName);
		
		String editingConfirmationMsg = TagManagerInGridView.editTag(topLevelTagName, editedTagName, "");
		
		softAssert.assertEquals(editingConfirmationMsg, "Selected tag was successfully renamed.", "6) Click on the name of one of the new tags so that it is highlighted, and use the \"Rename Tag\" button to change the tag name. Verify the tag instantly reflects your change:: ");
		
		TagManagerInGridView.deleteTag(editedTagName);
		
		TagManagerInGridView.searchForTag(editedTagName);
		
		String searchMsgForDeletedTag = getText(TagManagerInGridView.tagSearchMsgContainerLocator);
		
		softAssert.assertEquals(searchMsgForDeletedTag, "No Tags meet selected filter", "7) Click on the name of one of the new tags so that it is highlighted, and use the \"Delete Tag\" button to delete it:: ");
		
		softAssert.assertAll();
	}
}

package com.icontrolesi.automation.testcase.selenium.page.accordion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;

import com.icontrolesi.automation.platform.util.Logger;

public class ReviewAssignmentAccordion extends ParentAccordion {

	@FindBy(how = How.CSS, using = "li[class*='assignment']")
	public List<WebElement> assignmentList;

	public ReviewAssignmentAccordion(WebDriver driver) {
		super(driver);
		driver.switchTo().frame("reviewQueueFrame");
		SearchPage.waitForElement(driver, "li[class*='assignment']", "css-selector");
	}

	public String getPriority(WebElement item) {

		WebElement span = item.findElement(By.cssSelector("div"));
		span = span.findElement(By
				.cssSelector("div[class='header ui-helper-clearfix']"));
		span = span.findElement(By.cssSelector("span"));
		return span.getText().trim();

	}

	public String getWorkFlow(WebElement item) {
		WebElement span = item.findElement(By.cssSelector("div"));
		span = span.findElement(By
				.cssSelector("div[class='detail ui-helper-clearfix']"));
		span = span.findElement(By.cssSelector("span"));
		return span.getText().trim();
	}
	
	public Map<String,String> getStatus(WebElement item){
		
		Map<String, String> map = new HashMap<String, String>();
		
		WebElement span = item.findElement(By.cssSelector("div"));
		WebElement div= span.findElement(By.cssSelector("div[class='header ui-helper-clearfix']"));
		List<WebElement> spanChildren= div.findElements(By.cssSelector("span"));
		String assignmentName= spanChildren.get(1).getText();
		System.out.println( Logger.getLineNumber() + assignmentName);
		span = span.findElement(By.cssSelector("div[class='header ui-helper-clearfix']"));
		span = span.findElement(By.cssSelector("div[class='right stats']"));
		
		
		String spanClass=null;
		
		span = span.findElement(By.cssSelector("span"));
		spanClass = span.getAttribute("class");
		
		map.put("class", spanClass);
		map.put("value", "");
		map.put("pending", "");
		
		switch(spanClass){
		
		case "open":
			map.put("value", span.getText().trim());
			span = SearchPage.getFollowingSibling(driver, span);
			map.put("pending", span.getText().trim());
			break;
			
		case "complete":
			span = SearchPage.getFollowingSibling(driver, span);
			map.put("value", span.getText().trim());
			break;
		
		}
		
		return map;
	}
	


}

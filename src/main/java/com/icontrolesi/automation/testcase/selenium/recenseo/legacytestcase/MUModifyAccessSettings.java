package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;
/**
 * 
 * @author Shaheed Sagar
 * (1) Log into DEM0 and Select Administrative Settings under Settings Menu and Choose Manage Users 
	(2) Select 3-5 users, and click "Load Settings." 
	(3) Modify the Team Selection, two "General Access" settings and two "Workflow State Access" settings, making note of what you have changed 
	(4) Click "Update Settings" 
	(5) Verify on the confirmation page (Updates to apply): 
	(a) The correct users appear in "Selected Users" grid 
	(b) When checked, the "Show only proposed changes ......" radio button correctly shows the modified Access Settings (Including the team change) 
	(c) When selecting a specific user from the "Selected User" list, you can review the resultant Access Settings for each user (including the team change).  
	(6) Click on "Save Changes" and confirm the "Accesses were updated successfully!" screen appears.  
	(7) Reload the access settings of those same users and verify that only the intended changes have been applied.
	 *
 */
public class MUModifyAccessSettings extends TestHelper{
	@Test(enabled = false)
	public void test_c1675_MUModifyAccessSettings(){
		handleMUModifyAccessSettings(driver);
	}

	private void handleMUModifyAccessSettings(WebDriver driver){
		Driver.setWebDriver(driver);
		
		SearchPage sp = new SearchPage();

		new SelectRepo(Driver.getDriver(), "Recenseo Demo DB");

		waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		
		UserManager userManager = new UserManager();
		
		SprocketMenu sprocketMenu = new SprocketMenu();
		//sprocketMenu.selectSubMenu(Driver.getDriver(), sp, "Tools", "User Manager");
		
		userManager.checkUser("azaman");
		userManager.checkUser("jlumby");
		userManager.checkUser("rlauck");
		
		userManager.clickLoadSettings();
		
		userManager.setGeneralAccessWithoutSave(Driver.getDriver(), sp, "Global Edit");
		userManager.setGeneralAccessWithoutSave(Driver.getDriver(), sp, "Image On The Fly");
		
		List<WebElement> radioElements = Driver.getDriver().findElements(By.id("wfs_access_0"));
		WebElement element = userManager.getProperElement(radioElements, UserManager.NO_ACCESS);
		userManager.setState(element);
		radioElements = Driver.getDriver().findElements(By.id("wfs_access_1"));
		element = userManager.getProperElement(radioElements, UserManager.NO_ACCESS);
		userManager.setState(element);
		
		userManager.selectTeam("DEM0 Team 2");
		
		click(Driver.getDriver().findElement(By.id("updateAccesses")));
		
		List<WebElement> tdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='generalAccessesForUpdate_access']"));
		//List<WebElement> WtdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='workflowStateAccessesForUpdate_access']"));
		
		List<WebElement> userElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='loadedUsers_userID']"));
		
		List<String> expectedUsers = new ArrayList<String>();
		expectedUsers.add("azaman");
		expectedUsers.add("jlumby");
		expectedUsers.add("rlauck");
		
		boolean allOk = true;
		for(WebElement e : userElements){
			if(!expectedUsers.contains(e.getText())){
				allOk = false;
			}
		}
		
		/*if(allOk){
			Utils.handleResult("5(a) The correct users appear in 'Selected Users' grid ", result, true);
		}else{
			Utils.handleResult("5(a) The correct users appear in 'Selected Users' grid ", result, false);
		}*/
		
		click(Driver.getDriver().findElement(By.id("showAccessesAll")));
		
		String teamName = sp.checkValueOfSelectBox(Driver.getDriver(), "selectedTeam");
		/*if(teamName.equalsIgnoreCase("DEM0 Team 2")){
			Utils.handleResult("5(b) When checked, the 'Show only proposed changes ......' radio button correctly shows the modified Access Settings (Including the team change", result, true);
		}else{
			Utils.handleResult("5(b) When checked, the 'Show only proposed changes ......' radio button correctly shows the modified Access Settings (Including the team change", result, false);
		}*/
		
		click(userElements.get(1));
		tdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='generalAccessesForUpdate_access']"));
		if(tdElements.get(6).getAttribute("class").equalsIgnoreCase("accessByNone")){
			teamName = sp.checkValueOfSelectBox(Driver.getDriver(), "selectedTeam");
			/*if(teamName.equalsIgnoreCase("DEM0 Team 2")){
				Utils.handleResult("(c) When selecting a specific user from the 'Selected User' list, you can review the resultant Access Settings for each user (including the team change).", result, true);
			}else{
				Utils.handleResult("(c) When selecting a specific user from the 'Selected User' list, you can review the resultant Access Settings for each user (including the team change).", result, false);
			}*/
		}else{
			//Utils.handleResult("(c) When selecting a specific user from the 'Selected User' list, you can review the resultant Access Settings for each user (including the team change).", result, false);
		}
		
		click(Driver.getDriver().findElement(By.id("saveAccesses")));
		Utils.waitForAlertAndAccept();
		
		userManager.checkUser("azaman");
		userManager.checkUser("jlumby");
		userManager.checkUser("rlauck");
		
		userManager.clickLoadSettings();
		
		tdElements = Driver.getDriver().findElements(By.cssSelector("td[aria-describedby='generalAccesses_access']"));
		/*if(tdElements.get(6).getAttribute("class").equalsIgnoreCase("accessByNone")){
			Utils.handleResult("(7) Reload the access settings of those same users and verify that only the intended changes have been applied.", result, true);
		}else{
			Utils.handleResult("(7) Reload the access settings of those same users and verify that only the intended changes have been applied.", result, false);
		}*/
	}
}


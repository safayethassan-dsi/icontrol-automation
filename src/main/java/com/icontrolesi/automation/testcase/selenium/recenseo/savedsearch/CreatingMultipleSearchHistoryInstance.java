package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

public class CreatingMultipleSearchHistoryInstance extends TestHelper{
By todayItemLocator = By.cssSelector("#today > ul > li");
	
	@Test
	public void test_c37_CreatingMultipleSearchHistoryInstance(){
		handleCreatingMultipleSearchHistoryInstance();
	}
	
	private void handleCreatingMultipleSearchHistoryInstance(){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		SearchesAccordion.open();
		SearchesAccordion.openSearchHistoryTab();
		
		SearchesAccordion.openSavedSearchInfoDetail(SearchesAccordion.todayItemLocator);
		
		int totalCountBeforeRunningSearch = SearchesAccordion.getSavedSearchesInstanceCount();
		
		String searchName = SearchesAccordion.getSearchCriteriaNameToSearch();
		
		SearchesAccordion.closeSavedSearchInfoDetail();
		
		switchToDefaultFrame();
		waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		
		SearchPage.addSearchCriteria(searchName);
		SearchPage.setOperatorValue(0, "is not empty");
    	performSearch();
    	
    	SearchPage.goToDashboard();
    	waitForFrameToLoad(driver, Frame.MAIN_FRAME);
    	
    	SearchPage.clearSearchCriteria();
    	SearchPage.addSearchCriteria(searchName);
    	SearchPage.setOperatorValue(0, "is not empty");
    	performSearch();
    	
    	SearchPage.goToDashboard();
    	waitForFrameToLoad(driver, Frame.MAIN_FRAME);
    	
    	SearchesAccordion.open();
		SearchesAccordion.openSearchHistoryTab();
		
		SearchesAccordion.openSavedSearchInfoDetail(SearchesAccordion.todayItemLocator);
		
		int totalCountAfterRunningSearch = SearchesAccordion.getSavedSearchesInstanceCount();
		
		System.out.println( Logger.getLineNumber() + "Count got from detail: " + totalCountAfterRunningSearch);
		
		softAssert.assertEquals(totalCountAfterRunningSearch, totalCountBeforeRunningSearch + 2, "6. Multiple search history instances gets created in the 'Search History' tab:: ");
    	
       	softAssert.assertAll();
	}
}

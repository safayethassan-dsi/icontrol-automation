package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

/**
 * mantis# 15126
 * 
 * @author Ishtiyaq Kamal, atiq
 * 
 *         DB: DEM0 (1) Go to search page and run a new search with the
 *         criteria: Confidentiality contains (Double click in this field for a
 *         list of options). Confirm all confidentiality options are available.
 *         (2) Run Search. Confirm results are correct. (3) Repeat for privilege
 *         field (4) Run Search. Confirm results are correct.
 *
 */


public class SearchPickList extends TestHelper{
	List<String> suggestionForConfidentialKeyWord = Arrays.asList("(Test)", "Attorneys Eyes Only", "Confidential",
															  "No Idea", "None", "Sometimes", "Somewhat",
															  "Uber-Conf", "Undecided", "confidential" );
	
	List<String> suggestionForPrivilegeKeyWord = Arrays.asList("No", "Question", "Undecided", "Yes", "no", "undecided",
																"whatever", "yes");
	

	@Test
	public void test_c99_SearchPickList(){
		handleSearchPickList(driver);
	}

	protected void handleSearchPickList(WebDriver driver) {
		new SelectRepo( "Recenseo Demo DB");
		
		DocView.openPreferenceWindow();
		DocView.deselectAllDocument();
		DocView.checkSelectedColumns("Document ID", "Confidentiality", "Privileged");
		DocView.closePreferenceWindow();
		
		SearchPage.setWorkflow( "Search");

		SearchPage.addSearchCriteria("Confidentiality");

		WebElement confidentiality_area = getElement(By.cssSelector("input.CriterionValue"));

		Actions action = new Actions(driver);
		action.doubleClick(confidentiality_area).build().perform();
		
		waitForAttributeIn(By.cssSelector("body > ul"), "style", "display: block");
		List<String> suggestedListItems = getListOfItemsFrom(By.cssSelector("body > ul > li"), suggestion -> suggestion.getText().trim());
		System.out.println( Logger.getLineNumber() + suggestedListItems);
		softAssert.assertEquals(suggestionForConfidentialKeyWord, suggestedListItems, "(1)Confirm all confidentiality options are available:: ");

		tryClick(By.cssSelector("body > ul[style^='display: block;'] > li"));
		
		performSearch();
		
		List<String> confidentialSearchedItems = getListOfItemsFrom(DocView.documentConfidentialColumnLocator, c -> c.getText().trim().toLowerCase());
		
		boolean isSearchOk = confidentialSearchedItems.stream().allMatch(item -> item.contains(suggestionForConfidentialKeyWord.get(0).toLowerCase()));
		
		System.out.println( Logger.getLineNumber() + "Matched: " + suggestionForConfidentialKeyWord.get(0));
		
		softAssert.assertTrue(isSearchOk, "(2)Run Search. Confirm results are correct, Contents of Confidentialaity column are as expected:: ");
		
		SearchPage.goToDashboard();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		SearchPage.clearSearchCriteria();
		SearchPage.addSearchCriteria("Privileged");

		WebElement privilege_area = getElement(By.cssSelector("input.CriterionValue"));

		action.doubleClick(privilege_area).build().perform();
		
		waitForAttributeIn(By.cssSelector("body > ul"), "style", "display: block");

		suggestedListItems = getListOfItemsFrom(By.cssSelector("body > ul > li"), suggestion -> suggestion.getText().trim());
		
		softAssert.assertEquals(suggestionForPrivilegeKeyWord, suggestedListItems, "(3)Confirm all confidentiality options are available:: ");

		tryClick(By.cssSelector("body > ul[style^='display: block;'] > li"));
		
		performSearch();
		
		List<String> privilegeSearchedItems = getListOfItemsFrom(DocView.documentPrivilegedColumnLocator, p -> p.getText().trim().toLowerCase());
		
		isSearchOk = privilegeSearchedItems.stream().allMatch(item -> item.contains(suggestionForPrivilegeKeyWord.get(0).toLowerCase()));
		
		System.out.println( Logger.getLineNumber() + "Matched: " + suggestionForPrivilegeKeyWord.get(0));
		
		softAssert.assertTrue(isSearchOk, "(4)Run Search. Confirm results are correct, Contents of Confidentialaity column are as expected:: ");
		
		softAssert.assertAll();
	}
}

package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageDocumentSecurity;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewTemplatePane;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;

import com.icontrolesi.automation.platform.util.Logger;

public class Dbd61TagsWidget extends TestHelper{
	String tagName = "Dbd61TagsWidget_" + new Date().getTime();
	String firstTag = tagName + "_1";
	String secondTag = tagName + "_2";
	String thirdTag = tagName + "_3";
	
	String firstTagPermission = "Internal";
	String secondTagPermission = "Private";
	String thirdTagPermission = "Team";
	
	final String step5a = "5a) When you open the example document in Recenseo, you cannot see the tags in the tags widget:: "; 
	final String step5b = "5b) If you modify and save/post the document your changes do not also remove the documents from those tags:: ";
	
	@Test
	public void test_c226_Dbd61TagsWidget(){
		handleDbd66TagsWidget(driver);
	}


	private void handleDbd66TagsWidget(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Tags");
		DocView.closePreferenceWindow();
		
		SearchPage.setWorkflow("Search");
	
		performSearch();
		
		//tryClick(By.cssSelector("td[aria-describedby='grid_cb']"));
		DocView.selectAllDocuments();
		
		new DocView().clickActionMenuItem("Manage Document Security");
		switchToFrame(1);
		ManageDocumentSecurity.unBlockTeam("Document Security");	//unblock all documents first
	    ManageDocumentSecurity.closeWindow(); 
	    
	    //waitForFrameToLoad(Frame.MAIN_FRAME);
		
		int selectedDocumentId = DocView.getDocmentId();
		System.out.println( Logger.getLineNumber() + "Document ID: " + selectedDocumentId);
		
		TagManagerInGridView.open();
		TagManagerInGridView.createTopLevelTag(firstTag, firstTagPermission);
		TagManagerInGridView.createTopLevelTag(secondTag, secondTagPermission);
		TagManagerInGridView.createTopLevelTag(thirdTag, thirdTagPermission);
		
		TagManagerInGridView.addDocumentsForTag(firstTag);
		TagManagerInGridView.addDocumentsForTag(secondTag);
		TagManagerInGridView.addDocumentsForTag(thirdTag);
		
		TagManagerInGridView.close();
		
		SearchPage.logout();
		performLoginWithCredential(AppConstant.USER2, AppConstant.PASS2);
		
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Document ID");
		SearchPage.setCriterionValue(0, selectedDocumentId+"");
		
		performSearch();
		
		List<String> tagListForSelectedDocument = DocView.getTagsNameSplitted(1);	
		
		softAssert.assertFalse(tagListForSelectedDocument.contains(firstTag), step5a);
		softAssert.assertFalse(tagListForSelectedDocument.contains(secondTag), step5a);
		softAssert.assertFalse(tagListForSelectedDocument.contains(thirdTag), step5a);
		
		editText(DocView.commentInCodingTemplate, tagName);
		
		ReviewTemplatePane.saveDocument();
		
		SearchPage.logout();
		performLoginToRecenseo();
		
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Document ID");
		SearchPage.setCriterionValue(0, selectedDocumentId+"");
		
		performSearch();
		
		List<String> tagListForSelectedDocumentForOwner = DocView.getTagsNameSplitted(1);
		
		softAssert.assertTrue(tagListForSelectedDocumentForOwner.contains(firstTag), step5b);
		softAssert.assertTrue(tagListForSelectedDocumentForOwner.contains(secondTag), step5b);
		softAssert.assertTrue(tagListForSelectedDocumentForOwner.contains(thirdTag), step5b);
		
		softAssert.assertAll();
	 }
}

package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/*
 * DB: DEM0
(1) Go to search page and run a new search with the criteria: Document Date is "2000-06-22" (use date picker icon, do not type it out)
(2) Run search. Confirm results are correct
(3) Return to search page and run a new search with the criteria: Document Date is between "2000-06-22" and "2000-12-31" (use date picker icon, do not type it out)
(4) Run search. Confirm results are correct
 */


public class SearchFieldDate extends TestHelper{
	String startDate = "2000-06-22";
	String endDate = "2000-12-31";
	
	By startDateLocator = By.cssSelector(".CriterionInput > input");
	By endDateLocator = By.cssSelector(".CriterionInput > input:nth-child(3)");
	
   @Test
   public void test_c64_SearchFieldDate(){
	   //loginToRecenseo();
	   handleSearchFieldDate(driver);
   }

   	
    protected void handleSearchFieldDate(WebDriver driver){
	   new SelectRepo(AppConstant.DEM0);
	   
	   DocView.openPreferenceWindow();
	   DocView.checkSelectedColumns("Document Date");
	   DocView.closePreferenceWindow();
  		
  	   SearchPage.setWorkflow("Search");
  		
  	   SearchPage.addSearchCriteria("Document Date");
  	   
  	   hoverOverElement(By.id("criterion2"));
  	   tryClick(By.cssSelector("#criterion2  button:nth-child(2)  span.ui-icon-calendar"), 1);
  	   new Select(getElement(By.className("ui-datepicker-month"))).selectByIndex(5);
  	   selectFromDrowdownByText(By.className("ui-datepicker-year"), "2000");
  	   getElements(By.cssSelector(".ui-datepicker-calendar tr td")).stream().filter(d -> getText(d).equals("22")).findFirst().get().click();
  	         
	  
  	   SearchPage.setSortCriterion();
  	   SearchPage.setSortCriterionValue(1, "Document Date");
  	   
  	   performSearch();
		 
	   String firstDocumentsDate = getText(DocView.documentDateColumnLocator).split("\\s")[0];
	   DocView.gotoLastDocument();
	   String lastDocumentDate = getListOfItemsFrom((DocView.documentDateColumnLocator), d -> getText(d))
			   						.get(getTotalElementCount(DocView.documentDateColumnLocator) - 1)
			   						.split("\\s")[0];
	

	   softAssert.assertEquals(lastDocumentDate, startDate, "(2)Run search. Confirm results are correct,Document date columns contain the date [2000-06-22]:: ");
		 
	   SearchPage.goToDashboard();
	   waitForFrameToLoad(Frame.MAIN_FRAME);
	  
	   SearchPage.setOperatorValue(0, "is between");
	   SearchPage.setCriterionValue(1, endDate);
           		 
	   performSearch();
	   
	   firstDocumentsDate = getText(DocView.documentDateColumnLocator).split("\\s")[0];
	   DocView.gotoLastDocument();
	   lastDocumentDate = getListOfItemsFrom((DocView.documentDateColumnLocator), d -> getText(d))
					.get(getTotalElementCount(DocView.documentDateColumnLocator) - 1)
					.split("\\s")[0];
	   
	   boolean isSearchOk = firstDocumentsDate.compareTo(startDate) >= 0 && lastDocumentDate.compareTo(endDate) <= 0;
    		 
	   softAssert.assertTrue(isSearchOk, "3) Confirm results are correct:: ");
  	   	
	   softAssert.assertAll();
	}
}

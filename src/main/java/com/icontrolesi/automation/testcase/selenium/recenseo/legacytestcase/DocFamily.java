package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.io.Result;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

public class DocFamily extends TestHelper{
public Result result;
public static final int SORTING_TIME = 50;
public static final String SORTING_MESSAGE = "Confirm each time that the re-sort works properly(sorting done in an expected amount of time):";

@Test
public void test_c374_DocFamily(){
	//loginToRecenseo();
	handleDocFamily(driver);
}


private void handleDocFamily(WebDriver driver){
	new SelectRepo(AppConstant.DEM0);
	
	String [] documentIDList = {"513", "2507", "3074"};
	
	isDocFamilyCriteriaOk(documentIDList[0], 44);
	isDocFamilyCriteriaOk(documentIDList[1], 36);
	isDocFamilyCriteriaOk(documentIDList[2], 5);
 }

	private void isDocFamilyCriteriaOk(String docID, int totalCount){
		System.out.println( Logger.getLineNumber() + "Testing is in progress for Document ID: " + docID);
		switchToDefaultFrame();
		SearchPage.waitForFrameLoad(Frame.MAIN_FRAME);
		
		SearchPage.addSearchCriteria("Document ID");
		SearchPage.setCriterionValue(0, docID);
		
		performSearch();
		
		DocView.setNumberOfDocumentPerPage(50);
		
		int docCount = DocView.getDocmentCount();
		
		softAssert.assertEquals(docCount, totalCount, "(a) Each document includes the complete Document Family (" + docID + "/" + totalCount+ " docs)");
		
		List<WebElement> allTrElement = getElements(DocView.gridRowLocator);
		
		DocView.selectViewByLink("Doc Family");
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		WebElement clipElement = getElement(By.cssSelector("#grid > tbody > tr:nth-child(2) > td:nth-child(3) > span:nth-child(2) > img"));
			
		
		
		softAssert.assertTrue((clipElement.getAttribute("src").contains("clip.gif")), "(b) Recenseo displays the Family in proper order (parent email first followed by properly ordered attachments):: ");
		
		int counter = 0;
		for(int i=1;i<allTrElement.size();i++){
			if(allTrElement.get(i).getAttribute("class").contains("ResultedFromIncludeDocset")){
				counter++;
			}
		}
		
		softAssert.assertEquals(counter, totalCount - 1 , "(c) The Added Documents are highlighted to indicate they are pulled into the result set ");
		
		softAssert.assertEquals(getText(By.id("breadcrumb_msg")), "You are viewing the document family for doc id " + docID, "(d) The 'You are viewing the document family for doc id " + docID + "' message appears at the top-left, and references the appropriate document id ");
		
		DocView.clickBackToSearchResult();
		
		allTrElement = getElements(DocView.gridRowLocator);
		
		softAssert.assertTrue(getAttribute(allTrElement.get(0), "class").contains("Highlight"), "(e)Hitting the 'Back to Search Results' button properly returns focus to the original search/document.");
		
		SearchPage.goToDashboard();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		SearchPage.clearSearchCriteria();
	}
}

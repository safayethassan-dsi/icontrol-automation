package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FileMethods;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;
/**
 * 
 * @author Shaheed Sagar
 * "(1) in DEM0, open folder  \Email\Foster, Ryan e-mail 
	(2) From the Actions Menu in Grid View, Select Save Generate Production Input File 
	(3) Open file and confirm all document ids are accounted for and right database is listed"
 *
 */
public class POIGenerateProdInputFile extends TestHelper{
	@Test
	public void test_c1640_POIGenerateProdInputFile(){
		handlePOIGenerateProdInputFile(driver);
	}

	private void handlePOIGenerateProdInputFile(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);

		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Document ID");
		DocView.closePreferenceWindow();
		
		SearchPage.setWorkflow("Search");
	
		
		FolderAccordion.open();
		FolderAccordion.expandFolder("Email");
		FolderAccordion.openFolderForView("Foster, Ryan e-mail");
		
		Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMd");
        String fileMatchPattern = "DEM0-" + sdf.format(date) + "\\d+";
        
        System.err.println(fileMatchPattern + "$$$$$$$$$$");
        
        File [] fileListToDelete = FileMethods.getListOfFiles(AppConstant.DOWNLOAD_DIRECTORY, fileMatchPattern);
        FileMethods.deleteFiles(fileListToDelete);
        
		DocView docView = new DocView();
		docView.clickActionMenuItem("Generate Production Input File");
		
		waitFor(5);
		
		fileListToDelete = FileMethods.getListOfFiles(AppConstant.DOWNLOAD_DIRECTORY, fileMatchPattern);
		
		List<String> idList = new ArrayList<String>();
		
		if(fileListToDelete.length != 1){
			System.out.println( Logger.getLineNumber() + "No files downloaded...");
			throw new SkipException("Either no files downloaded with the pattern expected or there are multiple files that arises conflicts!");
		}else{
			try{
			BufferedReader br = new BufferedReader(new FileReader(fileListToDelete[0]));
			String line = "";
			while((line = br.readLine()) != null){
				 Pattern pattern = Pattern.compile(".*inv_id=\"\\d+\".*");
				 if(pattern.matcher(line).find()){
					 idList.add(line.replaceAll("\\D+", ""));
				 }
			}
			
			br.close();
			}catch (Exception e) {
				System.err.println(e.getMessage());
			}
			
		}
		
		List<String> documentIdListInGridView = getListOfItemsFrom(DocView.documentIDColumnLocator, id -> getText(id)); 
		
		softAssert.assertEquals(idList, documentIdListInGridView, "***) All document IDs are accounted for:: ");
		
		softAssert.assertAll();
	}
}

package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


/**
 * mantis# 15127
 * @author Ishtiyaq Kamal
 * 
 * DB: DEM0
(1) Run a search where no results are expected (Document ID IS EMPTY, For example)
(2) Verify that Recenseo:
(a) Presents dialogue: "Your search did not find any documents, please revise it and try again."
(b) Returns to search page
(c) Keeps the search you ran intact and ready for edits 
 *
 */


public class SearchResults extends TestHelper{
	 @Test
	 public void test_c56_SearchResults(){
		 //loginToRecenseo();
		 handleSearchResults(driver);
	 }
    
	 protected void  handleSearchResults(WebDriver driver) {
		new SelectRepo(AppConstant.DEM0);	
	   
	   waitForFrameToLoad(driver, Frame.MAIN_FRAME);
        
       //Select Search Criteria
		new Select(getElement(By.id("addList"))).selectByVisibleText("Document ID");
		
		//performSearch();
		waitForElementToBeClickable(By.id("topSearchButton"));
		getElement(By.id("topSearchButton")).click();
		
  		try {
		        WebDriverWait wait = new WebDriverWait(driver, 15);
		        wait.until(ExpectedConditions.alertIsPresent());
		        
		        Alert alert = driver.switchTo().alert();
		        
		        System.out.println( Logger.getLineNumber() + alert.getText());
		        
		        String alertText = alert.getText();
		        
		        alert.accept();
		        
		        boolean isInvalidSearchDisplayAlert = alertText.contains("One or more errors prevented your search from finishing:")
		        										&& alertText.contains("Search failed: Failed to get search results");
		     		        
		        Assert.assertTrue(isInvalidSearchDisplayAlert, "(2a)Presents dialogue: One or more errors prevented your search from finishing:: "); 
	          
	    } catch (Exception e) {
	        //exception handling
	    }
		
		
		boolean isSearchIntactAndEditable = false;
		try{
				waitForVisibilityOf(driver, By.cssSelector("span[style='vertical-align: top;']"));
				isSearchIntactAndEditable = true;
		    }catch (TimeoutException e) {
		        //Logger.log("(2c)Keeps the search you ran intact and ready for edits:fail");
	       } 
		
		Assert.assertTrue(isSearchIntactAndEditable, "(2c)Keeps the search you ran intact and ready for edits:: ");
	}//end SearchResults
}//end class	
	
package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;

public class Frame implements CommonActions{
	public static final String DEFAULT_FRAME = "DEFAULT";
	public static final String MAIN_FRAME = "MAIN";
	public static final String VIEWER_FRAME = "VIEWER";
	public static final String FOLDER_FRAME = "folderFrame";
	public static final String TAGS_FRAME = "tagFrame";
	public static final String CLUSTER_FRAME = "folderFrame";
	public static final String TEAMS_FRAME = "teams_frame";
	public static final String DOMAINS_FRAME = "domainsFrame";
	public static final String REVIEW_ASSIGNMENTS_FRAME = "reviewQueueFrame";
	public static final String SAVED_SEARCHES_FRAME = "searchesFrame";
	public static final String MANAGE_TAG_FRAME = "tag-manageIframe";
	
	public static final WebElement DOWNLOAD_PARENT_FRAME = Driver.getDriver().findElement(By.cssSelector("iframe[src='interceptorSingleDoc.jsp']"));
	
	public static String currentFrame = DEFAULT_FRAME;
	
	private static final Frame FRAME = new Frame();
	
	private Frame(){}

	public static String getCurrentFrame() {
		return currentFrame;
	}

	public static void setCurrentFrame(String currentFrame) {
		if(currentFrame.equals(Frame.DEFAULT_FRAME))
			FRAME.switchToDefaultFrame(Driver.getDriver());
		else{
			Driver.getDriver().switchTo().defaultContent();
			FRAME.waitForFrameToLoad(Driver.getDriver(), currentFrame);
		}
		Frame.currentFrame = currentFrame;
	}
	
	public static void switchToMainFrame(){
		Driver.getDriver().switchTo().defaultContent();
		new Frame().waitForFrameToLoad(Driver.getDriver(), MAIN_FRAME);
	}
	
	
	public static void switchToViewerFrame(){
		Driver.getDriver().switchTo().defaultContent();
		FRAME.waitForFrameToLoad(Driver.getDriver(), VIEWER_FRAME);
	}
	
	public static void switchToFolderFrame(){
		Driver.getDriver().switchTo().defaultContent();
		FRAME.waitForFrameToLoad(Driver.getDriver(), FOLDER_FRAME);
	}
}

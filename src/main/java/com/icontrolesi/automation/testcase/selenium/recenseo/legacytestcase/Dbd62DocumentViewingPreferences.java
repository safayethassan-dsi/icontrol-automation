package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Preferences;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;


/*
 * 	
		"DB: DEM0 
		(1) Go to Search Page and open the WorkFlow folder: /Email/Johnson, Jeff - 200901 - Part 01 
		(2) Click on Settings -> Document View/Download Settings  
		(3) Change ""Viewing Preferences"" for Microsoft Word to use Imaged version as default and Adobe PDF to use Text version as default. 
		(4) Verify: 
		(a) Any Word document with an imaged version defaults to show the images 
		(b) Any Word document without an imaged version defaults to the native viewer. 
		(c) Any Adobe PDF document defaults to the text view."

 */


public class Dbd62DocumentViewingPreferences extends TestHelper{
	@Test
	public void test_c285_Dbd62DocumentViewingPreferences(){
		handleDocumentViewPreferences(driver);
	}

	private void handleDocumentViewPreferences(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
	
		waitForFrameToLoad(Frame.MAIN_FRAME);
		FolderAccordion.open();
		FolderAccordion.expandFolder("Email");
		FolderAccordion.openFolderForView("Johnson, Jeff - 200901 - Part 01");
		
		switchToDefaultFrame();
		
		DocView.openPreferenceWindow();
		new Preferences().switchPreferenceTab(Preferences.FILE);
		
		Preferences pf= new Preferences();
		pf.switchPreferenceTab("file");
		
		Preferences.setViewerPreferenceTo("Microsoft Word", "Imaged");
		Preferences.setViewerPreferenceTo("PDF", "Text");
		
		DocView.clickCrossBtn();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		waitFor(10);
		DocView.clickOnDocument(10);
		
		//Check if Image viewer has been selected by default
		WebElement liImaged= waitForElement(DocView.imageViewLocator);
		
		softAssert.assertTrue(liImaged.getAttribute("class").contains("ui-state-active"), "(4a)Any Word document with an imaged version defaults to show the images:: ");
		
		//Click on a PDF doc
		DocView.clickOnDocument(13);
		
		WebElement liText= waitForElement(DocView.textViewLocator);
		
		softAssert.assertTrue(liText.getAttribute("class").contains("ui-state-active"), "(4c)Any Adobe PDF document defaults to the text view:: ");
		
		DocView.openPreferenceWindow();
		pf.switchPreferenceTab("file");
		
		Preferences.setViewerPreferenceTo("Microsoft Word", "Native");
		
		DocView.clickCrossBtn();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		DocView.clickOnDocument(10);
		
		//Check if Image viewer has been selected by default
		WebElement nativeView = waitForElement(DocView.nativeViewLocator);
		
		softAssert.assertTrue(nativeView.getAttribute("class").contains("ui-state-active"), "(4b) Any Word document without an imaged version defaults to the native viewer.:: ");
		
		softAssert.assertAll();
	}
}

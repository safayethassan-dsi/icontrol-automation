package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.integration_toolbar;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class ViewSelectorAddToSearchViewDocumentAndRefreshButtonDisplayed extends TestHelper{
	 
	 @Test
	 public void test_c149_ViewSelectorAddToSearchViewDocumentAndRefreshButtonDisplayed() {
	    //loginToRecenseo();
		 
		new SelectRepo(AppConstant.DEM0);
		 
		GeneralReport.openGeneralTallyReport();
		
		String scopeSelectionWindowHeader = driver.findElement(GeneralReport.scopeSelectionWindowHeaderLocator).getText().trim();
		
		softAssert.assertEquals(scopeSelectionWindowHeader, "Scope and Options", "4) Recenseo launches the Repor:: ");
		
		GeneralReport.closeScopeSelectionWindow();
		
		//GeneralReport.loadReportVisualization();
		
		GeneralReport.checkReportToolbar();
	  }
}

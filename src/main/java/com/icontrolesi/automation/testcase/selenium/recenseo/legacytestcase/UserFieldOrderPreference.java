package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;
/**
 *
 * @author Shaheed Sagar
 *
 *  	"DB: DEM0 
 *		(1)  In Search Open Folder ""Loose Documents"" 
		(2) Use your mouse to drag/drop the various fields, tag widget, etc on the coding template. 
		(3) Take note of each change made, and the resultant template order 
		(4) Log out,  and close your browser 
		(5) Re-open browser and log into the same Recenseo database. 
		(6) Verify that the template re-order remains in effect when you return to it in the new session."

*/

public class UserFieldOrderPreference extends TestHelper{
	   @Test(enabled = true)
	   public void test_c227_UserFieldOrderPreference(){
		   //loginToRecenseo();
		   handleUserFieldOrderPreference(driver);
	   }
	
	    
	 private void handleUserFieldOrderPreference(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
    	SearchPage.setWorkflow("Search");
        
        FolderAccordion.open();
        FolderAccordion.openFolderForView("Loose Files");
        
        WebElement draggableElement1 = getElement(By.cssSelector("#attrForm > div > label"));
        WebElement draggableElement2 = getElement(By.cssSelector("#attrForm > div:nth-child(3) > label"));
        
        String beforeDragAndDrop = getText(draggableElement1);
        String beforeDragAndDrop2 = getText(draggableElement2);
        
        System.out.println( Logger.getLineNumber() + beforeDragAndDrop + ", " + beforeDragAndDrop2);
        
        Actions actions = new Actions(driver);
        //actions.clickAndHold(draggableElement1).moveToElement(draggableElement2).release(draggableElement2).build().perform();
        //actions.dragAndDrop(draggableElement1, draggableElement2).build().perform();
        //actions.clickAndHold(draggableElement1).moveToElement(draggableElement2).release(draggableElement1).build().perform();
        actions.clickAndHold(draggableElement1).moveToElement(draggableElement2).release().build().perform();
        waitFor(2);
        
        SearchPage.logout();
        
        //loginToRecenseo();
        
        new SelectRepo(AppConstant.DEM0);
        SearchPage.setWorkflow("Search");
        
        FolderAccordion.open();
        FolderAccordion.openFolderForView("Loose Files");
        
        draggableElement1 = getElement(By.cssSelector("#attrForm > div > label"));
        draggableElement2 = getElement(By.cssSelector("#attrForm > div:nth-child(3) > label"));
        
        String afterDragAndDrop = getText(draggableElement1);
        String afterDragAndDrop2 = getText(draggableElement2);
        
        System.out.println( Logger.getLineNumber() + afterDragAndDrop + ", " + afterDragAndDrop2);
        
        softAssert.assertEquals(beforeDragAndDrop, afterDragAndDrop2, "6. Verify that the template re-order remains in effect when you return to it in the new session:: ");
        softAssert.assertEquals(beforeDragAndDrop2, afterDragAndDrop, "6. Verify that the template re-order remains in effect when you return to it in the new session:: ");
        
        softAssert.assertAll();
    }
}
package com.icontrolesi.automation.testcase.selenium.envity.relativity_tcs;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.common.Frame;
import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.BatchSetDetailsPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.RelativityBatchSetPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.RelativityWorkspaceHome;

public class RelativityLogin extends TestHelper{
	public static final By EMAIL_LOCATOR = By.id("_email");
	public static final By PASSWD_LOCATOR = By.id("_password__password_TextBox");
	public static final By PROJECT_NAME_LOCATOR = By.xpath("//*[text()='iControl SvP']"); 
	
	public static final By ADMINISTRATION_DROPDOWN_ICON_LOCATOR = By.cssSelector(".nav.navbar-nav.flexBar li:nth-child(4) span.icon-small-caret");
	public static final By ADMINISTRATION_ITEMS_LOCATOR = By.cssSelector(".nav.navbar-nav.flexBar li:nth-child(4) ul li");
	
	
	@Test
	public void test_0000_LoginToRelativity(){
		ProjectPage.performLogout();
		
		RelativityWorkspaceHome.loginToRelativity();
		RelativityWorkspaceHome.selectWorkspace("iControl SvP");
		
		tryClick(ADMINISTRATION_DROPDOWN_ICON_LOCATOR);
		tryClick(By.xpath("//*[text()='Batch Sets']"), 30);
		
		Frame.switchToExternalFrame();
		
		RelativityBatchSetPage.openBatchset("SL_09052018_0208 - BS");
		
		System.err.printf("Total Batch: %d", BatchSetDetailsPage.getTotalBatchCount());
			
		System.exit(0);
	}
}

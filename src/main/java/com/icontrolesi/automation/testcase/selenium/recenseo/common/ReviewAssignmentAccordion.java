package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;

import com.icontrolesi.automation.platform.util.Logger;

public class ReviewAssignmentAccordion implements CommonActions{
	public static By tabLocation = By.id("queue");
	public static By visibleAssignmentLocator = By.cssSelector("#assignmentList li[style$='block;']");
	public static By topAddToSearchBtn = By.cssSelector("button[title='Add selected assignment(s) to search']");
	public static By firstAssignmentLocator = By.cssSelector("#assignmentList li[style$='block;'] input.chkAssignment");
	public static By reviewTab = By.cssSelector("a[href='#actionReview']");
	public static By filterTab = By.cssSelector("a[href='#actionFilterAssignments']");
	public static By findTab = By.cssSelector("a[href='#actionFindAssignments']");
	public static By openButtonOnReviewTab = By.cssSelector("button[title='Open selected assignment(s) to review documents']");
	public static By clearAssignmentButtondLocator = By.cssSelector("#actionFindAssignments > button:nth-child(6) > span");
	public static By clearAssignmentFieldLocator = By.id("findAssignment");
	public static By searchingAssignmentMsgLocator = By.cssSelector("#messageContainer > span");
	public static By findButtonLocator = By.cssSelector("#actionFindAssignments > button");
	public static By findTabFilterLocator = By.id("filterOptions");
	public static By searchingForAssignmentLocator = By.cssSelector("#messageContainer > span.leftspace");
	public static By selectAllAssignmentsLocator = By.id("toggleAllAssignments");
	public static By refreshAssignmentLocator = By.id("btnRefreshAssignments");
	public static By refreshMsgLocator = By.cssSelector("#messageContainer span.leftspace");
	public static By sortAssignmentDropdownLocator = By.id("sortAssignments");
	public static By nextAssignmentsBtnLocator = By.id("nextPage");
	
	private static final ReviewAssignmentAccordion reviewAssignment = new ReviewAssignmentAccordion();
	

	public static void open(){
		TagAccordion categoryAccordion = new TagAccordion(); 
		categoryAccordion.waitForElementToBeClickable(tabLocation);
		reviewAssignment.tryClick(tabLocation);
		categoryAccordion.waitForFrameToLoad(Frame.REVIEW_ASSIGNMENTS_FRAME);
		categoryAccordion.waitForNumberOfElementsToBeGreater(By.cssSelector("#assignmentList > li"), 0);
		categoryAccordion.waitFor(5);
		
		System.out.println( Logger.getLineNumber() + "\nReviewAssignment accordion expanded...\n");
	}
	
	
	public static void openSelectedAssignment(){
		openReviewTab();
		reviewAssignment.tryClick(openButtonOnReviewTab);
		reviewAssignment.switchToDefaultFrame();
    	reviewAssignment.waitForFrameToLoad(Frame.MAIN_FRAME);
		reviewAssignment.waitForVisibilityOf(By.id("menu"));
		reviewAssignment.waitForNumberOfElementsToBeGreater(By.cssSelector("#grid > tbody > tr"), 1);
		System.out.println( Logger.getLineNumber() + "\nSelected assignment(s) open for view...\n");
	}
	
	public static void selectAssignment(String assignmentName){
		By assignmentLocation = By.cssSelector("#assignmentList li[style$='block;'] span.name");
		
		
		List<WebElement> assignments = reviewAssignment.getElements(assignmentLocation);
		
		for(int i = 0; i < assignments.size(); i++){
			if(assignments.get(i).getText().trim().contains(assignmentName) && assignments.get(i).isSelected() == false){
				//reviewAssignment.getParentOf(assignments.get(i)).findElement(By.className("chkAssignment")).click();
				reviewAssignment.tryClick(By.cssSelector("#assignmentList li[style$='block;']:nth-child(" + (i + 1) +  ") input.chkAssignment"));
				System.out.println( Logger.getLineNumber() + assignmentName + " selected...");
				break;
			}
		}
	}
	
	public static void openAssignment(String assignmentName){
		searchForAssignment(assignmentName);
		selectAssignment(assignmentName);
		openSelectedAssignment();
	}
	
	public static void addSelectedToSearch(){
		reviewAssignment.tryClick(topAddToSearchBtn);
	} 
	
	public static void selectFirstAssignment(){
		reviewAssignment.tryClick(firstAssignmentLocator, 8);
	}
	
	
	public static void openReviewTab(){
		reviewAssignment.tryClick(reviewTab, 1);
	}
	
	public static void selectAllAssignment(){
		if(reviewAssignment.isElementChecked(selectAllAssignmentsLocator)){
			System.out.println( Logger.getLineNumber() + "All assignments are already selected...");
		}else{
			reviewAssignment.tryClick(selectAllAssignmentsLocator);
			System.out.println( Logger.getLineNumber() + "All assignments are selected...");
		}
	}
	
	
	public static void deSelectAllAssignment(){
		if(reviewAssignment.isElementChecked(selectAllAssignmentsLocator)){
			reviewAssignment.tryClick(selectAllAssignmentsLocator);
			System.out.println( Logger.getLineNumber() + "All assignments are de-selected...");
		}else{
			System.out.println( Logger.getLineNumber() + "All assignments are already de-selected...");
		}
	}
	
	public static void openFilterTab(){
		WebElement filterTabElem = reviewAssignment.getElement(filterTab);
		if(filterTabElem.findElement(By.xpath("..")).getAttribute("aria-expanded").contains("true")){
			System.out.println( Logger.getLineNumber() + "Filter Tab already selected...");
		}else{
			filterTabElem.click();
			System.out.println( Logger.getLineNumber() + "Filter Tab selected...");
		}
		reviewAssignment.waitFor(1);
	}
	
	public static void openFindTab(){
		WebElement findTab = reviewAssignment.getElement(By.cssSelector("li[aria-controls='actionFindAssignments']"));
		if(reviewAssignment.getAttribute(findTab, "aria-expanded").equals("true")){
			System.out.println( Logger.getLineNumber() + "Find tab already active...");
		}else{
			findTab.click();
			reviewAssignment.waitFor(1);
			System.out.println( Logger.getLineNumber() + "Find tab is now active...");
		}
	}
	
	public static void sortAssignmentBy(String sortingCriterion){
		if(reviewAssignment.getSelectedItemFroDropdown(sortAssignmentDropdownLocator).equals(sortingCriterion)){
			System.out.println( Logger.getLineNumber() + "\nAlready sorted by '" + sortingCriterion + "'...");
		}else{
			reviewAssignment.selectFromDrowdownByText(sortAssignmentDropdownLocator, sortingCriterion);
			reviewAssignment.waitForVisibilityOf(refreshMsgLocator);
			reviewAssignment.waitForInVisibilityOf(refreshMsgLocator);
			System.out.println( Logger.getLineNumber() + "\nSorted by '" + sortingCriterion + "'...");
		}
	} 
	
	public static void searchForAssignment(String assignmentName){
		openFindTab();
		reviewAssignment.editText(clearAssignmentFieldLocator, assignmentName);
		reviewAssignment.tryClick(findButtonLocator, searchingAssignmentMsgLocator );
		reviewAssignment.waitForInVisibilityOf(searchingAssignmentMsgLocator);
	}
	
	public static void clearAssignmentField(){
		reviewAssignment.tryClick(clearAssignmentButtondLocator);
		waitForAssignmentsLoadingComplete();
	}
	
	public static void refreshAssignment(){
		reviewAssignment.tryClick(refreshAssignmentLocator);
		reviewAssignment.waitForVisibilityOf(refreshMsgLocator);
		reviewAssignment.waitForInVisibilityOf(refreshMsgLocator);
		reviewAssignment.waitFor(1);
	}
	
	public static void selectFilterByUsers(String filter){
		openFilterTab();
		List<WebElement> userFilters = reviewAssignment.getElements(By.name("displayUserOptions"));
		
		for(WebElement userFilter : userFilters){
			System.out.println( Logger.getLineNumber() + userFilter.findElement(By.xpath("..")).getText().trim()+"*****%%%%");
			if(userFilter.findElement(By.xpath("..")).getText().trim().equalsIgnoreCase(filter) && userFilter.isSelected() == false){
				userFilter.click();
				waitForAssignmentsLoadingComplete();
				System.out.println( Logger.getLineNumber() + "User filter [" + filter + " ] selected...");
				break;
			}else if(userFilter.findElement(By.xpath("..")).getText().trim().equalsIgnoreCase(filter) && userFilter.isSelected()){
				System.out.println( Logger.getLineNumber() + "User filter [" + filter + " ] already selected...");
			}
		}
	}
	
	
	public static void selectAllFilterByUsers(){
		openFilterTab();
		List<WebElement> userFilters = reviewAssignment.getElements(By.name("displayUserOptions"));
		
		for(WebElement userFilter : userFilters){
			String filterName = userFilter.findElement(By.xpath("..")).getText().trim();
			System.out.println( Logger.getLineNumber() + userFilter.findElement(By.xpath("..")).getText().trim()+"*****%%%%");
			if(userFilter.isSelected()){
				System.out.println( Logger.getLineNumber() + "User filter [" + filterName + " ] already selected...");
			}else if(userFilter.isSelected() == false){
				userFilter.click();
				waitForAssignmentsLoadingComplete();
				System.out.println( Logger.getLineNumber() + "User filter [" + filterName + " ] selected...");
			}
		}
	}
	
	public static void selectFilterByStatus(String filter){
		openFilterTab();
		List<WebElement> userFilters = reviewAssignment.getElements(By.name("displayOptions"));
		
		for(WebElement statusFilter : userFilters){
			System.out.println( Logger.getLineNumber() + statusFilter.findElement(By.xpath("..")).getText().trim()+"****%%%");
			if(statusFilter.findElement(By.xpath("..")).getText().trim().equalsIgnoreCase(filter) && statusFilter.isSelected() == false){
				statusFilter.click();
				waitForAssignmentsLoadingComplete();
				System.out.println( Logger.getLineNumber() + "Status filter [" + filter + " ] selected...");
				break;
			}else if(statusFilter.findElement(By.xpath("..")).getText().trim().equalsIgnoreCase(filter) && statusFilter.isSelected()){
				System.out.println( Logger.getLineNumber() + "Status filter [" + filter + " ] already selected...");
			}				
		}
	}
	
	
	public static void selectAllFilterByStatus(){
		openFilterTab();
		
		WebElement statusFilterAll = reviewAssignment.getElement(By.id("optDisplayAll"));
		if(statusFilterAll.isSelected()){
			System.out.println( Logger.getLineNumber() + "All status filters already selected...");
		}else{
			statusFilterAll.click();
			waitForAssignmentsLoadingComplete();
			System.out.println( Logger.getLineNumber() + "All status filters selected...");
		}
	}
	
	public static void deSelectFilterByStatus(String filter){
		openFilterTab();
		List<WebElement> userFilters = reviewAssignment.getElements(By.name("displayOptions"));
		
		for(WebElement statusFilter : userFilters){
			System.out.println( Logger.getLineNumber() + statusFilter.findElement(By.xpath("..")).getText().trim()+"****%%%");
			if(statusFilter.findElement(By.xpath("..")).getText().trim().equalsIgnoreCase(filter) && statusFilter.isSelected()){
				statusFilter.click();
				waitForAssignmentsLoadingComplete();
				System.out.println( Logger.getLineNumber() + "Status filter [" + filter + " ] deselected...");
				break;
			}else if(statusFilter.findElement(By.xpath("..")).getText().trim().equalsIgnoreCase(filter) && statusFilter.isSelected() == false){
				System.out.println( Logger.getLineNumber() + "Status filter [" + filter + " ] already deselected...");
			}				
		}
	}
	
	
	
	
	public static void selectOnlyOneUserFilter(String filter){
		openFilterTab();
		List<WebElement> userFilters = reviewAssignment.getElements(By.name("displayUserOptions"));
		
		for(WebElement userFilter : userFilters){
			System.out.println( Logger.getLineNumber() + userFilter.findElement(By.xpath("..")).getText().trim()+"*****%%%%");
			if(userFilter.findElement(By.xpath("..")).getText().trim().equalsIgnoreCase(filter) && userFilter.isSelected() == false){
				userFilter.click();
				waitForAssignmentsLoadingComplete();
				System.out.println( Logger.getLineNumber() + "User filter [" + filter + " ] selected...");
				break;
			}else if(userFilter.findElement(By.xpath("..")).getText().trim().equalsIgnoreCase(filter) == false && userFilter.isSelected()){
				userFilter.click();
				waitForAssignmentsLoadingComplete();
			}
		}
		
	}
	
	
	public static void selectFilterFromFindTab(String filterName){
		reviewAssignment.selectFromDrowdownByText(findTabFilterLocator, filterName);
		System.out.println( Logger.getLineNumber() + filterName + " selected for Find tab...");
	}
	
	public static void selectFilterCriterionOption(int criterionPosition, String criterionOption){
		reviewAssignment.selectFromDrowdownByText(By.cssSelector("#filterCritContainer > div:nth-child(" + criterionPosition + ") > span > select"), criterionOption);
		System.out.println( Logger.getLineNumber() + "Criterion option [ " + criterionOption + " ] selected...");
	}
	
	public static void setFilterCriterionValue(int criterionPosition, String criterionValue){
		reviewAssignment.editText(By.cssSelector("#filterCritContainer > div:nth-child(" + criterionPosition + ") > span > input"), criterionValue);
		System.out.println( Logger.getLineNumber() + "Criterion value: [ " + criterionValue + " ] inserted...");
	}
	
	public static void waitForAssignmentsLoadingComplete(){
		reviewAssignment.waitForVisibilityOf(searchingAssignmentMsgLocator);
		reviewAssignment.waitForInVisibilityOf(searchingAssignmentMsgLocator);
		reviewAssignment.waitFor(2);
	}
	
	public static boolean isFilteringOk(String filterData){
		boolean isFilterWorked = true;
		while(true){
			List<WebElement> assignedUserNameList = reviewAssignment.getElements(By.cssSelector("li.assignment.visible[style$='block;'] span.assignee"));
			for(WebElement assignedUserName : assignedUserNameList){
				if(reviewAssignment.getText(assignedUserName).equals(filterData) == false){
					return isFilterWorked;
				}
			}
			
			if(reviewAssignment.getCssValue(nextAssignmentsBtnLocator, "display").equals("block")){
				reviewAssignment.tryClick(nextAssignmentsBtnLocator, 5);
			}else{
				break;
			}
		}
		
		return isFilterWorked;
	}
	
	public static int getVisibleAssignmentCount(){
		return reviewAssignment.getTotalElementCount(visibleAssignmentLocator);
	}
}
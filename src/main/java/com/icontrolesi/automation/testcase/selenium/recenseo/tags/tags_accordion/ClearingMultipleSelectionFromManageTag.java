package com.icontrolesi.automation.testcase.selenium.recenseo.tags.tags_accordion;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageTags;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;

public class ClearingMultipleSelectionFromManageTag extends TestHelper{
	int totalItemsToSelect = getRandomNumberInRange(2, 5);
	
	@Test
	public void test_c1801_ClearingMultipleSelectionFromManageTag(){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Search");
		
		TagAccordion.open();
		
		ManageTags.open();
		
		List<WebElement> checkBoxList = getElements(ManageTags.tagItemCheckboxLocator);
		
		boolean isAllItemsChecked = true;
		
		for(int i = 0; i < totalItemsToSelect; i++){
			checkBoxList.get(i).click();
			waitFor(5);
			
			checkBoxList = getElements(ManageTags.tagItemCheckboxLocator);
			
			if(!getParentOf(checkBoxList.get(i)).getAttribute("class").contains("checked")){
				isAllItemsChecked = false;
				break;
			}
		}
		
		tryClick(ManageTags.clearSelectionLocator, 1);
		
		boolean isAllItemsDeselected = true;
				
		for(int i = 0; i < totalItemsToSelect; i++){
			if(getParentOf(checkBoxList.get(i)).getAttribute("class").contains("checked")){
				isAllItemsDeselected = false;
				break;
			}
		}
		
		softAssert.assertTrue(isAllItemsChecked, "***) Item is checked before clearing:: ");			
		softAssert.assertTrue(isAllItemsDeselected, "***) Item is unchecked after clearing:: ");
		
		softAssert.assertAll();
	}
}

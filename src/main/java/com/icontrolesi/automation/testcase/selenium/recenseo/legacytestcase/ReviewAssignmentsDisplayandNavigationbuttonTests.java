package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


/*
 * "DB: DEM0
    (1) Go to search page.  Open the Review Assignments Section.
    (2) Choose the Search Workflow
    (3) You should see all assignments that are assigned to you, in all workflows, and all assignments for people that you have permission to assign to
    (4) Click the Next 25 button until there are no more assignments, and make sure it goes away on the last page. Then click the Previous 25 button and make sure it disappears on the first page
    (5) Change to the Issue Review Workflow
    (6) You should only see assignments in the Issue Review workflow state that are assigned to you or assignments for people that you have permission to assign to in Issue Review."
 *
 *
 *
 */

public class ReviewAssignmentsDisplayandNavigationbuttonTests extends TestHelper{
   @Test
   public void test_c1654_ReviewAssignmentsDisplayandNavigationbuttonTests(){
	   //loginToRecenseo();
	   handleReviewAssignmentsDisplayandNavigationbuttonTests(driver);
   }


    protected void handleReviewAssignmentsDisplayandNavigationbuttonTests(WebDriver driver){
        new SelectRepo(AppConstant.DEM0);
        
        SearchPage.setWorkflow("Search");
        
        ReviewAssignmentAccordion.open();
       
        List<WebElement> assignmentsNotInWorkFlowState = getElements(By.cssSelector("#assignmentList > li[style$='block;'] > div > div:nth-child(2) > span.not-state"));
       
        boolean isAssignmentsNotInStateOk = true;
                
        int totalNextBtnClicked = 0;
        while(true){
        	for(WebElement workflowNotInState : assignmentsNotInWorkFlowState){
                if(workflowNotInState.getText().trim().equals("Search")){
                	isAssignmentsNotInStateOk = false;
                	break;
                }
        	}
        	
        	WebElement nextBtn = getElement(By.id("nextPage"));
        	if(isAssignmentsNotInStateOk == false || nextBtn.getCssValue("display").equals("none"))
        		break;
        	nextBtn.click();
        	ReviewAssignmentAccordion.waitForAssignmentsLoadingComplete();
        	assignmentsNotInWorkFlowState = getElements(By.cssSelector("#assignmentList > li[style$='block;'] > div > div:nth-child(2) > span.not-state"));
        	totalNextBtnClicked++;
        }
        
        
        List<WebElement> assignmentsInWorkFlowState = getElements(By.cssSelector("#assignmentList > li[style$='block;'] > div > div:nth-child(2) > span.in-state"));
        
        boolean isAssignmentsInStateOk = true;
           
        int totalPreviousBtnClicked = 0;
        while(true){
        	for(WebElement workflowInState : assignmentsInWorkFlowState){
                if(workflowInState.getText().trim().equals("Search") == false){
                	isAssignmentsInStateOk = false;
                	break;
                }
            }
        	
        	WebElement nextBtn = getElement(By.id("previousPage"));
        	if(isAssignmentsInStateOk == false || nextBtn.getCssValue("display").equals("none"))
        		break;
        	nextBtn.click();
        	ReviewAssignmentAccordion.waitForAssignmentsLoadingComplete();
        	assignmentsInWorkFlowState = getElements(By.cssSelector("#assignmentList > li[style$='block;'] > div > div:nth-child(2) > span.in-state"));
        	totalPreviousBtnClicked++;
        }
        
        
        System.out.println( Logger.getLineNumber() + isAssignmentsInStateOk +", " + isAssignmentsNotInStateOk);

        softAssert.assertTrue(isAssignmentsInStateOk && isAssignmentsNotInStateOk, "You should see all assignments that are assigned to you, in all workflows, and all assignments for people that you have permission to assign to:: ");

        System.out.println( Logger.getLineNumber() + "Next button disappeared after " + totalNextBtnClicked + " click(s)...");
        
        System.out.println( Logger.getLineNumber() + "Previous button disappeared after " + totalPreviousBtnClicked + " click(s)...");
        
        softAssert.assertEquals(totalNextBtnClicked, totalPreviousBtnClicked, "(4) Click the Next 25 button until there are no more assignments, and make sure it goes away on the last page. Then click the Previous 25 button and make sure it disappears on the first page:: ");
        
        switchToDefaultFrame();
        SearchPage.setWorkflow("Issue Review");

        assignmentsInWorkFlowState = getElements(By.cssSelector("#assignmentList > li[class$='visible'] > div > div:nth-child(2) > span.in-state"));
        
        isAssignmentsInStateOk = true;
        for(WebElement workflowInState : assignmentsInWorkFlowState){
            if(workflowInState.getText().trim().equals("Search") == false){
            	isAssignmentsInStateOk = false;
            	break;
            }
        }
        
        assignmentsNotInWorkFlowState = getElements(By.cssSelector("#assignmentList > li[class$='visible'] > div > div:nth-child(2) > span.not-state"));
        
        softAssert.assertTrue(isAssignmentsInStateOk && (assignmentsNotInWorkFlowState.size() == 0), "(6) You should only see assignments in the Issue Review workflow state that are assigned to you or assignments for people that you have permission to assign to in Issue Review.:: ");
        
        softAssert.assertAll();
    }
}
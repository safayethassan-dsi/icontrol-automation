package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;
/**
 *
 * @author Shaheed Sagar
 *
 *    "DB: DEM0
    (1) On Settings\General Preferences choose the option to Expand all pick lists 
    (2) Return to document and confirm all Pick lists are shown expanded
    (3) On Settings\General Preferences unselect the option to Expand all pick lists 
    (4)  Return to document and confirm all Pick Lists only show options already selected"
 */

public class DbdPickLists extends TestHelper{
	@Test
	public void test_c238_DbdPickLists(){
		handleDbdPickLists(driver);
	}

    private void handleDbdPickLists(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
    	
    	DocView.openPreferenceWindow();
    	DocView.enableExpandPickList();
    	DocView.clickCrossBtn();
    	
        SearchPage.setWorkflow("Privilege Review");
       
	    performSearch();
       
        boolean isAllExpanded = true;
        List<WebElement> showLessElements = getElements(By.cssSelector("a[class='ui-moreless-button']"));
        for(WebElement element : showLessElements){
        	System.out.println( Logger.getLineNumber() + element.getAttribute("textContent").trim()+"****");
        	if(element.getAttribute("textContent").trim().equals("- Show Less") == false){
        		isAllExpanded = false;
        		break;
        	}
        }
       
        softAssert.assertTrue(isAllExpanded, "(2) Return to document and confirm all Pick lists are shown expanded:: ");
       
        switchToDefaultFrame();
        
        DocView.openPreferenceWindow();
    	DocView.disableExpandPickList();
    	DocView.clickCrossBtn();
        
        boolean isAllCollapsed = true;
        showLessElements = getElements(By.cssSelector("a[class='ui-moreless-button']"));
        for(WebElement element : showLessElements){
        	if(element.getAttribute("textContent").trim().equals("+ Show All") == false){
        		isAllCollapsed = false;
        		break;
        	}
        }
        
        softAssert.assertTrue(isAllCollapsed, "(4)  Return to document and confirm all Pick Lists only show options already selected:: ");
        
        softAssert.assertAll();
    }
}
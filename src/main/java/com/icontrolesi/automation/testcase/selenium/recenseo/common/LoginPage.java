package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;

public class LoginPage implements CommonActions{
	
	public static void setUserName(String userName){
		Utils.waitForElement(Driver.getDriver(), "j_username", "id").sendKeys(userName);
	}
	
	public static void setPassword(String password){
		Utils.waitForElement(Driver.getDriver(), "j_password", "id").sendKeys(password);
	}
	
	public void clickOnForgotYourPassowrd(){
		WebElement forgetDiv = Utils.waitForElement(Driver.getDriver(), "forgot", "id");
		WebElement forgetLink = forgetDiv.findElement(By.tagName("a"));
		click(forgetLink);
	}
	
	public void clickOnLoginButton(){
		click(Utils.waitForElement(Driver.getDriver(), "input[class='login-button ui-button ui-widget ui-state-default ui-corner-all']", "css-selector"));
		Utils.waitShort();
	}
	
	
	public boolean checkAuthenticationFailure(){
		
		boolean failureMessagePresent=false;
		
		try {
	        
	       
           WebElement failure= Driver.getDriver().findElement(By.cssSelector("h1"));
           if(failure.getText().equalsIgnoreCase("iControl ESI -- Authentication Failed")){
        	   
        	   failureMessagePresent=true;
        	   
        	   
           }
           else{
        	   
        	   
        	   
        	   
           }
	        
	    } catch (Exception e) {
	        //exception handling
	    }
		
		
		return failureMessagePresent;
		
	}//end function
	
	
}

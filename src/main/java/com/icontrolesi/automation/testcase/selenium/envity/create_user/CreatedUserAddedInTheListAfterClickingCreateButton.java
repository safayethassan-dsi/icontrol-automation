package com.icontrolesi.automation.testcase.selenium.envity.create_user;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.UsersListPage;
import org.testng.annotations.Test;

import java.util.Date;

public class CreatedUserAddedInTheListAfterClickingCreateButton extends TestHelper {
        String possibleValidUserName = "AutoUser_" + new Date().getTime();
        String email = String.format("%s%s", possibleValidUserName, "@automation.com");

        @Test(description = "When clicked on 'Create' button, it returns to User List and user is added to the list.")
        public void  test_c277_CreatedUserAddedInTheListAfterClickingCreateButton(){
            ProjectPage.gotoUserAdministrationPage();

            UsersListPage.openCreateUserWindow();

            UsersListPage.CreateUser.perform(email, possibleValidUserName, "@3455466@EEEE13f", "@3455466@EEEE13f", "Abdul Awal", "Sazib", "Project Reviewer", "iControl");

            boolean isUserCreate = UsersListPage.isUserExist(possibleValidUserName);

            UsersListPage.deleteUser(possibleValidUserName);

            softAssert.assertTrue(isUserCreate, "***) User Created and listed in the table:: ");

            softAssert.assertAll();
        }
}

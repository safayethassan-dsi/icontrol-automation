package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ChangePassword;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;

import com.icontrolesi.automation.platform.util.Logger;

public class AuthenticateChangePassword extends TestHelper{
	String timeStamp = new Date().getTime() + "";
	
	@Test(enabled = false)
	public void test_c1539_AuthenticateChangePassword(){
		handleChangePassword(driver);
	}

	private void handleChangePassword(WebDriver driver){
		String newPassword = Configuration.getConfig("recenseo.newpassword") + timeStamp;
		SearchPage.openChangePassword(); 
		
		ChangePassword cp = new ChangePassword();
		
		System.out.println( Logger.getLineNumber() + "\n\nPassword (NEW) is being set...");
		cp.performPasswordChange(AppConstant.USER, newPassword);
		 
		SearchPage sp = new SearchPage();
		driver.switchTo().defaultContent();
		sp.clickCloseButton(driver, sp);
		
		SearchPage.logout();
		
		//Relogin with the new pass
		//SearchPage.waitForElement(driver, ELEMENT_USER_NAME, "name");
		
	    System.out.println( Logger.getLineNumber() + "Login with new password [" + newPassword + "]...");
	    //userName.sendKeys(Configuration.getConfig("recenseo.username"));
	    editText(By.id(ELEMENT_USER_NAME), Configuration.getConfig("recenseo.username"));
	    //password.sendKeys(Configuration.getConfig("recenseo.newpassword"));
	    editText(By.id(ELEMENT_PASSWORD), newPassword);
	    enterText(By.id(ELEMENT_PASSWORD), "\n");
	    
		//Check if relogin is successful
	    waitForFrameToLoad(driver, Frame.MAIN_FRAME);
	    boolean isReLoginSuccessful = getElement(By.id("sortOrder")).getTagName().equals("select")
	    								&& getElements(By.id("forgot")).size() == 0;
	    
	    Assert.assertTrue(isReLoginSuccessful, "(3)Once Password is changed, log out of Recenseo and then attempt to log back in with new password:: ");
		
		System.out.println( Logger.getLineNumber() + "Login with new password successful...");
		
		//Change password back to what it was before
	    SearchPage.openChangePassword(driver); 
		
	    System.out.println( Logger.getLineNumber() + "Password is being changed to its original state...");
	    cp.performPasswordChange(Configuration.getConfig("recenseo.newpassword"), AppConstant.PASS);
	 }
}

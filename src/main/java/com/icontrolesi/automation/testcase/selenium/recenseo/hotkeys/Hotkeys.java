package com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;

import com.icontrolesi.automation.platform.util.Logger;

public class Hotkeys implements CommonActions{
	public static By activateHotkeysChkBoxLocator = By.id("activateHotkeys");
	public static By restoreDefaultLocator = By.id("btnDefault");
	
	public static By viewTextKey = By.id("view-text_key");
	public static By viewImagedKey = By.id("view-imaged_key");
	public static By viewNativeKey = By.id("view-native_key");
	public static By viewHistoryKey = By.id("view-history_key");
	public static By toggleViewModeKey = By.id("toggle-view-mode_key");
	public static By saveDocKey = By.id("save-doc_key");
	public static By postDocKey = By.id("post-doc_key");
	public static By prevDocKey = By.id("prev-doc_key");
	public static By nextDocKey = By.id("next-doc_key");
	public static By firstDocKey = By.id("first-doc_key");
	public static By lastDocKey = By.id("last-doc_key");
	public static By prevPageKey = By.id("prev-page_key");
	public static By prevTermKey = By.id("prev-term_key");
	public static By nextTermKey = By.id("next-term_key");
	
	public static Hotkeys hotKeys = new Hotkeys();
	
	public static void open(){
		DocView.openPreferenceWindow();
		new Hotkeys().tryClick(By.xpath("//a[text()='Hotkeys']"), By.id("hotkeys"));
		System.out.println( Logger.getLineNumber() + "Hotkeys window opened...");
	}
	
	public static void close(){
		DocView.closePreferenceWindow();
	}
	
	public static void enableHotkeys(){
		if(hotKeys.isElementChecked(activateHotkeysChkBoxLocator)){
			System.out.println( Logger.getLineNumber() + "Hotkeys already enabled...");
		}else{
			hotKeys.tryClick(activateHotkeysChkBoxLocator, 1);
			System.out.println( Logger.getLineNumber() + "Hotkeys are enabled...");
		}
	}
	
	public static void setDefaultHotkeys(){
		hotKeys.tryClick(restoreDefaultLocator);
		System.out.println( Logger.getLineNumber() + "Default Hotkeys set...");
	}
	
	public static void disableHotkeys(){
		if(hotKeys.isElementChecked(activateHotkeysChkBoxLocator) == false){
			System.out.println( Logger.getLineNumber() + "Hotkeys already disabled...");
		}else{
			hotKeys.tryClick(activateHotkeysChkBoxLocator, 1);
			System.out.println( Logger.getLineNumber() + "Hotkeys are disabled...");
		}
	}
	
	public static void pressHotkey(boolean altCtl, String hotkeyValue){
		Actions actions = new Actions(Driver.getDriver());
		
		if(altCtl){
			actions.keyDown(Keys.ALT).keyDown(Keys.CONTROL).sendKeys(hotkeyValue).keyUp(Keys.ALT).keyUp(Keys.CONTROL).perform();
			System.out.println( Logger.getLineNumber() + "Hotkey pressed: ALT + CTRL + " + hotkeyValue + "...");
		}else{
			actions.keyDown(Keys.ALT).sendKeys(hotkeyValue).build().perform();
			System.out.println( Logger.getLineNumber() + "Hotkey pressed: ALT + " + hotkeyValue + "...");
		}
		
		hotKeys.waitFor(1);
	}
	

	public static void pressHotkey(boolean altCtl, Keys hotkeyValue){
		Actions actions = new Actions(Driver.getDriver());
		
		if(altCtl){
			actions.keyDown(Keys.ALT).keyDown(Keys.CONTROL).sendKeys(hotkeyValue).keyUp(Keys.ALT).keyUp(Keys.CONTROL).build().perform();
			System.out.println( Logger.getLineNumber() + "Hotkey pressed: ALT + CTRL + " + hotkeyValue.toString() + "...");
		}else{
			actions.keyDown(Keys.ALT).sendKeys(hotkeyValue).keyUp(Keys.ALT).build().perform();
			System.out.println( Logger.getLineNumber() + "Hotkey pressed: ALT + " + hotkeyValue.toString() + "...");
		}
	}
	
	
	public static void setPersonalizedHotkey(By element, String value){
		hotKeys.selectFromDrowdownByText(element, value);
		System.out.println( Logger.getLineNumber() + element.toString().split("_")[0].toUpperCase() + " is changed to '" + value + "'...");
	}
}

package com.icontrolesi.automation.testcase.selenium.envity.instances_tab;

import java.io.File;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.GeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.security.Credentials;
import org.openqa.selenium.security.UserAndPassword;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.Test;

public class EveryColumnDisplayedProperly extends TestHelper {
    @Test(description = "Workspace, Project Type, Last Modified, Job Status & Application Instance are displayed properly for each of the projects")
    public void test_c1840_EveryColumnDisplayedProperly(){
        ProjectPage.gotoGeneralConfigurationPage();
        GeneralConfigurationPage.Instances.gotoInstancePage();

        testColumnHeaders();        

        softAssert.assertAll();
    }

    void testColumnHeaders(){
        softAssert.assertEquals(getText(GeneralConfigurationPage.Instances.INSTANCE_NAME), "Instance Name", "Instance Name column exists:: ");
        softAssert.assertEquals(getText(GeneralConfigurationPage.Instances.IDENTIFIER), "Identifier", "Identifier column exists:: ");
        softAssert.assertEquals(getText(GeneralConfigurationPage.Instances.APP_URL), "App Url", "App Url column exists:: ");
        softAssert.assertEquals(getText(GeneralConfigurationPage.Instances.LAST_STARTED_TIME), "Last Started Time", "Last Started Time column exists:: ");
        softAssert.assertEquals(getText(GeneralConfigurationPage.Instances.LOCKED), "Locked", "Locked column exists:: ");
        softAssert.assertEquals(getText(GeneralConfigurationPage.Instances.STATUS), "Status", "Status column exists:: ");
        softAssert.assertEquals(getText(GeneralConfigurationPage.Instances.SEVENTH_COLUMN_HEADER), "", "Seventh Column column exists without any text:: ");
    }

    public void test(){
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.MINUTES);
        Wait wait = new FluentWait<WebDriver>(driver)
                        .withTimeout(30, TimeUnit.MINUTES)
                        .pollingEvery(5, TimeUnit.SECONDS)
                        .ignoring(NoSuchElementException.class);

        driver.findElement(By.className("test")).sendKeys(Keys.ENTER);
        Actions actions = new Actions(driver);
        actions.sendKeys(Keys.ENTER);
        Select select = new Select(driver.findElement(By.name("")));
        File screenShot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        Credentials credentials = new UserAndPassword(PROP_USERNAME, ELEMENT_PASSWORD);
        driver.switchTo().alert().authenticateUsing(credentials);
    }
}
package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
/**
 *
 * @author Shaheed Sagar
 *
 *  "DB: DEM0 
 
	1) Click on the Settings\General Preferences 
	2) Set your Search Order Preferences as follows: 
	   FIRST: Document Date - Ascending 
	   SECOND: Author - Descending (reverse alphabetical)  
	   THIRD: Title (or Subject) - Ascending 
	3) On the Search page, click on the Add Search Criteria drop down menu and choose 'filename' 'does not contain' 'paper' then hit run search 
	4) If not already displayed, set up Grid view to to display Doc Date, Author, and Title 
	5) Verify ALL of the following are true: 
	   5a) Documents are in ascending (oldest to newest) order by Doc Date 
	   5b) For documents that have NO Doc Date, the Authors are in descending (reverse alphabetical) order.  
	   5c) For the docs with no Doc Date and the same Author, the Titles should be in ascending (alphabetical) order 
	   5c) For documents with no Doc Date and no Author, the Titles are in ascending (alphabetical) order"
*/

public class UtiltiesPrefSortSetting extends TestHelper{
	final String documentDate = "Document Date";
	final String documentAuthor = "Author";
	final String documentTitle = "Title";
    @Test
    public void test_c124_UtiltiesPrefSortSetting(){
    	//loginToRecenseo();
    	handleUtiltiesPrefSortSetting();
    }
    
    
    private void handleUtiltiesPrefSortSetting(){
        new SelectRepo(AppConstant.DEM0);
       
        SearchPage.setWorkflow("Search");
        
        DocView.openPreferenceWindow();
        DocView.deselectAllDocument();
        DocView.checkSelectedColumns(documentDate, documentAuthor, documentTitle);
        
        if(!getSelectedItemFroDropdown(By.id("SRT1")).equals(documentDate))
        	selectFromDrowdownByText(By.id("SRT1"), documentDate);
        if(!getSelectedItemFroDropdown(By.id("SRT2")).equals(documentAuthor))
        	selectFromDrowdownByText(By.id("SRT2"), documentAuthor);
        if(!getSelectedItemFroDropdown(By.id("SRT3")).equals(documentTitle))
        	selectFromDrowdownByText(By.id("SRT3"), documentTitle);
        
        DocView.closePreferenceWindow();
        
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        SearchPage.addSearchCriteria("filename");
        SearchPage.setOperatorValue(0, "does not contain");
        SearchPage.setCriterionValue(0, "paper");
        
        performSearch();
        
        DocView.setNumberOfDocumentPerPage(500);

        List<WebElement> allDates = getElements(DocView.documentDateColumnLocator);
        
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        Date previousDate = null;
        boolean allOk = true;
        
        try {
			 previousDate = simpleDateFormat.parse(getText(allDates.get(0)));
			 
			 for(int i = 1; i < allDates.size(); i++){
				 Date date = simpleDateFormat.parse(getText(allDates.get(i)));
				 
				 if(date.compareTo(previousDate) < 0){
					 allOk = false;
					 break;
				 }
				 
				 previousDate = date;
			 }
		} catch (ParseException e2) {
			e2.printStackTrace();
		}
        
        
        softAssert.assertTrue(allOk, "5a) Documents are in ascending (oldest to newest) order by Doc Date:: ");
        
        String authorsSortingClassAscending = getAttribute(By.cssSelector("#jqgh_grid_auth > span > span[sort='asc']"), "class");
        String titleSortingClassAscending = getAttribute(By.cssSelector("#jqgh_grid_subj > span > span[sort='asc']"), "class");
        
        softAssert.assertFalse(authorsSortingClassAscending.contains("disabled"), "5b) For documents that have NO Doc Date, the Authors are in descending (reverse alphabetical) order:: ");
        softAssert.assertFalse(titleSortingClassAscending.contains("disabled"), "5c) For the docs with no Doc Date and the same Author, the Titles should be in ascending (alphabetical) order:: ");
        
        softAssert.assertAll();
    }
}

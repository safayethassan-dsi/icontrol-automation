package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/**
 * 
 * @author Shaheed Sagar
 *
 *	"DB: DEMO 

 */

public class ManUserDisplayFilterSelection extends TestHelper{
	@Test(enabled = false)
	public void test_c1672_ManUserDisplayFilterSelection(){
		handleManUserDisplayFilterSelection(driver);
	}
	
	private void handleManUserDisplayFilterSelection(WebDriver driver){
		new SelectRepo("Recenseo Demo DB");
		
		DocView.openPreferenceWindow();
		
		//Check tab text
		int countTab=0;
		
		try{
			Driver.getDriver().findElement(By.id("users_anchor"));
			countTab++;
		}catch(NoSuchElementException e){
			countTab--;
		}
		
		
       try{
			Driver.getDriver().findElement(By.id("teams_anchor"));
			countTab++;
		}catch(NoSuchElementException e){
			countTab--;
		}
		
		
		/*if(countTab==2){
			Utils.handleResult("2(a) The interface displays two tabs (Users and Teams)", result, true);
		}else{
			Utils.handleResult("2(a) The interface displays two tabs (Users and Teams)", result, false);
		}*/
		
		
		//Check whether tab contents are visible
		int elementsVisible=0;
		
        try{
			Driver.getDriver().findElement(By.id("loadAccesses"));
			elementsVisible=1;;
		}catch(NoSuchElementException e){}
		
        
        
     try{
		 Driver.getDriver().findElement(By.id("teams_anchor")).click();
			waitForFrameToLoad(driver, Frame.TEAMS_FRAME);
			Driver.getDriver().findElement(By.id("createTeam"));
			elementsVisible=1;;
		}catch(NoSuchElementException e){
			elementsVisible=0;
		}
        
		
     
     /*if(elementsVisible==1){
    	 Utils.handleResult("2(a) that each tab displays its elements cleanly", result, true);
     } else{
    	 Utils.handleResult("2(a) that each tab displays its elements cleanly", result, false);
      }*/
 	}//end function
}

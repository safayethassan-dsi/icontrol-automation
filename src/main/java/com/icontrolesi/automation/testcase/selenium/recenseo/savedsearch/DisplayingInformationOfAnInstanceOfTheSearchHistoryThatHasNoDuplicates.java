package com.icontrolesi.automation.testcase.selenium.recenseo.savedsearch;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

public class DisplayingInformationOfAnInstanceOfTheSearchHistoryThatHasNoDuplicates extends TestHelper{
	By todayItemLocator = By.cssSelector("#today > ul > li");
	
	String searchCriteria = "Author"; 
	
	String stepMsg8 = "8. Recenseo populates all the search criteria of the selected 'Search History' search on the Search Page ( %s )::";
	
	@Test
	public void test_c44_DisplayingInformationOfAnInstanceOfTheSearchHistoryThatHasNoDuplicates(){
		handleDisplayingInformationOfAnInstanceOfTheSearchHistoryThatHasNoDuplicates();
	}
	
	private void handleDisplayingInformationOfAnInstanceOfTheSearchHistoryThatHasNoDuplicates(){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		SearchesAccordion.open();
		SearchesAccordion.openSearchHistoryTab();
		
		List<WebElement> historyItemList = getElements(SearchesAccordion.todayItemLocator);
		Set<String> savedHistoryItems = new TreeSet<String>();
		for(WebElement historyItem : historyItemList){
			System.out.println( Logger.getLineNumber() + getText(historyItem)+"****");
			savedHistoryItems.add(getText(historyItem).split(",")[0]);
		}
		
		Set<String> criteriaToBeUsed = new TreeSet<>(Arrays.asList("Author", "Addressee", "BCC", "Case Name", "Comment", "Custodian"));
		
		for(String s : savedHistoryItems){
			System.out.println( Logger.getLineNumber() + s+"****");
		}
		
		
		for(String criterionName : criteriaToBeUsed){
			if(savedHistoryItems.contains(criterionName)){
				System.out.println( Logger.getLineNumber() + "Search History exists with name: " + criterionName);
			}else{
				switchToDefaultFrame();
				waitForFrameToLoad(Frame.MAIN_FRAME);
				SearchPage.addSearchCriteria(criterionName);
				performSearch();
				SearchPage.goToDashboard();
				System.out.println( Logger.getLineNumber() + "New Search History added with name: " + criterionName);
				waitForFrameToLoad(Frame.MAIN_FRAME);
				SearchesAccordion.open();
				SearchesAccordion.openSearchHistoryTab();
				break;
			}
		}
		
		
		hoverOverElement(todayItemLocator);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		WebElement addCriteriaToSearchBuilder = driver.findElement(SearchesAccordion.savedSearchInfoDetailLocator);
		
		jse.executeScript("arguments[0].click();", addCriteriaToSearchBuilder);
		waitFor(5);
		
		String infoHeader = getText(By.cssSelector("div[aria-describedby='recentInfoDialog'] >  div > span"));
		
		String runDateLabel = getText(By.id("runDateDiv"));
		
		String runTimeLabel = getText(By.cssSelector("#recentInfoDialog >div:nth-of-type(2) > label"));
		
		String resultCount = getText(By.cssSelector("#recentInfoDialog >div:nth-of-type(2) > label:nth-of-type(2)"));
		
		String criteria = getText(By.cssSelector("div > span[id='criteria']"));
		
		softAssert.assertEquals(infoHeader, "Info", String.format(stepMsg8, "***. Info Header"));
		softAssert.assertTrue(runDateLabel.startsWith("Run date: "), String.format(stepMsg8, "a. Run Time (run data within the same day will be consolidated for the same search criteria)"));
		softAssert.assertTrue(resultCount.startsWith("Result count:"), String.format(stepMsg8, "b. Run Time (run data within the same day will be consolidated for the same search criteria)"));
		softAssert.assertTrue(runTimeLabel.startsWith("Run time:"), String.format(stepMsg8, "c. Result Count (run data within the same day will be consolidated for the same search criteria)"));
		softAssert.assertTrue(criteria.startsWith("Criteria:"), String.format(stepMsg8, "d. Criteria"));
			
		softAssert.assertAll();	
			
		softAssert.assertAll();		
	}
}

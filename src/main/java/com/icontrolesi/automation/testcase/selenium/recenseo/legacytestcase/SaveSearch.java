package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/**
 * @author atiq,ishtiyaq
 * DB: DEM4 
(1) Run a Search with at least 4 criteria and 1 logical group   
(2) Return to search page, Confirm Recenseo accurately displays the criteria for your last search.   
(3) Click on the Save Search (disk icon) to save current search parameters.  
(4) Give the Search the name YYYYMMDDSCH0005  
(5) Clear the Search Criteria  
(6) Navigate to Saved Searches, find and open the newly saved search.  Verify Recenseo correctly displays your search's criteria.  
(7) Run your search and verify correct results.  
(8) Edit your search, and hit the Save Search icon, to save your changes.  
(9) Verify that Recenseo does not save a second search, using the same name but instead overwrites the original search.  
(10) Log out of the test DB, Then log back in, and open the saved search.  
(11) Verify that Recenseo accurately displays the EDITED crieteria.  
(12) Run your search and verify correct results. 
(13) While Saved Search Tab is open, switch databases and confirm the new database opens showing the Saved Searches and no error occurs
 * 
 */

public class SaveSearch extends TestHelper{
	DateFormat dateFormat = new SimpleDateFormat("YYYYMMDD_hhmmss");
	String timeStamp = dateFormat.format(Calendar.getInstance().getTime());
	String savedSearchName = timeStamp + "SCH005";
	
	
   @Test
   public void test_c104_SaveSearch(){
	   //loginToRecenseo();
	   handleSaveSearch(driver);
   }

  	
   protected void handleSaveSearch(WebDriver driver){
		new SelectRepo(AppConstant.DEM4);
		
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Addressee", "Author", "Document Type", "Document ID");
		DocView.clickCrossBtn();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Addressee");
		SearchPage.addSearchCriteria("Author");
		SearchPage.addSearchCriteria("Document Type");
		SearchPage.addSearchCriteria("Document ID");
		
		SearchPage.setOperatorValue(0, "is not empty");
		SearchPage.setOperatorValue(1, "is not empty");
		SearchPage.setOperatorValue(3, "is greater than");
		
		SearchPage.setCriterionValue(0, "Email");
		SearchPage.setCriterionValue(1, "1000");
		
	  	performSearch();
		
		int totalDocsForSearch = DocView.getDocmentCount();  
	
		SearchPage.goToDashboard();
	   
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		checkForCriterionLabels();
		checkForCriterionOperators();
		checkForCriterionValues();
        
		SearchPage.createSavedSearch(savedSearchName, "", "");
		
		SearchPage.clearSearchCriteria();
	    
		SearchesAccordion.open();
		SearchesAccordion.addSelectedToSearch(savedSearchName);
	   
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
	   
		checkForCriterionLabels();
		checkForCriterionOperators();
	    checkForCriterionValues();
	    
	    performSearch();
	    
	    softAssert.assertEquals(DocView.getDocmentCount(), totalDocsForSearch, "7) Run your search and verify correct results:: ");
	    
	    SearchPage.goToDashboard();
	    waitForFrameToLoad(Frame.MAIN_FRAME);
	    
	    //SearchPage.setCriterionValue(2, "Technical Specification");
	    SearchPage.setCriterionValue(1, "500");
	    
	    hoverOverElement(SearchPage.searchBoxLocator);
        tryClick(SearchPage.saveSearchBtnLocator, SearchPage.saveSearchNameLocator);
        editText(SearchPage.saveSearchNameLocator, savedSearchName);
        tryClick(SearchPage.savedSearchContinueBtnLocator);
	   
        String alertMsg = getAlertMsg();
	    
	    softAssert.assertEquals(alertMsg, "A saved search with same name/description already exists. Overwrite existing saved search?", "9) Verify that Recenseo does not save a second search, using the same name:: ");
	    
	    SearchesAccordion.open();
	    SearchesAccordion.searchForSavedSearch(savedSearchName);
	    
	    int totalSavedSearchWithSpecificName = SearchesAccordion.getSavedSearchCount(savedSearchName);
	    
	    softAssert.assertEquals(totalSavedSearchWithSpecificName, 1, "9) Original search is overwritten:: ");
	    
	    switchToDefaultFrame();
	    waitForFrameToLoad(Frame.MAIN_FRAME);
	    SearchPage.clearSearchCriteria();
	    
	    SearchPage.logout();
	    performLoginToRecenseo();
	    
	    new SelectRepo(AppConstant.DEM4);
		
		SearchPage.setWorkflow("Search");
		
		SearchesAccordion.open();
		SearchesAccordion.addSelectedToSearch(savedSearchName);
		
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		checkForCriterionLabels();
		checkForCriterionOperators();
		
		List<WebElement> criterionValueList = getElements(By.cssSelector(".CriterionInput  input[style$='block;']"));
        
        String docTypeValue = getText(criterionValueList.get(0));
        String docIdValue = getText(criterionValueList.get(1));
	    
        softAssert.assertEquals(docTypeValue, "Email");
        softAssert.assertEquals(docIdValue, "500");
        
        performSearch();
        
        softAssert.assertTrue(DocView.getDocmentCount() <= totalDocsForSearch, "12) Run your search and verify correct results:: ");
	    
        SearchPage.goToDashboard();
        SelectRepo.selectRepoFromSecondaryMenu("Recenseo Demo DB");
        
        boolean isSavedSearchExpanded = getAttribute(SearchesAccordion.tabLocation, "aria-expanded").equals("true");
        
        softAssert.assertTrue(isSavedSearchExpanded, "13) While Saved Search Tab is open, switch databases and confirm the new database opens showing the Saved Searches and no error occur:: ");        
        
	    softAssert.assertAll();
	}

	public void checkForCriterionLabels(){
		List<WebElement> labels = getElements(By.cssSelector(".CriterionField > span"));
		
		String AddresseeOperator = getText(labels.get(0));
        String AuthorOperator = getText(labels.get(1));
        String DocTypeOperator = getText(labels.get(2));
        String DocIdOperator = getText(labels.get(3));
        
        softAssert.assertEquals(AddresseeOperator, "Addressee");
        softAssert.assertEquals(AuthorOperator, "Author");
        softAssert.assertEquals(DocTypeOperator, "Document Type");
        softAssert.assertEquals(DocIdOperator, "Document ID");
	}
	
	public void checkForCriterionOperators(){
	    List<WebElement> criterionOperatorList = getElements((By.className("CriterionOperator")));
       
        String AddresseeOperatorValue = getSelectedItemFroDropdown(criterionOperatorList.get(0));
        String AuthorOperatorValue = getSelectedItemFroDropdown(criterionOperatorList.get(1));
        String DocTypeOperatorValue = getSelectedItemFroDropdown(criterionOperatorList.get(2));
        String DocIdOperatorValue = getSelectedItemFroDropdown(criterionOperatorList.get(3));
        
        softAssert.assertEquals(AddresseeOperatorValue, "is not empty");
        softAssert.assertEquals(AuthorOperatorValue, "is not empty");
        softAssert.assertEquals(DocTypeOperatorValue, "contains");
        softAssert.assertEquals(DocIdOperatorValue, "is greater than");
	}
	
	public void checkForCriterionValues(){
		//waitForVisibilityOf(By.cssSelector(".CriterionInput  input[style$='block;']"));
		List<WebElement> criterionValueList = getElements(By.cssSelector(".CriterionInput  input[style$='block;']"));
        
        String docTypeValue = getText(criterionValueList.get(0));
        String docIdValue = getText(criterionValueList.get(1));
	    
        softAssert.assertEquals(docTypeValue, "Email");
        softAssert.assertEquals(docIdValue, "1000");
	}
}

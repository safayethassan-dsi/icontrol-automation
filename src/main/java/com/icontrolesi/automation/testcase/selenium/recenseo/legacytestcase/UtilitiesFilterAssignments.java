package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class UtilitiesFilterAssignments extends TestHelper{
	public static final String userName = AppConstant.USER;
	public static final String  filterName = "Assigned To";
	public static final String  filterName2 = "Priority";
	
	@Test
	public void test_c171_UtilitiesFilterAssignments(){
		handleUtilitiesFilterAssignments(driver);
	}
	
	private void handleUtilitiesFilterAssignments(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		ReviewAssignmentAccordion.open();
		ReviewAssignmentAccordion.openFindTab();
		
		ReviewAssignmentAccordion.selectFilterFromFindTab(filterName);
		ReviewAssignmentAccordion.selectFilterCriterionOption(1, "contains");
		ReviewAssignmentAccordion.setFilterCriterionValue(1, userName);
		
		tryClick(ReviewAssignmentAccordion.findButtonLocator);
		ReviewAssignmentAccordion.waitForAssignmentsLoadingComplete();
		
		boolean isFilterWorkedForUser = ReviewAssignmentAccordion.isFilteringOk(userName);
		
		softAssert.assertTrue(isFilterWorkedForUser, "6. Confirm that Assignments are filtered to include all assigned to you and only you:: ");
		
		ReviewAssignmentAccordion.clearAssignmentField();

		String priorityValue = "1";
		ReviewAssignmentAccordion.selectFilterFromFindTab(filterName2);
		ReviewAssignmentAccordion.selectFilterCriterionOption(1, "equals");
		ReviewAssignmentAccordion.setFilterCriterionValue(1, priorityValue);
		
		tryClick(ReviewAssignmentAccordion.findButtonLocator);
		ReviewAssignmentAccordion.waitForAssignmentsLoadingComplete();
		
		boolean isFilterWorkedForPriority = ReviewAssignmentAccordion.isFilteringOk(priorityValue);
		
		softAssert.assertTrue(isFilterWorkedForPriority, "10. Confirm only assignments with the designated priority are showing (Priority value - 1):: ");
		
		priorityValue = "2";
		ReviewAssignmentAccordion.setFilterCriterionValue(1, priorityValue);
		
		tryClick(ReviewAssignmentAccordion.findButtonLocator);
		ReviewAssignmentAccordion.waitForAssignmentsLoadingComplete();
		
		isFilterWorkedForPriority = ReviewAssignmentAccordion.isFilteringOk(priorityValue);
		
		softAssert.assertTrue(isFilterWorkedForPriority, "10. Confirm only assignments with the designated priority are showing (Priority value - 2):: ");
		
		priorityValue = "3";
		ReviewAssignmentAccordion.setFilterCriterionValue(1, priorityValue);
		
		tryClick(ReviewAssignmentAccordion.findButtonLocator);
		ReviewAssignmentAccordion.waitForAssignmentsLoadingComplete();
		
		isFilterWorkedForPriority = ReviewAssignmentAccordion.isFilteringOk(priorityValue);
		
		softAssert.assertTrue(isFilterWorkedForPriority, "10. Confirm only assignments with the designated priority are showing (Priority value - 3):: ");
		
		softAssert.assertAll();
	}
}

package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ManageDocumentSecurity;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagManagerInGridView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;
/**
 *
 * @author Shaheed Sagar
 *
 *   "*Will Require Two Users (or two User IDs, an ""Admin"" ID and a ""Test User"" ID) 
	(1) Log into DEM0 and Select Administrative Settings under Settings Menu and Choose Manage Users 
	(2) Click on the Teams tab 
	(3) Confirm the ""Test User"" ID  is still in  ""Document Security"" Team  
	(4) Return to Search Page and Open the Tag ""SecurityTagTestSubTag""  
	(5) Confirm Documents are all Blocked for ""Document Security"" Team by Selecting ""Manage Document Security"" in the Actions Menu 
	(6) Confirm the ""Test User"" ID cannot Open or Search the Tag 
	(7) Confirm Tag Details Pop-up counts shows tag as having 0 documents."
 */

public class DSSecureTag extends TestHelper{
	String tagName = "SecurityTagTestSubTag";
	
	@Test
	public void test_c1541_DSSecureTag(){
		handleDSSecureTag(driver);
	}

    private void handleDSSecureTag(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
        
        SprocketMenu.openUserManager();
        UserManager.switchTab("Teams");
        UserManager.selectTeam("Document Security");
        
        boolean isUserExist = UserManager.isUserExists(AppConstant.USER2);
        
        softAssert.assertTrue(isUserExist, "(3) Confirm the \"Test User\" ID  is still in  \"DEM4 Admins\" Team:: ");
        
        SearchPage.goToDashboard();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        
        try {
			TagAccordion.open();
			TagAccordion.openTagForView(tagName);
		} catch (Exception e) {
			switchToDefaultFrame();
			waitForFrameToLoad(Frame.MAIN_FRAME);
			SearchPage.addSearchCriteria("Author");
	       
			performSearch();
	        
	        TagManagerInGridView.open();
	        TagManagerInGridView.deleteTag(tagName);
	        TagManagerInGridView.createTopLevelTag(tagName, "");
	        TagManagerInGridView.addDocumentsForTag(tagName);
	        TagManagerInGridView.close();
	        
	        SearchPage.goToDashboard();
	        waitForFrameToLoad(Frame.MAIN_FRAME);
	        
	        TagAccordion.open();
	        TagAccordion.openTagForView(tagName);
		}
        
        new DocView().clickActionMenuItem("Manage Document Security");
        switchToFrame(1);
        
        ManageDocumentSecurity.blockTeam("Document Security");
        ManageDocumentSecurity.closeWindow();
        
        SearchPage.logout();
        
        performLoginWithCredential(AppConstant.USER2, AppConstant.PASS2);
        
        new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
    	
    	TagAccordion.open();
    	TagAccordion.selectFilter("All");
    	TagAccordion.searchForTag(tagName);
    	
    	//getElements(TagAccordion.tagItemLocator).stream().filter(tag -> getText(tag).equals(tagName)).findFirst().get().click();
    	int searchResultCount = getTotalElementCount(TagAccordion.tagItemLocator);
         	
    	/*String alertMsg = getAlertMsg();
    	
    	softAssert.assertEquals(alertMsg, "No documents were found in the selected tag(s). Please select a different tag(s) and try again.", "6) Confirm the \"Test User\" ID cannot Open or Search the Tag:: ");
    	
    	TagAccordion.openTagInfoDetail(tagName);
    	
    	int totaDocCountInTag = TagAccordion.getTotalDocsCount();*/
    	
    	String notFoundMessage = getText(TagAccordion.messageContainer);
    	
    	softAssert.assertEquals(searchResultCount, 0, "6) \"Test User\" ID cannot Open or Search the Tag(Search count is zero):: "); 
    	softAssert.assertEquals(notFoundMessage, "No Tags meet selected filter", "6) \"Test User\" ID cannot Open or Search the Tag(Appropriate message shown):: ");
    	
    	
    	softAssert.assertAll();  
    }   
}
package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Download;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FileMethods;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

/**
 *
 * @author Shaheed Sagar
 *
 *  "DB: DEM0 
	(1) Run a search to find documents hosted as placeholders by searching where Placeholder Type ID is not empty.  
	(2) Verify that Recenseo presents you with a download link, and that you can use that link to download/access the file."
*/

public class DocbyDocPlaceholder extends TestHelper{
   @Test
   public void test_c221_DocbyDocPlaceholder(){
	   //loginToRecenseo();
	   handleDocbyDocPlaceholder(driver);
   }

    private void handleDocbyDocPlaceholder(WebDriver driver){
        new SelectRepo(AppConstant.DEM0);
        
        DocView.openPreferenceWindow();
        DocView.checkSelectedColumns("File Extension");
        DocView.closePreferenceWindow();

        SearchPage.setWorkflow("Search");
        
        SearchPage.addSearchCriteria("Placeholder Type ID");
        SearchPage.setOperatorValue(0, "is not empty");
        
        SearchPage.setSortCriterion();
        SearchPage.setSortCriterionValue(1, "Placeholder Type ID");
        
        performSearch();
        
        DocView.clickOnDocument(3);
        
        int selectedDocumentId = DocView.getDocmentId();
        
        String fileExtensionForSelectedDocument = getText(By.cssSelector("tr[id='" + selectedDocumentId + "'] td[aria-describedby='grid_extracted_docext']"));
        
        waitForFrameToLoad(Frame.VIEWER_FRAME);
        
        By placeHolderDownloadLinkLocator = By.xpath("//a[text()='Click here to download the original file']");
        
        softAssert.assertEquals(getText(placeHolderDownloadLinkLocator), "Click here to download the original file", "2) Verify that Recenseo presents you with a download link:: ");
        
        tryClick(placeHolderDownloadLinkLocator);
        
        String fileLocation = AppConstant.DOWNLOAD_DIRECTORY + "db-dem0-" + String.format("%07d", selectedDocumentId) + "." +fileExtensionForSelectedDocument;
        
        Download.waitForFileDownloadToComplete(fileLocation, 1800);
        
        boolean isFileDownloaded = FileMethods.isFileExist(fileLocation);
        
        FileMethods.deleteFileOrFolder(fileLocation);
        
        softAssert.assertTrue(isFileDownloaded, "2) Verify that Recenseo presents you with a download link which can be used to access or download the file:: ");
        
        softAssert.assertAll();
    }
}
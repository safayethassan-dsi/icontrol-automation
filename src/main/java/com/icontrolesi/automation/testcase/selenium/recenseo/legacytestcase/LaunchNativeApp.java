package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


public class LaunchNativeApp extends TestHelper{
	@Test
	public void test_c216_LaunchNativeApp(){
		handleLaunchNativeApp(driver);
	}
	
	private void handleLaunchNativeApp(WebDriver driver){	
			new SelectRepo(AppConstant.DEM0);

			waitForFrameToLoad(Frame.MAIN_FRAME);
			
			FolderAccordion.open();
	    	FolderAccordion.expandFolder("Email");
	   	 	FolderAccordion.openFolderForView("Foster, Ryan e-mail");
		     
            List<WebElement> docs= getElements(By.cssSelector("td[aria-describedby='grid_cb']"));
            click(docs.get(1));
            
            DocView.selectView("Native");
            
            switchToDefaultFrame();
            waitForFrameToLoad(Frame.MAIN_FRAME);
            
            click(SearchPage.waitForElement("btnDock", "id"));
             
            //Get all the window handles in a set
            Set <String> handles =driver.getWindowHandles();
            Iterator<String> it = handles.iterator();
            //iterate through your windows
            while (it.hasNext()){
                String newwin = it.next();
                driver.switchTo().window(newwin); 
                System.out.println( Logger.getLineNumber() + driver.getTitle());
            }
	}
}

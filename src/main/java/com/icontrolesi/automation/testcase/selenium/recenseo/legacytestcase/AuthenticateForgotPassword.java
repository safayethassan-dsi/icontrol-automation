package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

/**
*
* @author Shaheed Sagar
*
*   (1) Prior to testing this function, confirm your User ID has a valid email address captured on the New User Admin Tool 
	(2) On the main login page click on the "Forgot your password?" link and enter your user id. 
	(3) Confirm that you received a "Recenseo Password Reset" email with details and a temporary password. 
	(4) Log-in and follow "Change Password" steps to reset Password back your original password.
*/
public class AuthenticateForgotPassword extends TestHelper{
	@Test(enabled = false)
	public void test_c1543_AuthenticateForgotPassword(){
		landOnLoginPage();
		handleAuthenticateForgotPassword(driver);
	}

	private void handleAuthenticateForgotPassword(WebDriver driver){
		WebElement forgetDiv = Utils.waitForElement(Driver.getDriver(), "forgot", "id");
		WebElement forgetLink = forgetDiv.findElement(By.tagName("a"));
		click(forgetLink);
		
		WebElement userNameField = Utils.waitForElement(Driver.getDriver(), "userName", "id");
		userNameField.clear();
		userNameField.sendKeys(Configuration.getConfig("recenseo.username") + "\n");
		
		String successText = getText(By.id("generalMsg"));
		
		Assert.assertEquals(successText, "A reset token is being sent. This token will be valid for 2 days.");
	}
}

package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

import com.icontrolesi.automation.platform.util.Logger;

public class GridSortOnFly extends TestHelper{
	public static final int SORTING_TIME = 100;
	public static final String SORTING_MESSAGE = "(3)Confirm each time that the re-sort works properly(sorting done in an expected amount of time):";
	public List<String> currentContent = new ArrayList<String>();


	@Test
	public void test_c126_GridSortOnFly(){
		handleGridSortOnFly(driver);
	}

	private void handleGridSortOnFly(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
  		
  		SearchPage.setWorkflow("Search");
  		
		DocView.openPreferenceWindow();
		DocView.deselectAllDocument();
		DocView.checkSelectedColumns("Author");
		DocView.clickCrossBtn();
		
		waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		
		SearchPage.setSelectBox(driver, By.id("addList"), "Author");

		SearchPage.setSelectBox(driver, By.className("CriterionOperator"), "is not empty");

		performSearch();	
	
		//Set docs per page to be 100
		DocView.setNumberOfDocumentPerPage(100);

		@SuppressWarnings("unused")
		WebElement authorHeader = SearchPage.waitForElement(driver, "div [id=\"jqgh_grid_auth\"]", "css-selector");
		String columnTag = "grid_auth";
		String selector = "td[aria-describedby='"+columnTag+"']";
		List<WebElement> current = getElements(By.cssSelector(selector));
		
		
		//List<String> currentContent = new ArrayList<String>();
		for(WebElement element:current){
			currentContent.add(element.getText());
		}
	
		//Sort once
		checkSortTime(driver,DocView.author,columnTag,currentContent);
		
		 waitFor(driver, 10);
		
		authorHeader = SearchPage.waitForElement(driver, "div [id=\"jqgh_grid_auth\"]", "css-selector");
		columnTag = "grid_auth";
	    selector = "td[aria-describedby='"+columnTag+"']";
		current = getElements(By.cssSelector(selector));
		
		//Sort twice
		checkSortTime(driver,DocView.author,columnTag,currentContent);
		
		SearchPage.goToDashboard();
		waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		SearchPage.clearSearchCriteria();
		
		performSearch();
		
		DocView.openPreferenceWindow();
		DocView.deselectAllDocument();
		DocView.checkSelectedColumns("Case Name");
		DocView.clickCrossBtn();
		
        waitForFrameToLoad(driver, Frame.MAIN_FRAME);
		
		WebElement caseHeader = SearchPage.waitForElement(driver, "div [id=\"jqgh_grid_case\"]", "css-selector");
		columnTag = "grid_case";
		selector = "td[aria-describedby='"+columnTag+"']";
		current = getElements(By.cssSelector(selector));
		System.out.println( Logger.getLineNumber() + "Size of Case Name list: "+current.size());
				
		//If Content of the "Case Name" column is blank sort the column, resort if necessary
		if(current.get(0).getText().trim().isEmpty()){
			click(caseHeader);
			waitFor(driver, 10);
			try{
			    WebElement loadGrid = SearchPage.waitForElement(driver, "load_grid","id");
			    while (loadGrid.isDisplayed()) {
			    	//do nothing
			   }
			 }catch(Exception e1){
				//do nothing
			}
		}
		
		waitFor(driver, 25);
		
		current = getElements(By.cssSelector(selector));
		
		caseHeader = SearchPage.waitForElement(driver, "div [id=\"jqgh_grid_case\"]", "css-selector");
		//If still empty then re-sort
        if(current.get(0).getText().trim().isEmpty()){
			click(caseHeader);
			waitFor(driver, 4);
			WebElement loadGrid = SearchPage.waitForElement(driver, "load_grid","id");
			try{
			int counter = 1;
			while (loadGrid.isDisplayed()) {
				waitFor(driver, 1);
				if(counter > 300)
					break;
				counter++;
			 }
			}catch(Exception e2){}
		}
        
        
        waitFor(driver, 25);
        
        columnTag = "grid_case";
		selector = "td[aria-describedby='"+columnTag+"']";
		
		current = getElements(By.cssSelector(selector));
		
		caseHeader = SearchPage.waitForElement(driver,
				"div [id=\"jqgh_grid_case\"]", "css-selector");
        
		waitFor(driver, 10);
        current = getElements(By.cssSelector(selector));
       
        //Thread.sleep(10000);
        
        //If still empty then quit test case
        if(current.get(0).getText().trim().isEmpty()){
			System.out.println( Logger.getLineNumber() + "Case Name column is empty so cannot continue with test case");
			//result.nl().record("Case Name column is empty so cannot continue with test case");
			return;
		}
		
        
    		currentContent = new ArrayList<String>();
			for(WebElement element:current){
			currentContent.add(element.getText());
			}//end for
		
			//Sort once
			checkSortTime(driver,DocView.caseName,columnTag,currentContent);
			
			caseHeader = SearchPage.waitForElement(driver,	"div [id=\"jqgh_grid_case\"]", "css-selector");
			columnTag = "grid_case";
			selector = "td[aria-describedby='"+columnTag+"']";
			current = getElements(By.cssSelector(selector));
			
			waitFor(driver, 5);
		
			//re-Sort once
			checkSortTime(driver,DocView.caseName,columnTag,currentContent);

	}

	private void checkSortTime(WebDriver driver, By columnHeader, String columnTag, List<String> currentContent) {
		Utils.waitMedium();
		long time = System.currentTimeMillis();
		tryClick(columnHeader);
		//waitFor(driver, 10);
		
		waitForVisibilityOf(DocView.loadingMsgLocator);
		waitForInVisibilityOf(DocView.loadingMsgLocator);
		
		long endTime = System.currentTimeMillis();
		
		int timeInSeconds = (int) ((long) (endTime - time) / 1000);
		System.out.println( Logger.getLineNumber() + "Sort time: " + timeInSeconds + " s");
			
		Assert.assertTrue(timeInSeconds <= SORTING_TIME, SORTING_MESSAGE);
		
		String selector = "td[aria-describedby='"+columnTag+"']";
		List<WebElement> sorted = getElements(By.cssSelector(selector));
		List<String> values = new ArrayList<String>();
		for(WebElement element:sorted){
			values.add(element.getText());
		}
		
		Assert.assertNotEquals(values, currentContent, "(3)Confirm each time that the re-sort works properly:: ");
	}
}

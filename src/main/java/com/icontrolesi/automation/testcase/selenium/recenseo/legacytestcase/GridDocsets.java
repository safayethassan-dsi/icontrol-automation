package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

import com.icontrolesi.automation.platform.util.Logger;

/**
 * 
 * @author Shaheed Sagar
 * NEW FOR 2013.5:

	DB: DEM0
	
	1) Search where Author contains johnson AND sort by Author (ascending)
	2) In Grid View, select the Complete doc families button (the plus sign).
	3) Verify each of the following (may need to find new examples, if data changes in DEM0 cause those here not to work):
	  3a) Search count increases from 1,425 to 1,528
	  3b) The row for any docs added should be highlighted (lavender color)
	  3c) Parents ALWAYS display as the first document in the doc family
	4) Verify the number of attachments following the parent matches the number grid view reports in the parent's first column data
	5) Confirm all attachments are sorted in inv_id order (best approximation of wf folder that we could accomplish quickly here). Recenseo maintains the user sort, by displaying the entire doc family at the point where its first member (parent OR child) appeared in the original result.
	The doc family for inv_id 3268 is a good example to look at. DocID 3268 is pulled earlier in the result set because some of the children have an author value that sorts earlier (and other members are added to the result), yet Recenseo displays the family in family order.
	6) Use Show/Hide columns to add/remove some columns from grid view. When you close the pop-up, confirm:
	  6a) The column changes take effect
	  6b) Both Grid and Doc Navigation numbers indicate you still have 1,528 documents
	  6c) The Complete/Exclude Doc Family button is properly set to "-" "Exclude Added Family"
	7) Select the minus sign (Exlude Added Family) and ensure both grid view and coding template reflect the reduction back to 1,425 documents.
	8) Run the same search again but check the "Include doc family" option. Perform each of the validations from steps 3-6 above again.
	9) Toggle the Complete/Exclude Doc Family button a few times... make sure the image/and mouse-over stay in sync with what is displaying in grid view.
 *
 */

public class GridDocsets extends TestHelper{
	@Test
	public void test_c175_GridDocsets(){
		handleGridDocsets(driver);
	}
	

	private void handleGridDocsets(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Author");
		DocView.closePreferenceWindow();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Author");
		SearchPage.setCriterionValue(0, "johnson");
		
		SearchPage.setSortCriterion();
		SearchPage.setSortCriterionValue(1, "Author");
		
		performSearch();
		
		DocView docView = new DocView();
		
		int oldDocCount = DocView.getDocmentCount();
		
		DocView.clickOnCompleteDocFamily("Complete Doc Family");
		
		int newDocCount = DocView.getDocmentCount();
		
		softAssert.assertTrue(newDocCount > oldDocCount , "3a) Search count increases from " + oldDocCount + " to " + newDocCount + ":: ");
				
		List<WebElement> allTr = getElements(By.cssSelector("#grid > tbody > tr[role='row']"));
		
		boolean found = false;
		for(WebElement e : allTr){
			System.out.println( Logger.getLineNumber() + e.getCssValue("background-color")+"^^^^^^");
			if(Utils.isSameColor(e, "#afb6f7")){
				found = true;
				break;
			}
		}
		
		
		softAssert.assertTrue(found, "3b) The row for any docs added should be highlighted (lavender color):: ");
		
		/*List<WebElement> allTdOfFirstRow = allTr.get(1).findElements(By.tagName("td"));
		String src = allTdOfFirstRow.get(2).findElements(By.tagName("span")).get(1).findElement(By.tagName("img")).getText();
			
		String srcString = getAttribute(DocView.documentFileTypeLocator, "src");
		softAssert.assertTrue(srcString, "3c) Parents ALWAYS display as the first document in the doc family:: ");
		
		String number = src.replaceAll("\\D+", "");
		
		int num = Integer.parseInt(number);
		
		int counter = 0;
		for(int i=2;i<allTr.size();i++){
			WebElement tdEl = allTr.get(i).findElement(By.cssSelector("td[aria-describedby='grid_mimeType']"));
			List<WebElement> allSpans = tdEl.findElements(By.tagName("span"));
			if(allSpans.size()>1){
				if(allSpans.get(1).getText().equalsIgnoreCase("A")){
					counter++;
				}
			}
		}
		
		softAssert.assertEquals(counter, num, "4) Verify the number of attachments following the parent matches the number grid view reports in the parent's first column data:: ");
		
		int oldNum = 0;
		boolean sortingWrong = false;
		for(int i=1;i<allTr.size();i++){
			WebElement tdEl = allTr.get(i).findElement(By.cssSelector("td[aria-describedby='grid_inv_id']"));
			int newNum = Integer.parseInt(tdEl.getText());
			if(oldNum > newNum){
				sortingWrong = true;
			}
			oldNum = newNum;
		}
		
		softAssert.assertTrue(sortingWrong, "5) Confirm all attachments are sorted in inv_id order . Recenseo maintains the user sort, by displaying the entire doc family at the point where its first member (parent OR child) appeared in the original result:: ");
		*/
		int currentColumnHeaderCount = getTotalElementCount(By.cssSelector("th[id^='grid_']"));
		
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Document ID", "Title");
		DocView.clickCrossBtn();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		int finalColumnHeaderCount = getTotalElementCount(By.cssSelector("th[id^='grid_']")); 
		
		softAssert.assertEquals(finalColumnHeaderCount - currentColumnHeaderCount, 2, "6a. The column changes take effect:: ");
		
		int docCountAfterChange = DocView.getDocmentCount();
		
		softAssert.assertEquals(docCountAfterChange, newDocCount, "6b) Both Grid and Doc Navigation numbers indicate you still have 1,530 documents:: ");
		
		String docFamilyBtnText = getText(By.cssSelector("#includeDocset_btn > div > span"));
		
		softAssert.assertEquals(docFamilyBtnText, "Exclude Added Family", "6c) The Complete/Exclude Doc Family button is properly set to '-' 'Exclude Added Family':: ");
		
		docView.clickCompleteDocFamily();
		docCountAfterChange = DocView.getDocmentCount();
		
		softAssert.assertEquals(docCountAfterChange, oldDocCount, "7) Select the minus sign (Exlude Added Family) and ensure both grid view and coding template reflect the reduction back to 1,426 documents:: ");
		
		/*SearchPage.goToDashboard();
		
		//8 to start
		waitForFrameToLoad(Frame.MAIN_FRAME);
		SearchPage.addSearchCriteria("Author");
		SearchPage.setCriterionValue( 0, "johnson");
		sp.clickIncludeDocFamilyCheckBox();
		
		//sp.addSortCondition("Author", SearchPage.SORT_ASCENDING);
		
		performSearch();
		
		DocView.setNumberOfDocumentPerPage(50);
		//doing manual click on author instead of sort
		click(Utils.waitForElement("jqgh_grid_auth", "id"));
		DocView.waitWhileLoading(Driver.getDriver());
		
		oldDocCount = Integer.parseInt(docView.getTotalDocumentCount(sp));
		
		docView.clickCompleteDocFamily();
		docView.waitWhileLoading(Driver.getDriver());
		
		newDocCount = Integer.parseInt(docView.getTotalDocumentCount(sp));
		
		if(oldDocCount > newDocCount){
			Utils.handleResult("3a) Search count decreases from 1,426 to 1,530", result, true);
		}else{
			Utils.handleResult("3a) Search count decreases from 1,426 to 1,530", result, false);
		}
		
		docView.clickCompleteDocFamily();
		docView.waitWhileLoading(Driver.getDriver());
		
		tableElement = Utils.waitForElement("grid", "id");
		tbodyElement = tableElement.findElement(By.tagName("tbody"));
		allTr = tbodyElement.findElements(By.tagName("tr"));
		
		found = false;
		for(WebElement e : allTr){
			if(Utils.isSameColor(e, "#afb6f7")){
				found = true;
				break;
			}
		}
		
		if(found){
			Utils.handleResult("3b) The row for any docs added should be highlighted (lavender color)", result, true);
		}else{
			Utils.handleResult("3b) The row for any docs added should be highlighted (lavender color)", result, false);
		}
		
		allTdOfFirstRow = allTr.get(1).findElements(By.tagName("td"));
		num = 0;
		try{
			WebElement imgElement = allTdOfFirstRow.get(2).findElements(By.tagName("span")).get(1).findElement(By.tagName("img"));
			if(imgElement.getAttribute("src").contains("clip.gif")){
				Utils.handleResult("3c) Parents ALWAYS display as the first document in the doc family", result, true);
			}else{
				Utils.handleResult("3c) Parents ALWAYS display as the first document in the doc family", result, false);
			}
			
			String numberWithBracket = imgElement.getText();
			String number = numberWithBracket.substring(1, (numberWithBracket.length()-2));
			num = Integer.parseInt(number);
		}catch(NoSuchElementException e){
			//Utils.handleResult("3c) Parents ALWAYS display as the first document in the doc family", result, false);
		}
		
		//6 to start
		new SprocketMenu().openSubMenu(SprocketMenu.PREFERENCES);
		new Preferences().switchPreferenceTab(Preferences.VIEW);
		prefs = new String[]{"inv_id","auth"};
		new ViewPreferences().setColumns( prefs);
		docView.clickCrossButton();
		
		DocView.openPreferenceWindow();
		DocView.checkAllColumns();
		DocView.clickCrossBtn();
		
		try{
			Driver.getDriver().findElement(By.id("jqgh_grid_prvr"));
			//Utils.handleResult("6a) The column changes take effect", result, true);
		}catch(NoSuchElementException e){
			//Utils.handleResult("6a) The column changes take effect", result, false);
		}
		docCountAfterChange = Integer.parseInt(docView.getTotalDocumentCount(sp));
		if(docCountAfterChange == oldDocCount){
			Utils.handleResult("6b) Both Grid and Doc Navigation numbers indicate you still have 1,530 documents", result, true);
		}else{
			Utils.handleResult("6b) Both Grid and Doc Navigation numbers indicate you still have 1,530 documents", result, false);
		}
		
		tdElement = Driver.getDriver().findElement(By.id("includeDocset_btn"));
		divElement = tdElement.findElement(By.tagName("div"));
		spanElement = divElement.findElement(By.tagName("span"));
		
		if(spanElement.getAttribute("class").contains("minus")){
			Utils.handleResult("6c) The Complete/Exclude Doc Family button is properly set to '-' 'Exclude Added Family'", result, true);
		}else{
			Utils.handleResult("6c) The Complete/Exclude Doc Family button is properly set to '-' 'Exclude Added Family'", result, false);
		}
		boolean buttonWrong = false;
		
		docView.clickCompleteDocFamily();
		docView.waitWhileLoading(Driver.getDriver());
		tdElement = Driver.getDriver().findElement(By.id("includeDocset_btn"));
		divElement = tdElement.findElement(By.tagName("div"));
		spanElement = divElement.findElement(By.tagName("span"));
		if(!spanElement.getAttribute("class").contains("minus")){
			buttonWrong = true;
		}
		docView.clickCompleteDocFamily();
		docView.waitWhileLoading(Driver.getDriver());
		tdElement = Driver.getDriver().findElement(By.id("includeDocset_btn"));
		divElement = tdElement.findElement(By.tagName("div"));
		spanElement = divElement.findElement(By.tagName("span"));
		if(spanElement.getAttribute("class").contains("minus")){
			buttonWrong = true;
		}
		docView.clickCompleteDocFamily();
		docView.waitWhileLoading(Driver.getDriver());
		tdElement = Driver.getDriver().findElement(By.id("includeDocset_btn"));
		divElement = tdElement.findElement(By.tagName("div"));
		spanElement = divElement.findElement(By.tagName("span"));
		if(!spanElement.getAttribute("class").contains("minus")){
			buttonWrong = true;
		}
		
		if(!buttonWrong){
			Utils.handleResult("9) Toggle the Complete/Exclude Doc Family button a few times... make sure the image/and mouse-over stay in sync with what is displaying in grid view.", result, true);
		}else{
			Utils.handleResult("9) Toggle the Complete/Exclude Doc Family button a few times... make sure the image/and mouse-over stay in sync with what is displaying in grid view.", result, false);
		}*/
		
		softAssert.assertAll();
	}
}

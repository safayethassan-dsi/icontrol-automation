package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

import com.icontrolesi.automation.platform.util.Logger;
/**
*
* @author Shaheed Sagar
*
*    DB: DEM0 
	(1) Go to search page, run a search, Choose Default View Layout Option  
	(2) Select the Undock button in the  upper-right corner of the Document Viewer Window, and verify Recenseo: 
	(a) "undocks" the document view in a separate frame.  
	(b) displays the correct document id at the upper-right of new frame  
	(3)  Leave the document "undocked", and return to the search page.  Run a new search, and verify Recenseo:  
	(a) continues using the separate frame for the document view  
	(b) properly updates the document displayed and the Document ID displayed for the new search result.  
	(4) Use the dock/undock function repeatedly 10 times. Verify that Recenseo: 
	(a) Does not create multiple instances of the separate document view  
	(b) Correctly docks/undocks the view each time.
*/
public class Dbd64DualMonitor extends TestHelper{
	@Test
	public void test_c241_Dbd64DualMonitor(){
		handleDbd64DualMonitor(driver);
	}

	private void handleDbd64DualMonitor(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Document ID");
		DocView.clickCrossBtn();
		
		SearchPage.setWorkflow("Search");
		
		performSearch();
        
        DocView docView = new DocView();
        
        String documentId = docView.getDocumentId();
        
        String mainWindowHandle = driver.getWindowHandle();
        
        DocView.clickUndockButton();
        Utils.waitMedium();
        //String[] windows = docView.getChildrenWindows(sp);
        Set<String> windows = driver.getWindowHandles();
        
        System.out.println( Logger.getLineNumber() + "Total frames: " + windows.size());
        
        System.out.println( Logger.getLineNumber() + "1: " + driver.getTitle());
        
        switchToNewWindow();
        
        System.out.println( Logger.getLineNumber() + "2: " + driver.getTitle());
       
        softAssert.assertTrue(windows.size() == 2, "(2a) \"undocks\" the document view in a separate frame:: ");
       
        /*switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);*/
       
        String testDocumentId = docView.getDocumentId();
        
        softAssert.assertEquals(documentId, testDocumentId, "(2b) displays the correct document id at the upper-right of new frame:: ");
       	
        waitForVisibilityOf(By.id("btnDock"));
  	    getElement(By.id("btnDock")).click();
  	    waitFor(5);
        
        driver.switchTo().window(mainWindowHandle);
  	    
        SearchPage.goToDashboard();
        
	    waitForFrameToLoad(Frame.MAIN_FRAME);
	    
        SearchPage.addSearchCriteria("Document ID");
        SearchPage.setOperatorValue(0, "does not equal");
        SearchPage.setCriterionValue(0, documentId);
        
        performSearch();
        
        DocView.clickUndockButton();
        Utils.waitMedium();
        
        windows = driver.getWindowHandles();
        
        softAssert.assertTrue(windows.size() == 2, "(3a) continues using the separate frame for the document view:: ");
        
        //waitForFrameToLoad(Frame.MAIN_FRAME);
        WebElement tdElement = getElement(By.cssSelector("td[aria-describedby='grid_inv_id']"));
        documentId = tdElement.getText();
        
        switchToNewWindow();
        
        testDocumentId = docView.getDocumentId();
        
        softAssert.assertEquals(documentId, testDocumentId, "(3b) properly updates the document displayed and the Document ID displayed for the new search result:: ");
        
        waitForVisibilityOf(By.id("btnDock"));
  	    getElement(By.id("btnDock")).click();
  	    waitFor(5);
  	    
  	    switchToNewWindow();
        
        boolean docked = false;
        
        for(int i = 1 ;i <= 10; i++){
        	
        	windows = driver.getWindowHandles();
        	
        	boolean isCreatedWindowOk = (docked == false && windows.size() == 1) || (docked && windows.size() == 2);
        	
        	softAssert.assertTrue(isCreatedWindowOk, "(4a) Does not create multiple instances of the separate document view:: \n(4b) Correctly docks/undocks the view each time::");
        	
        	if(docked == false){
        		DocView.clickUndockButton();
        		waitFor(5);
        	}else{
        		switchToNewWindow();
        		DocView.clickUndockButton();
        		waitFor(5);
        		driver.switchTo().window(mainWindowHandle);
        	}
        	
        	docked = !docked;
        }
        
        softAssert.assertAll();
	}
}
package com.icontrolesi.automation.testcase.selenium.recenseo.nested_picklist;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.WorkflowFieldManager;

public class SearchByNestedValue extends TestHelper{
	String picklistName = "SearchByNestedValue_c1695";
	String picklistName02 = picklistName + "_2";
	
	@Test
	public void test_c1695_SearchByNestedValue(){
		new SelectRepo(AppConstant.DEM0);
		
		DocView.openPreferenceWindow();
		DocView.checkSelectedColumns("Priv Reason");
		DocView.closePreferenceWindow();
		
		SearchPage.setWorkflow("Search");
		
		SprocketMenu.openWorkflowAndFieldManager();
		
		WorkflowFieldManager.selectFieldFromFieldSetup("Priv Reason");
		
		WorkflowFieldManager.createParentPicklistItem(picklistName);
		
		hoverOverElement(WorkflowFieldManager.picklistItemLocator);
		
		WorkflowFieldManager.createChildPicklistItem(picklistName, picklistName02);
		
		SearchPage.gotoViewDocuments();
		
		DocView.selectAllDocuments();
		
		DocView.openGlobalEditWindow();
		
		DocView.GlobalEdit.addAttributesForGlobalEdit("Priv Reason (prvr)");
		DocView.GlobalEdit.selectEditAction("Append Selected Values to Existing Data");
		
		System.err.println(getText(getElements(By.cssSelector("li[class$='childNode'] a")).get(3))+"****");
		
		getElements(By.cssSelector("li[class$='childNode'] a")).stream()
			.filter(item -> getText(item).equals(picklistName02))
			.findFirst().get().findElement(By.tagName("i")).click();
		
		DocView.GlobalEdit.clickNextButton();
		DocView.GlobalEdit.clickConfirmButton();
		
		SearchPage.goToDashboard();
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		SearchPage.addSearchCriteria("Priv Reason");
		SearchPage.setCriterionValue(0, picklistName02);
		
		performSearch();
		
		String privReasonValue = picklistName + " ➞ " + picklistName02;
		
		boolean isSearchOk = getListOfItemsFrom(DocView.documentPrivReasonColumnLocator, item -> getText(item)).stream()
													.allMatch(privReason -> privReason.contains(privReasonValue));
		
		softAssert.assertTrue(isSearchOk, "*** GridView shows up correctly after edit (Priv Reason):: ");
		
		SprocketMenu.openWorkflowAndFieldManager();
	    
		WorkflowFieldManager.selectFieldFromFieldSetup("Priv Reason");
	    WorkflowFieldManager.deletePicklistItem(picklistName);
	    WorkflowFieldManager.deletePicklistItem(picklistName02);
				
		softAssert.assertAll();
	}
}

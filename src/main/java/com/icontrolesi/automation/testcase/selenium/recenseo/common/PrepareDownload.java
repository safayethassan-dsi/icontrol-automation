package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.platform.config.Configuration;

public class PrepareDownload{

	public void setFileTypeSettings(WebDriver driver, SearchPage sp, int setAll) throws InterruptedException{
		
		//setAll=1 (for all Imaged)
		//SetAll=0 (for all Native)
		//SetAll=null (if some imaged some native)
		
		SearchPage.waitForElement("fTypeDialog", "id").click();
		
		sp.waitForFrameLoad(driver, "fileDownloadPrefFrame");
		
		Thread.sleep(5);
		
		if(setAll==1){
			
			List<WebElement> radioButton= driver.findElements(By.cssSelector("input[value='imaged']"));
			for(int i=0;i<radioButton.size();i++){
				
				
				radioButton.get(i).click();
				Thread.sleep(8000);
			}
		}
		else if(setAll==0){
			
			List<WebElement> radioButton= driver.findElements(By.cssSelector("input[value='native']"));
			for(int i=0;i<radioButton.size();i++){
				
				
				radioButton.get(i).click();
				Thread.sleep(8000);
			}
			
		}//end else if
		
		
		
		//Click the Submit button
		driver.switchTo().defaultContent();
		sp.waitForFrameLoad(driver, "MAIN");
		sp.switchToFrame(1);
		SearchPage.waitForElement(driver, "input[onclick='validate();']", "css-selector").click();
		if(Configuration.getConfig("selenium.browser").equals("ie11")){
			sp.waitForElement(driver, "input[onclick='validate();']", "css-selector").click();
	    }
		
		
	}//end function
	
	
	
	
	
}//end class

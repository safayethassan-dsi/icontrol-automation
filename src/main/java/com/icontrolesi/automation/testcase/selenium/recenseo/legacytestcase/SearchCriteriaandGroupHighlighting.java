package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchesAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

import com.icontrolesi.automation.platform.util.Logger;

/**
 *
 * @author Shaheed Sagar
 *
 *  "DB: DEM0  
	(1) Open iControl_032 - Complicated Saved search in DEM0.  
	(2) Select two criteria in middle group and make sure they highlight blue with the containing group highlighting green.  
	(3) Select a criteria from the third group  
	(4) Make sure the first two criteria are deselected and the new criteria highlights blue with the containing group highlighting green.  
	"
	*/

public class SearchCriteriaandGroupHighlighting extends TestHelper{
   @Test
   public void test_c1652_SearchCriteriaandGroupHighlighting(){
	   //loginToRecenseo();
	   handleSearchCriteriaandGroupHighlighting(driver);
   }

    private void handleSearchCriteriaandGroupHighlighting(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
        
    	SearchPage.setWorkflow("Search");
         
    	SearchesAccordion.open();
        SearchesAccordion.selectFilter("All");
        SearchesAccordion.addSelectedToSearch("iControl_032 - Complicated Saved Search");
         
        switchToDefaultFrame();
        waitForFrameToLoad(Frame.MAIN_FRAME);
        
        WebElement firstElement = Utils.waitForElement("criterion6", "id");
        WebElement secondElement = Utils.waitForElement("criterion7", "id");
        click(firstElement);
        click(secondElement);
        
        String box1 = firstElement.getCssValue("box-shadow");
        String box2 = secondElement.getCssValue("box-shadow");
        box1 = box1.trim();
        box2 = box2.trim();

        boolean isHighlighingOk = box1.equalsIgnoreCase("0px 0px 2px 3px #A7B2F5") || box1.equalsIgnoreCase("rgb(167, 178, 245) 0px 0px 2px 3px") 
									&& (box2.equalsIgnoreCase("0px 0px 2px 3px #A7B2F5") || box2.equalsIgnoreCase("rgb(167, 178, 245) 0px 0px 2px 3px"));
        
       softAssert.assertTrue(isHighlighingOk, "(2) Select two criteria in middle group and make sure they highlight blue:: ");
        
        WebElement outerElement = Utils.waitForElement("criterion5", "id");
        String groupHighlightColor = outerElement.getCssValue("box-shadow").trim();
        System.out.println( Logger.getLineNumber() + "Group highlight: "+outerElement.getCssValue("box-shadow").trim());
       
        boolean isGroupHighlightingOk = groupHighlightColor.equalsIgnoreCase("rgb(167, 245, 195) 0px 0px 2px 3px") || groupHighlightColor.equalsIgnoreCase("0px 0px 2px 3px #A7F5C3");
        
       softAssert.assertTrue(isGroupHighlightingOk, "The containing group highlighting green:: ");

        WebElement thirdElement = Utils.waitForElement("criterion11", "id").findElements(By.tagName("div")).get(1)
                                        .findElement(By.tagName("div")).findElement(By.tagName("span"));
        new Actions(Driver.getDriver()).click(thirdElement).build().perform();
        click(thirdElement);
        thirdElement.click();

        System.out.println( Logger.getLineNumber() + "sec 3rdel str "+thirdElement.getText());

        Utils.waitShort();

        thirdElement = Driver.getDriver().findElement(By.id("criterion11"));

        firstElement = Utils.waitForElement("criterion6", "id");
        secondElement = Utils.waitForElement("criterion7", "id");
        box1 = firstElement.getCssValue("box-shadow");
        box2 = secondElement.getCssValue("box-shadow");
        box1 = box1.trim();
        box2 = box2.trim();
        
        System.out.println( Logger.getLineNumber() + box1 + ", " + box2);
        System.out.println( Logger.getLineNumber() + thirdElement.getCssValue("box-shadow")+"****");

        boolean isHighlighingOk2 = box1.contains("none") && box2.contains("none") 
        									&& (thirdElement.getCssValue("box-shadow").equalsIgnoreCase("rgb(167, 178, 245) 0px 0px 2px 3px")
        									|| thirdElement.getCssValue("box-shadow").equalsIgnoreCase("0px 0px 2px 3px #A7B2F5"));
        
       
       softAssert.assertTrue(isHighlighingOk2, "(4) Make sure the first two criteria are deselected and the new criteria highlights blue:: ");
        
        WebElement insideUuterElement = Utils.waitForElement("criterion9", "id");

        System.out.println( Logger.getLineNumber() + "Last. " + insideUuterElement.getCssValue("box-shadow").trim());
        groupHighlightColor = insideUuterElement.getCssValue("box-shadow").trim();
        
        boolean isHighlightingOk3 = groupHighlightColor.equalsIgnoreCase("rgb(167, 245, 195) 0px 0px 2px 3px") || (groupHighlightColor.equalsIgnoreCase("0px 0px 2px 3px #A7F5C3"));
        
       softAssert.assertTrue(isHighlightingOk3, "The containing group highlighting green:: ");
       
       softAssert.assertAll();
    }
}


package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;


public class Dbd77DocNavigation extends TestHelper{
	String textToAdd = " This is for test";
	
	@Test
	public void test_c1647_Dbd77DocNavigation(){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.setWorkflow("Search");
		
		FolderAccordion.open();
		FolderAccordion.expandFolder("Email");
		FolderAccordion.openFolderForView("Foster, Ryan e-mail");
		
		switchToDefaultFrame(); 
		waitForFrameToLoad(Frame.MAIN_FRAME);
		 
		 DocView.clickOnDocument(1);
		 
		 //Assign value to text area
		 SearchPage.waitForElement("textarea[id='comm']", "css-selector").sendKeys("not to save");;
	
		 DocView.clickOnDocument(2);
		 
		 String alerMsg = getAlertMsg(driver);
		 boolean isPopDisplayedProperly = alerMsg.contains("You have changed this document without saving")
				 							&& alerMsg.contains("Cancel - Abandon the changes and continue") 
				 							&& alerMsg.contains("OK - Save and continue");
		 
		 Assert.assertTrue(isPopDisplayedProperly, "(2)verify that Recenseo displays the pop-up:: ");
		 
		 //Change a Field, Use Next Doc, Cancel
		 DocView.clickOnDocument(3);
		 acceptAlert(driver);
		 waitFor(10);
		 DocView.clickFirstDocumentBtn();
		 waitFor(10);
		 String textBeforeEdit = SearchPage.waitForElement("textarea[id='comm']", "css-selector").getAttribute("value");
		 System.out.println( Logger.getLineNumber() + "Before Edit: "+textBeforeEdit);

		 editText(DocView.commentInCodingTemplate, (textBeforeEdit + textToAdd));
			 
			 //dc.gridNavigation(sp, "next");
			 System.out.println( Logger.getLineNumber() + "Clicking next..............");
			 DocView.clickNextDocumentBtn();
	 
			 dismissAlert(driver);
			 waitFor(10);
			 DocView.clickPreviousDocumentBtn();
			 waitFor(10);
			 
			 String afterClickingNextDoc = getText(DocView.commentInCodingTemplate);
			 
			 Assert.assertEquals(textBeforeEdit, afterClickingNextDoc, "Change a Field, Use Next Doc, Cancel- Changes made and not saved:: ");
			 
			 enterText(DocView.commentInCodingTemplate, textToAdd);
			 
			 DocView.clickLastDocumentBtn();
			 
			 acceptAlert();
			 waitFor(10);
			 
			 DocView.clickFirstDocumentBtn();
			 waitFor(10);
			 
			 String afterClickingLastDoc = getText(DocView.commentInCodingTemplate);
			 
			 Assert.assertEquals(afterClickingLastDoc, afterClickingNextDoc + textToAdd, "Change a Field, Use Last Doc, Ok - Changes made and saved:: ");
			 
			/* enterText(DocView.commentInCodingTemplate, textToAdd);
			 
			 //int currentDocumentNumber = Integer.valueOf(getText(DocView.selectDocument));
			 DocView.selectDocumentBySpecificNumber("2");
			 
			 dismissAlert(driver);
			 waitFor(10);
			 DocView.clickPreviousDocumentBtn();
			 waitFor(10);
			 
			 Assert.assertEquals(afterClickingLastDoc, getText(DocView.commentInCodingTemplate), "Change a Field, Use Go to/Enter, Cancel - Changes made and not saved:: ");
			 */
			 
			 int i = 1;
			 for( ; i <= 20; i++){
			 
				 DocView.clickNextDocumentBtn();
				 
				 String alertText = getAlertMsg();
				 
				 if(alerMsg != null || alertText.equals("") == false)
					 break;
			 }
			 
			 Assert.assertEquals(i, 20, "(4) Navigate as quickly as you can through 20 documents, without making ANY changes, verify that you are never prompted with the \"Unsaved Changes\" message above. ");
	}
}

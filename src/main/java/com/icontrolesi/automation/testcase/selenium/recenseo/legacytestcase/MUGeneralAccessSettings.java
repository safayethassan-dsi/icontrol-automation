package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Driver;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Utils;

import com.icontrolesi.automation.platform.util.Logger;
/**
 * 
 * @author Shaheed Sagar
 * "(1) Log in to DEMO, select the Objective Coding workflow state from the selection box in Recenseo's menu bar, and run any search.  
	(2) Select Administrative Settings under Settings Menu and Choose Manage Users 
	(3) Remove your access to the Objective Coding workflow state 
	(4) Exit Repository  
	(5) RE-open the repository, confirm that Recenseo automatically selects a different wf state, and that you do not have access to select the state that you removed access for.  
	(6) Open Workflow State Manager, and verify that you cannot manage the removed workflow state (it should not even be listed)  
	(7) Post a Document in your current workflow state 
	(8) Confirm Coding History shows changes being made in NEW workflow state 
	(9) Select Administrative Settings under Settings Menu and Choose Manage Users again  
	(10) Remove your access to the following General Accesses: Create Redaction, Create Annotation, Global Edit, and Save To Disk (Prepare Download) 
	(11) Exit Respository and RE-open it to confirm Access is Removed for these features.  Global Edit and Prepare Download  should not appear in Actions Menu and you should not be able to Create a Redaction or Annotation on any image. 
	(12) Select Administrative Settings under Settings Menu and Choose Manage Users again 
	(13) Restore all of the access removed in steps (3) and (10)  
	(14) Exit Respository and RE-open it to confirm Access is Restored for these features."
 *
 */
public class MUGeneralAccessSettings extends TestHelper{
	@Test(enabled = false)
	public void test_c377_MUGeneralAccessSettings(){
		handleMUGeneralAccessSettings(driver);
	}

	private void handleMUGeneralAccessSettings(WebDriver driver){
		Driver.setWebDriver(driver);
		
		SearchPage sp = new SearchPage();

		new SelectRepo(Driver.getDriver(), "Recenseo Demo DB");

		SearchPage.setWorkflow(Driver.getDriver(), "Objective Coding");
		
		performSearch();
		
		UserManager userManager = new UserManager();
		
		SprocketMenu sprocketMenu = new SprocketMenu();
		SprocketMenu.openUserManager();
		userManager.checkUser("iceautotest");
		userManager.clickLoadSettings();
		userManager.setWorkflowStateAccess(UserManager.OBJECTIVE_CODING, UserManager.NO_ACCESS);
		userManager.clickUpdateAndSave();
        
        //Driver.getDriver().get("https://recenseotest.icontrolesi.com/QuickReviewWeb/case?id=148");
        String appUrl= Configuration.getConfig("recenseo.username");
		driver.get(appUrl);
		System.out.println( Logger.getLineNumber() + appUrl);
		String stringUrl= appUrl+"/QuickReviewWeb/case?id=148";
		System.out.println( Logger.getLineNumber() + "String in selector: "+stringUrl);
		Driver.getDriver().get(stringUrl);
        
        Driver.getDriver().switchTo().defaultContent();
        WebElement selectElement = Utils.waitForElement(Driver.getDriver(), "workflows", "id");
        Select select = new Select(selectElement);
        List<WebElement> alloptions = select.getOptions();
        boolean found = false;
        for(WebElement e : alloptions){
        	if(e.getText().equalsIgnoreCase("Objective Coding")){
        		found = true;
        	}
        }
        
        /*if(!found){
        	Utils.handleResult("(5) RE-open the repository, confirm that Recenseo automatically selects a different wf state, and that you do not have access to select the state that you removed access for.", result, true);
        }else{
        	Utils.handleResult("(5) RE-open the repository, confirm that Recenseo automatically selects a different wf state, and that you do not have access to select the state that you removed access for.", result, false);
        }*/
        
        //new SprocketMenu().selectSubMenu(Driver.getDriver(), sp, "Tools", "Workflow and Field Manager");
        WebElement workflowTemplateElement = Utils.waitForElement(Driver.getDriver(), "ui-id-3", "id");
        click(workflowTemplateElement);
        
        selectElement = Utils.waitForElement(Driver.getDriver(), "wflow-select-wflow", "id");
        select = new Select(selectElement);
        alloptions = select.getOptions();
        found = false;
        for(WebElement e : alloptions){
        	if(e.getText().equalsIgnoreCase("Objective Coding")){
        		found = true;
        	}
        }
        
        /*if(!found){
        	Utils.handleResult("(6) Open Workflow State Manager, and verify that you cannot manage the removed workflow state (it should not even be listed)", result, true);
        }else{
        	Utils.handleResult("(6) Open Workflow State Manager, and verify that you cannot manage the removed workflow state (it should not even be listed)", result, false);
        }*/
        
        //Driver.getDriver().get("https://recenseotest.icontrolesi.com/QuickReviewWeb/case?id=148");
        appUrl= Configuration.getConfig("recenseo.url");
		driver.get(appUrl);
		System.out.println( Logger.getLineNumber() + appUrl);
		stringUrl= appUrl+"/QuickReviewWeb/case?id=148";
		System.out.println( Logger.getLineNumber() + "String in selector: "+stringUrl);
		Driver.getDriver().get(stringUrl);
        
      	String currentWorkflow = sp.checkValueOfSelectBox(Driver.getDriver(), "workflows");

		SearchPage.waitForFrameLoad(Driver.getDriver(), "MAIN");

		performSearch();
		
		DocView docView = new DocView();
		docView.clickPostButton();
		DocView.clickOnDocument(0);
		
		docView.switchToViewer();
		docView.selectView(Driver.getDriver(), sp, "History");
		
		WebElement tableElement = Utils.waitForElement(Driver.getDriver(), "ChangeData", "class-name");
		WebElement tbodyElement = tableElement.findElement(By.tagName("tbody"));
		List<WebElement> trElements = tbodyElement.findElements(By.tagName("tr"));
		List<WebElement> tdElements = trElements.get(1).findElements(By.tagName("td"));
		String workSpaceValue = tdElements.get(2).getText();
        
		/*if(currentWorkflow.equalsIgnoreCase(workSpaceValue)){
			Utils.handleResult("(8) Confirm Coding History shows changes being made in NEW workflow state", result, true);
		}else{
			Utils.handleResult("(8) Confirm Coding History shows changes being made in NEW workflow state", result, false);
		}*/
		
		//sprocketMenu.selectSubMenu(Driver.getDriver(), sp, "Tools", "User Manager");
		userManager.loadUserSettings(Driver.getDriver(), sp, "ice");
		
		userManager.setGeneralAccess(Driver.getDriver(), sp, "Redaction Creation", false);
		userManager.setGeneralAccess(Driver.getDriver(), sp, "Annotation Creation", false);
		userManager.setGeneralAccess(Driver.getDriver(), sp, "Global Edit", false);
		userManager.setGeneralAccess(Driver.getDriver(), sp, "Prepare Download", false);
		
		userManager.clickUpdateAndSave();
		Utils.waitForAlertAndAccept();
        
        //Driver.getDriver().get("https://recenseotest.icontrolesi.com/QuickReviewWeb/case?id=148");
		driver.get(appUrl);
		System.out.println( Logger.getLineNumber() + appUrl);
		stringUrl= appUrl+"/QuickReviewWeb/case?id=148";
		System.out.println( Logger.getLineNumber() + "String in selector: "+stringUrl);
		Driver.getDriver().get(stringUrl);
        
        
        Driver.getDriver().switchTo().defaultContent();		
		SearchPage.waitForFrameLoad(Driver.getDriver(), "MAIN");
		
		performSearch();
		
	/*	try{
			docView.clickActionMenuItem(Driver.getDriver(), sp, "Global Edit");
			//Utils.handleResult("(11) Exit Respository and RE-open it to confirm Access is Removed for these features.  Global Edit and Prepare Download  should not appear in Actions Menu and you should not be able to Create a Redaction or Annotation on any image.", result, false);
		}catch(InterruptedException e){
			try{
				docView.clickActionMenuItem(Driver.getDriver(), sp, "Prepare Download");
				//Utils.handleResult("(11) Exit Respository and RE-open it to confirm Access is Removed for these features.  Global Edit and Prepare Download  should not appear in Actions Menu and you should not be able to Create a Redaction or Annotation on any image.", result, false);
			}catch(InterruptedException ex){
				docView.switchToViewer();
				docView.selectView(Driver.getDriver(), sp, "Image");
				Utils.waitMedium();
				List<WebElement> allLabelElements = Driver.getDriver().findElements(By.tagName("label"));
				boolean allOk = true;
				for(WebElement el :  allLabelElements){
					if(el.getAttribute("for").equalsIgnoreCase("click_redact")){
						if(!el.getAttribute("class").contains("disabled")){
							allOk = false;
						}
					}else if(el.getAttribute("for").equalsIgnoreCase("click_annotate")){
						if(!el.getAttribute("class").contains("disabled")){
							allOk = false;
						}
					}
				}
				if(allOk){
					Utils.handleResult("(11) Exit Respository and RE-open it to confirm Access is Removed for these features.  Global Edit and Prepare Download  should not appear in Actions Menu and you should not be able to Create a Redaction or Annotation on any image.", result, true);
				}else{
					Utils.handleResult("(11) Exit Respository and RE-open it to confirm Access is Removed for these features.  Global Edit and Prepare Download  should not appear in Actions Menu and you should not be able to Create a Redaction or Annotation on any image.", result, false);
				}
				
			}
		}*/
		
		Driver.getDriver().switchTo().defaultContent();	
		SearchPage.waitForFrameLoad(Driver.getDriver(), "MAIN");
		SprocketMenu.openUserManager();
		userManager.checkUser("iceautotest");
		userManager.clickLoadSettings();
		userManager.setWorkflowStateAccess(UserManager.OBJECTIVE_CODING, UserManager.ASSIGN);
		userManager.clickUpdateAndSave();
		
		//sprocketMenu.selectSubMenu(Driver.getDriver(), sp, "Tools", "User Manager");
		userManager.loadUserSettings(Driver.getDriver(), sp, "ice");
		
		userManager.setGeneralAccess(Driver.getDriver(), sp, "Redaction Creation", true);
		userManager.setGeneralAccess(Driver.getDriver(), sp, "Annotation Creation", true);
		userManager.setGeneralAccess(Driver.getDriver(), sp, "Global Edit", true);
		userManager.setGeneralAccess(Driver.getDriver(), sp, "Prepare Download", true);
		
		userManager.clickUpdateAndSave();
		Utils.waitForAlertAndAccept();
        
        //Driver.getDriver().get("https://recenseotest.icontrolesi.com/QuickReviewWeb/case?id=148");
	    appUrl= Configuration.getConfig("recenseo.url");
		driver.get(appUrl);
		System.out.println( Logger.getLineNumber() + appUrl);
		stringUrl= appUrl+"/QuickReviewWeb/case?id=148";
		System.out.println( Logger.getLineNumber() + "String in selector: "+stringUrl);
		Driver.getDriver().get(stringUrl);
        
		
		
		Driver.getDriver().switchTo().defaultContent();
		selectElement = Utils.waitForElement(Driver.getDriver(), "workflows", "id");
        select = new Select(selectElement);
        alloptions = select.getOptions();
        found = false;
        for(WebElement e : alloptions){
        	if(e.getText().equalsIgnoreCase("Objective Coding")){
        		found = true;
        		break;
        	}
        }
        
        if(found){
        	SearchPage.waitForFrameLoad(Driver.getDriver(), "MAIN");
        	
        	performSearch();
        	
        	WebElement ulElement = Utils.waitForElement(Driver.getDriver(), "processmenu", "id");
        	List<WebElement> allLiElements = ulElement.findElements(By.tagName("li"));
        	boolean foundPD = false;
        	boolean foundGE = false;
        	for(WebElement el : allLiElements){
        		WebElement e = el.findElement(By.tagName("a"));
        		if(e.getText().equalsIgnoreCase("Prepare Download")){
        			foundPD = true;
        			if(foundGE) break;
        		}else if(e.getText().equalsIgnoreCase("Global Edit")){
        			foundGE = true;
        			if(foundPD) break;
        		}
        	}
        	/*if(foundPD && foundGE){
        		Utils.handleResult("(14) Exit Respository and RE-open it to confirm Access is Restored for these features.", result, true);
        	}else{
        		Utils.handleResult("(14) Exit Respository and RE-open it to confirm Access is Restored for these features.", result, false);
        	}*/
        }else{
        	//Utils.handleResult("(14) Exit Respository and RE-open it to confirm Access is Restored for these features.", result, false);
        }
	}
}

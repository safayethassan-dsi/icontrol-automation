package com.icontrolesi.automation.testcase.selenium.envity.edit_user;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.UsersListPage;
import org.testng.annotations.Test;

import java.util.Date;

public class EditedUserAddedInTheListAfterClickingSaveButton extends TestHelper {
        String possibleValidUserName = "AutoUser_" + new Date().getTime();
        String email = String.format("%s%s", possibleValidUserName, "@automation.com");

        @Test(description = "When clicked on 'Save' button, it returns to User List and the Edits are updated.")
        public void  test_c282_EditedUserAddedInTheListAfterClickingSaveButton(){
            ProjectPage.gotoUserAdministrationPage();

            UsersListPage.openCreateUserWindow();

            UsersListPage.CreateUser.perform(email, possibleValidUserName, "@3455466@EEEE13f", "@3455466@EEEE13f", "Abdul Awal", "Sazib", "Project Reviewer", "iControl");

            boolean isUserCreated = UsersListPage.isUserExist(possibleValidUserName);

            UsersListPage.EditUser.openFor(possibleValidUserName);

            UsersListPage.CreateUser.perform(email, possibleValidUserName + "_Edited", "@3455466@EEEE13f", "@3455466@EEEE13f", "Abdul Awal", "Sazib", "Project Reviewer", "iControl");

            boolean isUserEdited = UsersListPage.isUserExist(possibleValidUserName+"_Edited");

            UsersListPage.deleteUser(possibleValidUserName + "_Edited");

            softAssert.assertTrue(isUserCreated, "***) User Created and listed in the table:: ");
            softAssert.assertTrue(isUserEdited, "***) User Edited:: ");

            softAssert.assertAll();
        }
}

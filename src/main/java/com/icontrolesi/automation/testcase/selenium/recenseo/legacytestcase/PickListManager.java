package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.WorkflowFieldManager;

public class PickListManager extends TestHelper{
	By addresseeLocator = By.xpath("//*[text()='Addressee:']");
	
	@Test
	public void test_c381_PickListManager(){
		handlePickListManager(driver);
	}

	private void handlePickListManager(WebDriver driver){
		new SelectRepo(AppConstant.DEM0);
		SearchPage.setWorkflow("Objective Coding");
		
		SprocketMenu.openWorkflowAndFieldManager();
		
		WorkflowFieldManager.switchToWorkflowTemplates();
		WorkflowFieldManager.setWorkflow("Objective Coding");
		WorkflowFieldManager.setDisPlayType("Addressee", "Text");
		
		SearchPage.goToDashboard();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		FolderAccordion.open();
		FolderAccordion.openFolderForView("Email");
		/*FolderAccordion.expandFolder("Email");
		FolderAccordion.openFolderForView("Foster, Ryan e-mail");*/
			
		WebElement addressee = getElement(addresseeLocator);
		
		String addresseeType = SearchPage.getFollowingSibling(addressee).getTagName();
		
		softAssert.assertEquals(addresseeType, "textarea", "4) Open the folder [EmailFoster, Ryan e-mail]  and confirm the Addressee field can now be written in the coding template:: ");
		
		SprocketMenu.openWorkflowAndFieldManager();
		WorkflowFieldManager.switchToWorkflowTemplates();
		WorkflowFieldManager.setWorkflow("Objective Coding");
		WorkflowFieldManager.setDisPlayType("Addressee", "Multi-pick");
       
		SearchPage.gotoViewDocuments();
		
		addressee = getElement(addresseeLocator);
		
		boolean isAddresseeChangedToMultiPick = SearchPage.getFollowingSibling(addressee).getAttribute("class").startsWith("picklist_");
		
		softAssert.assertTrue(isAddresseeChangedToMultiPick, "7) Return to Document and confirm Values are Present and can be Saved:: ");
		
		SprocketMenu.openWorkflowAndFieldManager();
		WorkflowFieldManager.switchToWorkflowTemplates();
		WorkflowFieldManager.setWorkflow("Objective Coding");
		WorkflowFieldManager.setDisPlayType("Addressee", "Label");
      
		SearchPage.gotoViewDocuments();
		
		addressee = getElement(addresseeLocator);
		
		String tagname = SearchPage.getFollowingSibling(addressee).getTagName();
		addresseeType = SearchPage.getFollowingSibling(addressee).getAttribute("class");
		
		softAssert.assertEquals(tagname, "span", "10) Return to Document and confirm Addressee value remains but can no longer be updated:: ");
		softAssert.assertEquals(addresseeType, "labelText", "10) Return to Document and confirm Addressee value remains but can no longer be updated:: ");
		
		softAssert.assertAll();
	}
}

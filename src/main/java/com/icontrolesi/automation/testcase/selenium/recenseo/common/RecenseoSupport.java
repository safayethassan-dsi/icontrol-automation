package com.icontrolesi.automation.testcase.selenium.recenseo.common;

//import com.thoughtworks.selenium.webdriven.commands.KeyEvent;
import org.openqa.selenium.By;
import com.icontrolesi.automation.common.CommonActions;
import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;

import com.icontrolesi.automation.platform.util.Logger;

/**
 * Created by atiq on 5/29/15.
 */
public interface RecenseoSupport extends CommonActions {
    String PROP_USERNAME = "recenseo.username";
    String PROP_PASSWORD = "recenseo.password";

    String ELEMENT_USER_NAME = "j_username";
    String ELEMENT_PASSWORD = "j_password";
    
    public static By userNameLocator = By.id(ELEMENT_USER_NAME);
    public static By passwordLocator = By.id(ELEMENT_PASSWORD);
    public static By loginBtnLocator = By.id("l_button");

    default void performLoginToRecenseo() {
    	System.out.println( Logger.getLineNumber() + "performLogin");
   
    	System.out.println( Logger.getLineNumber() + "Waiting to type user name...");
    	 
        enterText(userNameLocator, AppConstant.USER);
        enterText(passwordLocator, AppConstant.PASS);
        
        if(Configuration.getConfig("selenium.browser").equals("ie11"))
        	waitFor(20);
        
        tryClick(loginBtnLocator, By.id("logo"));
        
        System.out.println( Logger.getLineNumber() + "Login performed...");
    }
    
      
    
    default void performLoginWithCredential(String userID, String password) {
    	System.out.println( Logger.getLineNumber() + "Loging with USERID '" + userID + "' ...");
   
    	System.out.println( Logger.getLineNumber() + "Waiting to type user name...");
    	editText(userNameLocator, userID);
    	editText(passwordLocator, password);

    	tryClick(loginBtnLocator);
    	waitForInVisibilityOf(loginBtnLocator);
    	
        System.out.println( Logger.getLineNumber() + "Login performed for '" + userID + "' ...");
    }
}

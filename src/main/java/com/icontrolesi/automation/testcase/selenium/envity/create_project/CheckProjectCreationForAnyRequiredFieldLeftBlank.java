package com.icontrolesi.automation.testcase.selenium.envity.create_project;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.CALStandardGeneralConfigurationPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectInfoLoader;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;
import com.icontrolesi.automation.testcase.selenium.envity.common.SALGeneralConfigurationPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class CheckProjectCreationForAnyRequiredFieldLeftBlank extends TestHelper {
    private String projectNameToCheck = "";
    private String predictionField = "dsi_review_field_05";
    private String truePositiveVal = "Very Good";
    private String trueNegativeVal = "Very Bad";
    /*
            SAL specific properties
     */
    private String envizeInstance = "-Please Select-";
    private String relativityInstance = "-Please Select-";
    private String workspace = "-Please Select-";
    private String documentIdentifier = "Control Number";
    private String savedSearchEnding = "SS";
    private String batchSetEnding = "BS";
    private String batchPrefix = "AWAL_";
    private String batchSize = "10";


    final String ERROR_MSG_FOR_NAME = "Please enter a SAL project name";
    final String ERROR_MSG_FOR_ENVIZE_INSTANCE = "Please Select an Envize Instance";
    final String ERROR_MSG_FOR_RELATIVITY_INSTANCE = "Please Select a Relativity Instance";
    final String ERROR_MSG_FOR_WORKSPACE = "Please select a Workspace";
    final String ERROR_MSG_FOR_PREDICTION_FIELD = "Please Enter Prediction Field";
    final String ERROR_MSG_FOR_TRUE_POSITIVE_VAL = "Please specify a different (non-default) value";
    final String ERROR_MSG_FOR_TRUE_NEGATIVE_VAL = "Positive and Negative value must be different";
    final String ERROR_MSG_FOR_DOCUMENT_IDENTIFIER = "Please Enter Document Identifier";
    final String ERROR_MSG_FOR_SAVEDSEARCH = "Please Enter Saved Search name";
    final String ERROR_MSG_FOR_BATCHSET_NAME = "Please Enter Batch Set name";
    final String ERROR_MSG_FOR_BATCH_PREFIX = "Batch Prefix is required";
    final String ERROR_MSG_FOR_BATCH_SIZE = "Batch Size is required";
    final String ERROR_MSG_FOR_JUDGMENTAL_SAMPLE = "Please add a Judgemental Sample or a Synthetic Document";



    @Test(description = "If any Required Field is blank, Project can not be created.")
    public void test_382_CheckProjectCreationForAnyRequiredFieldLeftBlank(){
        testCALStdProjectCreationKeepingAllFieldsBlank();
        ProjectPage.gotoHomePage();
        testSALProjectCreationKeepingAllFieldsBlank();

        softAssert.assertAll();
    }

    public void testSALProjectCreationKeepingAllFieldsBlank(){
        SALGeneralConfigurationPage.gotoProjectCreationPage();
        waitFor(10);
        selectFromDrowdownByIndex(SALGeneralConfigurationPage.SAL_ENVIZE_INSTANCE_DROPDOWN_LOCATOR, 0);

        tryClick(SALGeneralConfigurationPage.SAL_CREATE_PROJECT_BTN_LOCATOR);   //For getting out of the focus of the select box
        tryClick(SALGeneralConfigurationPage.SAL_CREATE_PROJECT_BTN_LOCATOR, 1);

        verifyErrorMessageFor(SALGeneralConfigurationPage.SAL_PROJECT_NAME_FIELD_LOCATOR, ERROR_MSG_FOR_NAME, "1. SAL project name:: ");
        verifyErrorMessageFor(SALGeneralConfigurationPage.SAL_ENVIZE_INSTANCE_DROPDOWN_LOCATOR, ERROR_MSG_FOR_ENVIZE_INSTANCE, "1. SAL project name:: ");
        verifyErrorMessageFor(SALGeneralConfigurationPage.SAL_RELATIVITY_INSTANCE_DROPDOWN_LOCATOR, ERROR_MSG_FOR_RELATIVITY_INSTANCE, "1. SAL project name:: ");
        verifyErrorMessageFor(SALGeneralConfigurationPage.SAL_WORKSPACE_DROPDOWN_LOCATOR, ERROR_MSG_FOR_WORKSPACE, "1. SAL project name:: ");
        verifyErrorMessageFor(SALGeneralConfigurationPage.SAL_PREDICTION_FIELD_LOCATOR, ERROR_MSG_FOR_PREDICTION_FIELD, "1. SAL project name:: ");
        verifyErrorMessageFor(SALGeneralConfigurationPage.SAL_TRUE_POSITIVE_DROPDOWN_LOCATOR, ERROR_MSG_FOR_TRUE_POSITIVE_VAL, "1. SAL project name:: ");
        verifyErrorMessageFor(SALGeneralConfigurationPage.SAL_TRUE_NEGATIVE_DROPDOWN_LOCATOR, ERROR_MSG_FOR_TRUE_NEGATIVE_VAL, "1. SAL project name:: ");
        verifyErrorMessageFor(SALGeneralConfigurationPage.SAL_SAVEDSEARCH_NAME_FIELD_LOCATOR, ERROR_MSG_FOR_SAVEDSEARCH, "1. SAL project name:: ");
        verifyErrorMessageFor(SALGeneralConfigurationPage.SAL_BATCHSET_NAME_FIELD_LOCATOR, ERROR_MSG_FOR_BATCHSET_NAME, "1. SAL project name:: ");
        verifyErrorMessageFor(SALGeneralConfigurationPage.SAL_BATCH_PREFIX_FIELD_LOCATOR, ERROR_MSG_FOR_BATCH_PREFIX, "1. SAL project name:: ");
        verifyErrorMessageFor(SALGeneralConfigurationPage.SAL_BATCH_SIZE_FIELD_LOCATOR, ERROR_MSG_FOR_BATCH_SIZE, "1. SAL project name:: ");
    }

    public void testCALStdProjectCreationKeepingAllFieldsBlank(){
        ProjectPage.gotoProjectCreationPage("Create Standard CAL Project");
        selectFromDrowdownByIndex(CALStandardGeneralConfigurationPage.ENVIZE_INSTANCE_LOCATOR, 0);

        tryClick(CALStandardGeneralConfigurationPage.CREATE_PROJECT_BTN_LOCATOR);   //For getting out of the focus of the select box
        tryClick(CALStandardGeneralConfigurationPage.CREATE_PROJECT_BTN_LOCATOR, 1);

        verifyErrorMessageFor(CALStandardGeneralConfigurationPage.PROJECT_NAME_LOCATOR, "Project Name must not be empty.", "1. SAL project name:: ");
        verifyErrorMessageFor(CALStandardGeneralConfigurationPage.ENVIZE_INSTANCE_LOCATOR, "Please specify a different (non-default) value", "1. SAL project name:: ");
        verifyErrorMessageFor(CALStandardGeneralConfigurationPage.RELATIVITY_CONNECTION_LOCATOR, "Please specify a different (non-default) value", "1. SAL project name:: ");
        verifyErrorMessageFor(CALStandardGeneralConfigurationPage.WORKSPACE_LOCATOR, "Please specify a different (non-default) value", "1. SAL project name:: ");
        verifyErrorMessageFor(CALStandardGeneralConfigurationPage.DOCUMENT_IDENTIFIER_LOCATOR, "Please specify a different (non-default) value", "1. SAL project name:: ");

        verifyErrorMessageFor(CALStandardGeneralConfigurationPage.PREDICTION_FIELD_LOCATOR, "This field is required.", "1. SAL project name:: ");
        verifyErrorMessageFor(CALStandardGeneralConfigurationPage.TRUE_POSITIVE_DROPDOWN_LOCATOR, "Please specify a different (non-default) value", "1. SAL project name:: ");
        verifyErrorMessageFor(CALStandardGeneralConfigurationPage.TRUE_NEGATIVE_DROPDOWN_LOCATOR, ERROR_MSG_FOR_TRUE_NEGATIVE_VAL, "1. SAL project name:: ");
        verifyErrorMessageFor(CALStandardGeneralConfigurationPage.SAVED_SEARCH_FIELD_LOCATOR, "This field is required.", "1. SAL project name:: ");
        verifyErrorMessageFor(CALStandardGeneralConfigurationPage.BATCH_SET_FIELD_LOCATOR, "This field is required.", "1. SAL project name:: ");

        // verifyErrorMessageFor(CALStandardGeneralConfigurationPage.JUDGEMENTAL_SAMPLE_LOCATOR, ERROR_MSG_FOR_JUDGMENTAL_SAMPLE, "1. SAL project name:: ");

    }

    void verifyErrorMessageFor(By element, String expectedErrorMsg, String verificationFailuerMsg){
        WebElement we = getSiblingOf(element, "label");
        String actualErrorMsg = getText(we);
        System.err.println(String.format("$b:: %s <<===>> %s", actualErrorMsg.equals(expectedErrorMsg), actualErrorMsg, expectedErrorMsg));
        softAssert.assertEquals(actualErrorMsg, expectedErrorMsg, verificationFailuerMsg);
    }

    public void testSalProjectCreationKeepingAnyOftheRequiredFieldBlank(){

        SALGeneralConfigurationPage.gotoProjectCreationPage();
        SALGeneralConfigurationPage.populateGeneralConfigurationForSAL(projectNameToCheck, envizeInstance, relativityInstance, workspace);
        SALGeneralConfigurationPage.populateFieldSettingsForSAL(predictionField, truePositiveVal, trueNegativeVal, documentIdentifier);
        SALGeneralConfigurationPage.populateReviewSettingForSAL(savedSearchEnding, batchSetEnding, batchPrefix, batchSize);

        tryClick(SALGeneralConfigurationPage.SAL_CREATE_PROJECT_BTN_LOCATOR);

        softAssert.assertEquals(SALGeneralConfigurationPage.SAL_PROJECT_NAME_FIELD_LOCATOR, "Please enter a SAL project name", "1) SAL project name not provided:: ");

    }
}

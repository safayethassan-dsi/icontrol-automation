package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SprocketMenu;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.UserManager;

/**
 *
 * @author Shaheed Sagar
 *
 *  "*Will Require Three Users (or two User IDs, an ""Admin"" ID and two ""Test User"" ID) 
	(1) Log into DEM0 and Select Administrative Settings under Settings Menu and Choose Manage Users 
	(2) Click on the Teams tab 
	(3) Confirm ""Test User"" ID 1 is on DEM0 Team 1  and that Enable Field Restriction is checked and Team Restricted Fields include SUBJ and RELV 
	(4) Confirm ""Test User"" ID 2 is on DEM0 Team 2 and that Enable Field Restriction is checked and Team Restricted Fields include CONF, CASE, PRIV, AUTH, SRCE  
	(5) Leave the database and return 
	(6) Confirm you cannot search any of the Attributes (Fields) owned by Team 1 or Team 2 and cannot add those fields to Grid View (unless you are an admin user, then you will see all fields). 
	(7) Confirm ""Test User"" ID 1 cannot search any of the Attributes (Fields) owned by Team 2  and cannot add those fields to Grid View but CAN search on the Attributes (Fields) owned by Team 1 and see them in Grid View 
	(8) Confirm ""Test User"" ID 2 cannot search any of the Attributes (Fields) owned by Team 1 and cannot add those fields to Grid View but CAN search  on the Attributes (Fields) owned by Team 2 and see them in Grid View"
*/

public class TeamReviewAttributes extends TestHelper{
	@Test
	public void test_c1639_TeamReviewAttributes(){
		handleTeamReviewAttributes(driver);
	}

    private void handleTeamReviewAttributes(WebDriver driver){
    	new SelectRepo(AppConstant.DEM0);
    	
    	SearchPage.setWorkflow("Search");
        
        SprocketMenu.openUserManager();
        UserManager.switchTab("Teams");
        UserManager.selectTeam("Document Security");
        
        boolean isUserExist = UserManager.isUserExists(AppConstant.USER2);
        
        softAssert.assertTrue(isUserExist, "(3) Confirm the \"Test User\" ID  is still in  \"Document Security\" Team:: ");
     
        UserManager.enableFieldRestriction();
        UserManager.selectTeamRestrictedFields("Title", "Relevant");
        
        UserManager.selectTeam("DEM0 Team 1");
        
        isUserExist = UserManager.isUserExists(AppConstant.ICEAUTOTEST_4);
        
        softAssert.assertTrue(isUserExist, "(4) Confirm the \"Test User\" ID  is still in  \"DEM0 Team 1\" Team:: ");
        
        UserManager.enableFieldRestriction();
        UserManager.selectTeamRestrictedFields("Confidentiality", "Case Name", "Privileged", "Author", "Custodian");
        
     }
}
package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.FolderAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.TagAccordion;

import com.icontrolesi.automation.platform.util.Logger;

/**
 * mantis# 15106
 * 
 * @author Ishtiyaq Kamal
 *
 */

public class CreateNoSearchGroup extends TestHelper{
	@Test
	public void test_c1649_CreateNoSearchGroup(){
		handleCreateNoSearchGroup(driver);
	}

	@SuppressWarnings("unused")
	protected void handleCreateNoSearchGroup(WebDriver driver){
		new SelectRepo("Recenseo Demo DB");
		
		SearchPage.setWorkflow("Search");
		
		FolderAccordion.open();
		FolderAccordion.selectFirstFolder();
		FolderAccordion.addSelectedToSearch();

		// Check if Search group added or not
		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);

		int NoGroupAdded = 1;

		try {
			WebElement extra_search_group = (new WebDriverWait(driver, 5)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[class='Criterion ui-widget ui-helper-reset ui-corner-all ui-draggable-handle GroupCriterion']")));
			System.out.println( Logger.getLineNumber() + "Extra group added after adding folder");
			NoGroupAdded = 0;
		} catch (TimeoutException e) {
			System.out.println( Logger.getLineNumber() + "Extra group not added");
		}

		TagAccordion.open();
		TagAccordion.selectFirstTag();
		TagAccordion.addSelectedToSearch();

		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);

		try {
			WebElement extra_search_group = (new WebDriverWait(driver, 5)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[class='Criterion ui-widget ui-helper-reset ui-corner-all ui-draggable-handle GroupCriterion']")));
			System.out.println( Logger.getLineNumber() + "Extra group added after adding tag");
			Logger.log("Extra group added");
			NoGroupAdded = 0;
		} catch (TimeoutException e) {
			System.out.println( Logger.getLineNumber() + "Extra group not added");
		}

		ReviewAssignmentAccordion.open();
		
		// Select checkbox for first review assignment
		List<WebElement> revAssignments = getElements(By.cssSelector("input[name='chkAssignment']"));
		if (revAssignments.size() == 0) {
			System.out.println( Logger.getLineNumber() + "List of review assignments not found");
		}

		for (int i = 0; i < revAssignments.size(); i++) {
			if (revAssignments.get(i).isDisplayed()) {
				revAssignments.get(i).click();
				break;
			}
		}

		ReviewAssignmentAccordion.addSelectedToSearch();

		switchToDefaultFrame();
		waitForFrameToLoad(Frame.MAIN_FRAME);
		// WebElement find_criterion=getElement(By.id("criterion1"));

		try {
			WebElement extra_search_group = (new WebDriverWait(driver, 5)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[class='Criterion ui-widget ui-helper-reset ui-corner-all ui-draggable-handle GroupCriterion']")));
			System.out.println( Logger.getLineNumber() + "Extra group added after adding review assignment");
			NoGroupAdded = 0;
		} catch (TimeoutException e) {
			System.out.println( Logger.getLineNumber() + "Extra group not added");
		}

		Assert.assertEquals(NoGroupAdded, 1, "(1)Add a Folder, a Tag, a Domain and a Review Assignment to Search Criteria and confirm NONE of these actions create new search groups:: ");
		// Check that within search group the newer criteria is added at the
		// bottom
		List<WebElement> criteria_elements = getElements(By.cssSelector("div[class='CriterionContent ui-helper-reset']"));
		
		String order_ok_folder = "0";
		String order_ok_tags = "0";
		String order_ok_assignments = "0";

		if (criteria_elements.get(1).getText().contains("folders")) {
			order_ok_folder = "1";
		}

		if (criteria_elements.get(2).getText().contains("tags")) {
			order_ok_tags = "1";
		} else
			order_ok_tags = "0";

		if (criteria_elements.get(3).getText().contains("assignments")) {
			order_ok_assignments = "1";
		} else
			order_ok_assignments = "0";

		boolean isCriteriaOk = order_ok_folder == "1" && order_ok_assignments == "1" && order_ok_tags == "1";
		Assert.assertTrue(isCriteriaOk, "(2)Confirm Criteria is added to the bottom of the currently selected group (top level if starting from scratch):: ");
	}
}

package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

/**
 *
 * @author Shaheed Sagar
 *
 *         "DB: DEM4 (1) Verify that you do not have Grid View configured to
 *         include Concept Rank by running an empty search and looking at the
 *         Grid View Columns. (2) Return to search page and run a new search
 *         with the criteria: ConceptFIND = Fantasy Basketball is here...and
 *         it's FREE! Join a league or create your own. It's a slam dunk!
 *         http://fantasybasketball.commissioner.com [^] Run your Fantasy
 *         Basketball league for FREE with our fully-customizable league
 *         management service! http://basketball.commissioner.com [^] The tab
 *         for SportsLine's FREE Fantasy Football is being picked up by Miller
 *         Lite, our favorite beer around here. Check them out offline at your
 *         local bar or online with us. Grab a Miller Lite. It's Miller Time!
 *         Half is better than nothing. Because nothing is probably illegal.
 *         Shop Half.com for half price or better on all your favorite Music,
 *         Books, Movies & Games. Gear up this NFL Season with the best
 *         selection of licensed apparel for all 31 teams. Visit the NFL Fan
 *         Shop at MVP.SportsLine.Com for all your needs...all season long!!
 *         ConceptFIND Similarity = 70% ConceptFIND Result limit = 50 (3) Verify
 *         that Recenseo adds (in addition to the ConceptFind criteria), a
 *         descending sort on Concept Rank (4) Run Search (5) Confirm the
 *         following: (a) Recenseo automatically adds a Concept Rank column on
 *         the left-hand side of Grid View (b) Recenseo sorts the results in
 *         Descending Concept Rank order (c) Reviewing a few documents reveals
 *         that the returned docs match the search text conceptually."
 */

public class SearchFieldConceptFIND extends TestHelper{
	@Test
	public void test_c98_SearchFieldConceptFIND(){
		handleSearchFieldConceptFIND(driver);
	}
	
	
	private void handleSearchFieldConceptFIND(WebDriver driver){
		new SelectRepo(AppConstant.DEM4);
		
		DocView.openPreferenceWindow();
		DocView.deselectAllDocument();
		
		boolean isConceptFindDeselected = !DocView.isColumnSelectedInPreference("Concept Rank");
		
		softAssert.assertTrue(isConceptFindDeselected, "*** Grid View is not configured to include Concept Rank:: ");
		
		DocView.closePreferenceWindow();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("ConceptFind");
		SearchPage.setCriterionValueTextArea(0, "Fantasy Basketball is here...and it's FREE! Join a league or create your own. It's a slam dunk! http://fantasybasketball.commissioner.com [^] Run your Fantasy Basketball league for FREE with our fully-customizable league management service! http://basketball.commissioner.com [^] The tab for SportsLine's FREE Fantasy Football is being picked up by Miller Lite, our favorite beer around here. Check them out offline at your local bar or online with us. Grab a Miller Lite. It's Miller Time! Half is better than nothing. Because nothing is probably illegal. Shop Half.com for half price or better on all your favorite Music, Books, Movies & Games. Gear up this NFL Season with the best selection of licensed apparel for all 31 teams. Visit the NFL Fan Shop at MVP.SportsLine.Com for all your needs...all season long!!");
		SearchPage.setOperatorValue(0, "70%");
		SearchPage.setOperatorValue(1, "50");
		
		String criterionOperator = SearchPage.getSortCriterionOperatorValue(1);
		String criterionSortingOrder = SearchPage.getSortCriterionOrderingValue(1);
		
		softAssert.assertEquals(criterionOperator, "Concept Rank", "*** Recenseo adds a sorting criterion (Concept Rank):: ");
		softAssert.assertEquals(criterionSortingOrder, "Descending", "*** Recenseo adds a sorting criterion as Descending (Concept Rank):: ");
	
		performSearch();

		String columnHeader = getText(DocView.conceptRank);
		
		softAssert.assertEquals(columnHeader, "Concept Rank", "*** Recenseo automatically adds a Concept Rank column on the right-hand side of Grid View:: ");
		
		DocView.setNumberOfDocumentPerPage(100);
		
		List<Float> conceptRankValueList = getListOfItemsFrom(DocView.conceptRankColumnLocator, data -> Float.parseFloat(data.getText().trim()));
		
		System.out.println( Logger.getLineNumber() + conceptRankValueList + "**********************");
		
		List<Float> sortedConceptRankValueList = conceptRankValueList.stream().sorted(Collections.reverseOrder()).collect(Collectors.toList());
		
		softAssert.assertEquals(conceptRankValueList, sortedConceptRankValueList, "*** Recenseo sorts the results in Descending Concept Rank order:: ");
		
		softAssert.assertAll();
	}
}
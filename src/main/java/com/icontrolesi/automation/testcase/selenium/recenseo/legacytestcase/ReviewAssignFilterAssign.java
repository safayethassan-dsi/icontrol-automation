package com.icontrolesi.automation.testcase.selenium.recenseo.legacytestcase;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.ReviewAssignmentAccordion;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

import com.icontrolesi.automation.platform.util.Logger;

/**
 * 
 * @author Ishtiyaq Kamal
 * "DB: DEM0  
(1) Go to search page.  Open the Review Assignments Section.  
(2) Confirm there are multiple review assignments across multiple assignees, and some of them are priority 1, 2, and 3. 
(3) Go to the Find Tab.  Select ""Assigned to"" from the add a filter drop down 
(4) Enter Assigned to Contain an existing assignee name. Click find. 
(5) Confirm it returned assignments assigned to the given assignee. 
(6) Click the clear button. 
(7) Select ""Priority"" from the add a filter drop down. 
(8) Enter priority equal to ""1"". Click find. 
(9) Confirm it returned assignments with a priority of 1. 
(10) Add ""Workflow"" from the add a filter drop down. 
(11) Enter Workflow equals [An existing workflow].  Click find. 
(12) Confirm it returned assignments with an existing workflow entered above."
 */

public class ReviewAssignFilterAssign extends TestHelper{
   @Test
   public void test_c95_ReviewAssignFilterAssign(){
	   //loginToRecenseo();
	   handleReviewAssignFilterAssign(driver);
   }

 
protected void handleReviewAssignFilterAssign(WebDriver driver) {
	int step2Pass=1;
    
	new SelectRepo(AppConstant.DEM0);
	
	SearchPage.setWorkflow("Search");
	
	ReviewAssignmentAccordion.open();
	
	List<WebElement> StateList= getElements(By.cssSelector("span[class*='-state']"));
  	
  	System.out.println( Logger.getLineNumber() + StateList.size());
  	String assignmentState="";
  	
  	for(int i=0;i<StateList.size();i++){
  		if(StateList.get(i).isDisplayed()){
  					assignmentState= StateList.get(i).getText();
  					System.out.println( Logger.getLineNumber() + "State of first assignment in list: " + assignmentState);
  					if(StateList.get(i).getText().equalsIgnoreCase("Issue Review") || StateList.get(i).getText().equalsIgnoreCase("Privilege Review") || StateList.get(i).getText().equalsIgnoreCase("Search"))
  					{
  						break;
  					}
  			}
  		}
  	
	List<WebElement> priorityValues= getElements(By.cssSelector("span[class*='priority priority-']"));
	
	System.out.println( Logger.getLineNumber() + priorityValues.size());
	
	WebElement AssigneeValue;
	String assignee[]= new String[100];
	int countassignee=0;
	
	List<WebElement> divNodes= getElements(By.cssSelector("div[class='right']"));
	
	for(int i=0;i<divNodes.size();i++){
		if(divNodes.get(i).isDisplayed()){
			try{
				divNodes.get(i).findElement(By.cssSelector("button"));
				//System.out.println( Logger.getLineNumber() + "Test passed: Checkout button present with unassigned assignment");
			}catch(NoSuchElementException e){
				//System.out.println( Logger.getLineNumber() + "Checkout button not present");
			}
			
			try {
				AssigneeValue= divNodes.get(i).findElement(By.cssSelector("span"));
				assignee[countassignee]=AssigneeValue.getText();
				countassignee++;
			}catch(NoSuchElementException e){
				//System.out.println( Logger.getLineNumber() + "Assignee span element not present");
			}
		}
	}

	String uniqueAssignee[]= new String[100];
	int index=0;
	
	for(int i=0;i<countassignee;i++){
		if(!Arrays.asList(uniqueAssignee).contains(assignee[i])){
			uniqueAssignee[index]=assignee[i];
			index++;
		}
	}
	
    for(int i=0;i<countassignee;i++){
    	System.out.println( Logger.getLineNumber() + "Value in Assignee field: " + assignee[i]);
    }
	
    for(int i=0;i<(index);i++){
    	System.out.println( Logger.getLineNumber() + "Unique assignee: " + uniqueAssignee[i]);
    }
	
	
    //Verify that there are more than one unique assignee
    if (uniqueAssignee.length>1){
    }else{
    	//result.nl().record("Number of assignee is not more than one");
        Logger.log("Number of assignee is not more than one");
        step2Pass=0;
        //result.fail();
    }
    
    //List the different priorities that are visible with the assignments
  //Retrieve all priority elements
  	priorityValues= getElements(By.cssSelector("span[class*='priority priority-']"));
  	System.out.println( Logger.getLineNumber() + priorityValues.size());
  	
  	String[] priorityList= new String[50];
  	int countVisibleAssign=0;
  	
  	for(int i=0;i<priorityValues.size();i++){
  		if(priorityValues.get(i).isDisplayed()){
  			priorityList[countVisibleAssign]=priorityValues.get(i).getText();
  			countVisibleAssign++;
  		}//end if
  	}//end for
  	
  	for(int i=0;i<countVisibleAssign;i++){
  		System.out.print(priorityList[i] + " ");
  	}
  	
  	//Generate array containing the visible unique priorities
  	String uniquePrio[]= new String[50];
  	int indexPrio=0;
  	
     for(int i=0;i<countVisibleAssign;i++){
		if(!Arrays.asList(uniquePrio).contains(priorityList[i])){
			uniquePrio[indexPrio]=priorityList[i];
			indexPrio++;
		}
	}//end for
  	
    
    //Print unique priority array
     for(int i=0;i<indexPrio;i++){
   		System.out.print(uniquePrio[i] + " ");
   	} 
    
     
    if(uniquePrio.length>1){
    }else{
    	//result.nl().record("Priority type is not more than one");
        Logger.log("Priority type is not more than one");
        step2Pass=0;
        //result.fail();
    }
    
    softAssert.assertEquals(step2Pass, 1, "(2)Confirm there are multiple review assignments across multiple assignees, and some of them are priority 1, 2, and 3:: ");
    
    //Click "Find" button
    click(getElement(By.cssSelector("a[href='#actionFindAssignments']")));
    waitFor(3);
    
    
    //Select "Assigned to" from Filter options
    new Select(getElement(By.id("filterOptions"))).selectByVisibleText("Assigned To");
    waitFor(2);
    
    //Provide Assignee name
    getElement(By.className("filterCriteriaText")).sendKeys(uniqueAssignee[0]);		
    	
    //Click the "Find" button
    click(getElement(By.cssSelector("button[onclick='performSearch()']")));
    
    waitFor(8);

    //Verify the visible assignments contain the expected assignee name
	String SearchedAssignee[]= new String[100];
	countassignee=0;
	//Get the list of div elements whose class="right"
		divNodes= getElements(By.cssSelector("div[class='right']"));
		if(divNodes.size()==0){
			  System.out.println( Logger.getLineNumber() + "List of nodes, which are parents of assignee elements, cannot be not found");
		}	
	
	//Find the children of all the above div nodes which are span elements
	for(int i=0;i<divNodes.size();i++){
		if(divNodes.get(i).isDisplayed()){
			try{
				divNodes.get(i).findElement(By.cssSelector("button"));
			}catch(NoSuchElementException e){
				//System.out.println( Logger.getLineNumber() + "Checkout button not present");
			}
			
			try {
				AssigneeValue= divNodes.get(i).findElement(By.cssSelector("span"));
				SearchedAssignee[countassignee]=AssigneeValue.getText();
				countassignee++;
			}catch(NoSuchElementException e){
				//System.out.println( Logger.getLineNumber() + "Assignee span element not present");
			}
		}
	}

    int filterOk=0;
	
    for(int i=0;i<countassignee;i++){
    	if(SearchedAssignee[i].equalsIgnoreCase(uniqueAssignee[0])){
    		filterOk=1;
    		break;
    	}else filterOk=0;
    }
    
    softAssert.assertEquals(filterOk, 1, "(5)Confirm it returned assignments assigned to" + uniqueAssignee[0] + ":: ");
    
    //Press the clear button
    click(getElement(By.cssSelector("button[onclick='clearFilter()']")));
    
    waitFor(8);
    
    //Select "Priority" from the Filter option
    new Select(getElement(By.id("filterOptions"))).selectByVisibleText("Priority");
    
    //Provide priority value
    getElement(By.className("filterCriteriaText")).sendKeys(uniquePrio[0]);	
    
    //Click Find
    click(getElement(By.cssSelector("button[onclick='performSearch()']")));
    
    waitFor(8);
    
    
    //Check whether the visible assignments in the search result contain the expected priority value
  //Retrieve all priority elements
  	priorityValues= getElements(By.cssSelector("span[class*='priority priority-']"));
  	System.out.println( Logger.getLineNumber() + priorityValues.size());
  	
  	String[] SearchedPriorityValues= new String[50];
  	countVisibleAssign=0;
  	
  	for(int i=0;i<priorityValues.size();i++){
  		if(priorityValues.get(i).isDisplayed()){
  			SearchedPriorityValues[countVisibleAssign]=priorityValues.get(i).getText();
  			countVisibleAssign++;
  		}//end if
  	}//end for
  	
  	
  	filterOk=0;
  	
  	for(int i=0;i<countVisibleAssign;i++){
  		if(SearchedPriorityValues[i].equalsIgnoreCase(uniquePrio[0])){
  			filterOk=1;
  		}
  		else{
  			filterOk=0;
  		}
  	}//end for 
  	
  	softAssert.assertEquals(filterOk, 1, "(9)Confirm it returned assignments with a priority of 1:: ");
    
    //Press the clear button
    click(getElement(By.cssSelector("button[onclick='clearFilter()']")));
    
    waitFor(10);
    
  	//From "Add a filter" drop-down select "Workflow"
  	 new Select(getElement(By.id("filterOptions"))).selectByVisibleText("Workflow");
  	 
     waitFor(2);
     
     //From the first select-box set the value "equals"
  	List<WebElement> workflowSelectBoxes= getElements(By.cssSelector("select[class='filterCriteriaOption']"));
  	if(workflowSelectBoxes.size()==0){
		  System.out.println( Logger.getLineNumber() + "List of search criteria nodes concerning workflow state not found");
	}	
  	 
  	 new Select(workflowSelectBoxes.get(0)).selectByVisibleText("equals");
  	 
  	 //From the second dropdown select desired workflow
  	new Select(workflowSelectBoxes.get(1)).selectByVisibleText(assignmentState);
  	
  	//Click "Find"
  	click(getElement(By.cssSelector("button[onclick='performSearch()']")));
  	
  	waitFor(8);
  	
  	//Retrieve "states" of all visible assignments
  	StateList= getElements(By.cssSelector("span[class*='-state']"));
  	if(StateList.size()==0){
		  System.out.println( Logger.getLineNumber() + "List of nodes, which are parents of assignee elements,cannot be found");
	}	
  	System.out.println( Logger.getLineNumber() + StateList.size());
  	
  	String[] AssignmentStates= new String[50];
  	countVisibleAssign=0;
  	
  	for(int i=0;i<StateList.size();i++){
  		if(StateList.get(i).isDisplayed()){
  			AssignmentStates[countVisibleAssign]=StateList.get(i).getText();
  			countVisibleAssign++;
  		}
  	}
  	
  	filterOk=0;
  	
  	for(int i=0;i<countVisibleAssign;i++){
  		if(AssignmentStates[i].equalsIgnoreCase(assignmentState)){
  			filterOk=1;
  		}else{
  			filterOk=0;
  		}
  	}
  	
  	softAssert.assertEquals(filterOk, 1, "(12)Confirm it returned assignments with state-" + assignmentState + ":: ");
  	
  	softAssert.assertAll();
  }
}

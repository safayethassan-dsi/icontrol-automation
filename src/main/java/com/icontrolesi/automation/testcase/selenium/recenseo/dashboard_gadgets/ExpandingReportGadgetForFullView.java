package com.icontrolesi.automation.testcase.selenium.recenseo.dashboard_gadgets;

import java.util.Date;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.Frame;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class ExpandingReportGadgetForFullView extends TestHelper{
	String gadgetName = "ExpandingReportGadgetForFullView_" + new Date().getTime();
	@Test
	 public void test_c181_ExpandingReportGadgetForFullView() {
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.createGadgetByName(gadgetName);
		
		SearchPage.openGadgetByName(gadgetName);
		
		switchToDefaultFrame();
		softAssert.assertEquals(getText(By.cssSelector("#reportTab > a")), gadgetName, "*) User navigates to tab 'Reports' with name 'RunReportsFromDashboardGadgetsTest':: ");
		
		waitForFrameToLoad(Frame.MAIN_FRAME);
		
		boolean isChartAppeared = (getTotalElementCount(GeneralReport.chartContainerLocator) == 1);		
		
		softAssert.assertTrue(isChartAppeared, "Report chart loaded:: ");
		
		SearchPage.Gadgets.removeGadgetWithName(gadgetName);
		
		softAssert.assertAll();
	 }
}

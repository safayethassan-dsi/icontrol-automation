package com.icontrolesi.automation.testcase.selenium.recenseo.report.general.launching_report;

import java.util.List;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.report.common.GeneralReport;

public class RunReportsFromGridView extends TestHelper{
	 @Test
	 public void test_c129_RunReportsFromGridView() {
	    //loginToRecenseo();
		 
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.gotoViewDocuments();
		
		switchToDefaultFrame();
		softAssert.assertEquals(getAttribute(SearchPage.viewDocumentsLink, "class"), "tab-highlight", "2) User navigates to tab 'View Documents':: ");
		
		GeneralReport.openGeneralTallyReport();
		
		String scopeSelectionWindowHeader = driver.findElement(GeneralReport.scopeSelectionWindowHeaderLocator).getText().trim();
		
		softAssert.assertEquals(scopeSelectionWindowHeader, "Scope and Options", "4) Recenseo launches the Repor:: ");
		
		String defaultScopeOfReport = getSelectedItemFroDropdown(GeneralReport.scopeSelectorDropdownLocator);
		
		softAssert.assertEquals(defaultScopeOfReport, "All Documents", "(*) Recenseo launches the Report with Default Scope:: ");
		
		List<String> scopeSelectionListItems = getItemsValueFromDropdown(GeneralReport.scopeSelectorDropdownLocator);
		
		softAssert.assertEquals(scopeSelectionListItems, GeneralReport.scopeSelectionItems, "(*) Scope Selection should be a combo-box with Generic options:: ");
		
		softAssert.assertAll();
	 }
}

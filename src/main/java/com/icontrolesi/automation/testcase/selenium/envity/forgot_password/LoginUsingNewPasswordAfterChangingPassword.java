package com.icontrolesi.automation.testcase.selenium.envity.forgot_password;

import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.envity.common.ProjectPage;

public class LoginUsingNewPasswordAfterChangingPassword extends TestHelper{
	@Test
	public void test_c347_LoginUsingNewPasswordAfterChangingPassword(){
		String newPassword = AppConstant.PASS2 + "_new";
		ProjectPage.performLogout();
		
		performLoginWith(AppConstant.USER2, AppConstant.PASS2);
		
		ProjectPage.ResetPassword.changePassword(AppConstant.PASS2, newPassword);
		
		ProjectPage.performLogout();
		
		performLoginWith(AppConstant.USER2, newPassword);
		
		ProjectPage.ResetPassword.changePassword(newPassword, AppConstant.PASS2);
		
		boolean isUserLoggedIn = ProjectPage.isUserLoggedIn();
		
		softAssert.assertTrue(isUserLoggedIn);
		
		softAssert.assertAll();
	}
}

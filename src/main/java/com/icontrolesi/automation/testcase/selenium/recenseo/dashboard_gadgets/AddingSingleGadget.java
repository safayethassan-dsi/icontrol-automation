package com.icontrolesi.automation.testcase.selenium.recenseo.dashboard_gadgets;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;

public class AddingSingleGadget extends TestHelper{
	@Test
	public void test_c150_AddingSingleGadget(){
		new SelectRepo(AppConstant.DEM0);
		
		SearchPage.createGadgetByName("RunReportsFromDashboardGadgetsTest");
		
		SearchPage.openGadgetByName("RunReportsFromDashboardGadgetsTest");
		
		switchToDefaultFrame();
		softAssert.assertEquals(getText(By.cssSelector("#reportTab > a")), "RunReportsFromDashboardGadgetsTest", "*) User navigates to tab 'Reports' with name 'RunReportsFromDashboardGadgetsTest':: ");
		
		softAssert.assertAll();
	}
}

package com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.personalized_hotkeys;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.DocView;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SearchPage;
import com.icontrolesi.automation.testcase.selenium.recenseo.common.SelectRepo;
import com.icontrolesi.automation.testcase.selenium.recenseo.hotkeys.Hotkeys;

import com.icontrolesi.automation.platform.util.Logger;

public class SaveDcoumentUsingCustomHotkeys extends TestHelper{
	@Test
	public void test_c1718_SaveDcoumentUsingCustomHotkeys(){
		new SelectRepo(AppConstant.DEM0);
		
		Hotkeys.open();
		Hotkeys.enableHotkeys();
		Hotkeys.setDefaultHotkeys();
		Hotkeys.setPersonalizedHotkey(Hotkeys.saveDocKey, "W");
		Hotkeys.close();
		
		SearchPage.setWorkflow("Search");
		
		SearchPage.addSearchCriteria("Author");
		SearchPage.setCriterionValue(0, "jeff");
		
		performSearch();
		
		String previousComment = DocView.getComment();
		
		editText(DocView.commentInCodingTemplate, previousComment + "_EditedCommentForTestCase_c1718.");
		
		Hotkeys.pressHotkey(true, "w");
		
		String commentAfterEditing = DocView.getComment();
		
		editText(DocView.commentInCodingTemplate, previousComment);
		
		System.out.println( Logger.getLineNumber() + previousComment.length() + "\n" + commentAfterEditing.length() + "\n" + "_EditedCommentForTestCase_c1718".length());
		System.out.println( Logger.getLineNumber() + previousComment + "\n" + commentAfterEditing);
		
		Assert.assertEquals(commentAfterEditing, previousComment + "_EditedCommentForTestCase_c1718.", "*** The document has been saved:: ");

	}
}

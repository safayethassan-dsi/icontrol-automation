package com.icontrolesi.automation.testcase.selenium.recenseo.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.icontrolesi.automation.common.CommonActions;

import com.icontrolesi.automation.platform.util.Logger;

public class SprocketMenu implements CommonActions{
	static WebDriver driver = Driver.getDriver();
	

	public static final String PREFERENCES = "preferences";
	public static final String REVIEW_ASSIGNMENT_MANAGER = "Review Assignment Manager";
	public static final String USER_MANAGER = "User Manager";
	public static final String WORKFLOW_AND_FIELD_MANAGER = "Workflow and Field Manager";
	public static final String DCS_UTILS = "DCS Utils/";
	public static final String MARKUP_MANAGER = "Markup Manager(Revisions, Comments, etc)";
	public static final String FILTER_TOOL = "Filter Tool";
	public static final String ANALYTICS_MANAGER = "Analytics Manager";
	public static final String PERSISTENT_HIGHLIGHT_MANAGER = "Persistent Highlight Manager";
	public static final String SEARCH_MANAGER = "Search Manager";
	public static final String ENVIZE_MANAGER = "Envize� Manager";
	
	public static By toolsMenuLocator = By.cssSelector("#tools > span");
	
	static SprocketMenu sprocketMenu = new SprocketMenu();


	public void selectSubMenu(String menuName, String toolSubMenu){
		driver = Driver.getDriver();
		
		String url = driver.getCurrentUrl();
		System.out.println( Logger.getLineNumber() + "CurrentURL:"+url);
		String[] splittedUrl= url.split("case");
		String currentUrl= "";
		
		if(menuName.equalsIgnoreCase("Preferences")){
			currentUrl= splittedUrl[0]+"mvc/preferences/preference";
            driver.get(currentUrl);
		}else if(menuName.equalsIgnoreCase("Tools")){
			if(toolSubMenu.equalsIgnoreCase("Review Assignment Manager")){
				currentUrl= splittedUrl[0]+"ReviewAssignmentServlet?cmd=9&actions=ADMIN";
				driver.get(currentUrl);
			}else if(toolSubMenu.equalsIgnoreCase("User Manager")){
				currentUrl= splittedUrl[0]+"manageUsers.jsp";
				driver.get(currentUrl);
				acceptAlert(driver);
				//Utils.waitMedium();
			}else if(toolSubMenu.equalsIgnoreCase("Persistent Highlight Manager")){
				currentUrl= splittedUrl[0]+"managePersistentHighlight.jsp";
				driver.get(currentUrl);
				//Utils.waitMedium();
			}else if(toolSubMenu.equalsIgnoreCase("Predictive Coding Manager")){
				currentUrl= splittedUrl[0]+"/mvc/tar/admin";
				driver.get(currentUrl);
				//Utils.waitMedium();
			}else if(toolSubMenu.equalsIgnoreCase("Workflow and Field Manager")){
				currentUrl= splittedUrl[0]+"/mvc/fieldAdmin";
				driver.get(currentUrl);
				//Utils.waitMedium();
			}
		}
		
		waitFor(25);
	}
	
	
	
	    //To select Preferences call this method with argument "SprocketMenu.PREFERENCES"
	    //To call items under Tools, call like this "SprocketMenu.[menuName]" 
	
	
/*	public void openSubMenu(String menuName) throws NoSuchElementException{
		Driver.getDriver().switchTo().defaultContent();
		Actions actions = new Actions(Driver.getDriver());
	    SearchPage.clickUserMenu();
		
		
	    
	    if(Configuration.getConfig(CrossBrowserSupport.PROP_BROWSER).equals(CrossBrowserSupport.BROWSER_IE11)){
	    	SearchPage.clickUserMenu();
	    }
	    WebElement dropdownitem;
	    if(menuName.equalsIgnoreCase(PREFERENCES)){
	    	
	    	//WebElement preferenceOption= Driver.getDriver().findElement(By.cssSelector("a[href='mvc/preferences/preference']"));
	    	//WebElement preferenceOption= Utils.waitForElement(Driver.getDriver(), "a[href='mvc/preferences/preference']", "css-selector");
	    	//actions.moveToElement(dropdownitem).perform();
	    	//actions.moveToElement(preferenceOption).perform();
	    	waitFor(10);
	    	WebElement preferences= Utils.waitForElement(Driver.getDriver(), "a[href='mvc/preferences/preference']", "css-selector");
	    	
	    	Utils.waitMedium();
		    //click(dropdownitem);
	    	//click(preferenceOption);
	    	click(preferences);
		    if(Configuration.getConfig(CrossBrowserSupport.PROP_BROWSER).equals(CrossBrowserSupport.BROWSER_IE11)){
		    	//click(dropdownitem);
		    	//click(preferenceOption);
		    	//click(preferenceOption);
		    	//click(preferenceOption);
		    	
		    }
		    
		    Utils.waitLong();
		    Utils.waitLong();
		    
		    Driver.getDriver().switchTo().defaultContent();
		    
		    
		    String projectName = Configuration.getConfig(Configuration.PROJECT_NAME);
		    String appUrl=Configuration.getConfig(projectName + ".url");
		    
		    
		    
		    String selectorString= "iframe[src*='"+appUrl+"']";
		    System.out.println( Logger.getLineNumber() + "app url"+appUrl);
		    
		    //Utils.waitForElement(Driver.getDriver(), "iframe[src*='https://recenseotest.icontrolesi.com/QuickReviewWeb/']", "css-selector");
		    Utils.waitForElement(Driver.getDriver(), selectorString, "css-selector");
		    
		    //Driver.getDriver().switchTo().frame(Driver.getDriver().findElement(By.cssSelector("iframe[src*='https://recenseotest.icontrolesi.com/QuickReviewWeb/']")));
		    Driver.getDriver().switchTo().frame(Driver.getDriver().findElement(By.cssSelector(selectorString)));
		    
		    
		    waitFor(20);
		   
		    
		    Utils.setCurrentURL();
			String selectorString= "iframe[src*='" + Utils.getCurrentURL() + "']";
			System.out.println( Logger.getLineNumber() + "String in selector: "+selectorString);
		    Utils.waitForElement(Driver.getDriver(), selectorString, "css-selector");
		    Driver.getDriver().switchTo().frame(Driver.getDriver().findElement(By.cssSelector(selectorString)));
		    
		    return;
	    
	   }else{
	    	//dropdownitem = Driver.getDriver().findElement(By.id("tool"));
	    	//actions.moveToElement(dropdownitem).build().perform();
		    //actions.click().build().perform();
	    	//click(dropdownitem);
		    SearchPage.clickToolsMenu();
	    	if(Configuration.getConfig(CrossBrowserSupport.PROP_BROWSER).equals(CrossBrowserSupport.BROWSER_IE11)){
	    		SearchPage.clickToolsMenu();
		    }
	    	waitFor(2);
		    
		    SearchPage sp= new SearchPage();
		    
		    //List<WebElement> allAs = Driver.getDriver().findElements(By.tagName("a"));
		    List<WebElement> toolsMenuItems= Driver.getDriver().findElements(By.cssSelector("li[id*='menu_']"));
		    
		    for(WebElement e : toolsMenuItems){
		    	if(e.findElement(By.cssSelector("a")).getText().equalsIgnoreCase(menuName)){
		    		
		    		System.out.println( Logger.getLineNumber() + "Menu name: "+e.toString());
		    		
		    		
		    		//actions.moveToElement(e).build().perform();;
		    		waitFor(8);
                    click(e.findElement(By.cssSelector("a")));
                    waitFor(10);
                    
		    		acceptAlert(Driver.getDriver());
		    		
                    //actions.doubleClick(e);
                    if(Configuration.getConfig(CrossBrowserSupport.PROP_BROWSER).equals(CrossBrowserSupport.BROWSER_IE11)){
                    	
                    	//selectSubMenu(Driver.getDriver(), sp, "Tools", e.toString());
                    	
        		    }
                    if(Configuration.getConfig(CrossBrowserSupport.PROP_BROWSER).equals(CrossBrowserSupport.BROWSER_IE11)){
        		    	//click(e);
        		    }
				    Utils.waitLong();
				    Utils.waitLong();
				    Driver.getDriver().switchTo().defaultContent();
				    //Utils.waitForElement(Driver.getDriver(), "iframe[src*='https://recenseotest.icontrolesi.com/QuickReviewWeb/']", "css-selector");
				    //Driver.getDriver().switchTo().frame(Driver.getDriver().findElement(By.cssSelector("iframe[src*='https://recenseotest.icontrolesi.com/QuickReviewWeb/']")));
				    //Thread.sleep(10000);
				    
				    String projectName = Configuration.getConfig(Configuration.PROJECT_NAME);
				    String appUrl=Configuration.getConfig(projectName + ".url");
				    
				    
				    
				    String selectorString= "iframe[src*='"+appUrl+"']";
				    
				    //Utils.waitForElement(Driver.getDriver(), "iframe[src*='https://recenseotest.icontrolesi.com/QuickReviewWeb/']", "css-selector");
				    Utils.waitForElement(Driver.getDriver(), selectorString, "css-selector");
				    
				    Utils.waitMedium();
				    
				    //Driver.getDriver().switchTo().frame(Driver.getDriver().findElement(By.cssSelector("iframe[src*='https://recenseotest.icontrolesi.com/QuickReviewWeb/']")));
				    Driver.getDriver().switchTo().frame(Driver.getDriver().findElement(By.cssSelector(selectorString)));
				    
				    
				    waitFor(20);
				    
				    
				    
				    Utils.setCurrentURL();
					String selectorString= "iframe[src*='" + Utils.getCurrentURL() + "']";
					System.out.println( Logger.getLineNumber() + "String in selector: "+selectorString);
				    WebElement frame= Utils.waitForElement(Driver.getDriver(), selectorString, "css-selector");
				    Thread.sleep(2000);
				    Driver.getDriver().switchTo().frame(frame);
				    
				    
				    
				    waitFor(2);
				    return;
		    	}
		    }
	    }
	    throw new NoSuchElementException("sub menu not found");
	}*/
	
	
	public static void openToolsItem(String itemName){
		driver = Driver.getDriver();
		sprocketMenu.switchToDefaultFrame();
		driver.findElement(toolsMenuLocator).click();
		sprocketMenu.waitFor(1);
		driver.findElement(By.linkText(itemName)).click();

		sprocketMenu.acceptAlert();
		
		/*if(Configuration.getConfig("selenium.browser").equals("firefox")){
			sprocketMenu.waitFor(10);
		}*/
		sprocketMenu.waitForVisibilityOf(By.id("reportTab"));
		sprocketMenu.waitForFrameToLoad(Frame.MAIN_FRAME);
		System.out.println( Logger.getLineNumber() + itemName + " window opened...");
	}
	
	
	public static void openReviewAssignmentManager(){
		openToolsItem("Review Assignment Manager");
		
	}
	
	public static void openWorkflowAndFieldManager(){
		openToolsItem("Workflow and Field Manager");
		sprocketMenu.waitForNumberOfElementsToBeGreater(WorkflowFieldManager.fieldsOfFieldSetupLocator, 0);
	}
	
	public static void openUserManager(){
		openToolsItem("User Manager");
		//sprocketMenu.acceptAlert();
	}

		
	public static void clickRepo(String repoNameInShort){
		List<WebElement> repoList = sprocketMenu.getElements((By.cssSelector(".truncate.repo-anchor")));
		for(WebElement repo : repoList){
			String repoName = sprocketMenu.getText(repo);
			if(repoName.startsWith(repoNameInShort)){
				repo.click();
				sprocketMenu.switchToDefaultFrame();
				sprocketMenu.waitForFrameToLoad(Frame.MAIN_FRAME);
				//sprocketMenu.waitFor(45);
				sprocketMenu.waitForVisibilityOf(SearchPage.runSearchButton);
				sprocketMenu.waitForPageLoad();
				System.out.println( Logger.getLineNumber() + "Repository '" + repoNameInShort + "' selected...");
				break;
			}
		}
	}
	
	public static void clickRepoMenu(){
		Driver.getDriver().switchTo().defaultContent();
		if(sprocketMenu.getAttribute(By.cssSelector("#secondaryMenu li > ul[class^='repo']"), "aria-expanded").equals("false")){
			sprocketMenu.tryClick((By.id("repo-menu-header")));
			System.out.println( Logger.getLineNumber() + "Repo menu expanded...");
		}else{
			System.out.println( Logger.getLineNumber() + "Repo menu already expanded...");
		}
	}
	
	
	public static void selectRepoFromSecondaryMenu(String repoName){
		clickRepoMenu();
		clickRepo(repoName);
	}
}

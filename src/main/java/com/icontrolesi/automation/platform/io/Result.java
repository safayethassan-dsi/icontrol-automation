package com.icontrolesi.automation.platform.io;

/**
 * @author atiq
 */
public class Result {
    private String testCaseName;
    private String issueNumber;
    private long executionTime;
    private String status;
    private ResultWriter writer;
    private StringBuilder result = new StringBuilder();

    public Result(String testCaseName, String issueNumber) {
        this.testCaseName = testCaseName;
        this.issueNumber = issueNumber;
        this.writer = ResultWriterMgr.getWriter();
        this.status = "Pass";
    }

    public String getTestCaseName() {
        return testCaseName;
    }

    public String getIssueNumber() {
        return issueNumber;
    }

    public long getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(long executionTime) {
        this.executionTime = executionTime;
    }

    public String getStatus() {
        return status;
    }

    public void pass() {
        this.status = "Pass";
    }

    public void fail() {
        this.status = "Fail";
    }

    public Result record(String data) {
        result.append(data);
        return this;
    }
    public Result nl() {
        result.append("\n");
        return this;
    }
    public String normalize() {
        return result.toString();
    }

    public void output() {
        writer.write(this);
    }
}

package com.icontrolesi.automation.platform.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class TestcaseToTestsuiteMapper {
	
	/**
	 * @author Abdul Awal
	 * @param mapperFile File location for the mapped TC with TS
	 * @param itemSet List of test cases (fail/pass whatever they are)
	 * @return String of formatted mapped TC and Test Suite
	 * 
	 * This method maps test cases to test suite and return a formatted string with mapping done 
	 */
	public static String getMappedTestCasesToTestSuite(String mapperFile, Set<String> itemSet){
		Properties props = new Properties();
		try {
			props.load(new FileInputStream(mapperFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		StringBuffer sb = new StringBuffer();
		
		for(Map.Entry<Object, Object> entry: props.entrySet()){
			String suiteName = entry.getKey().toString().replaceAll("(?=\\p{Upper})", " ").trim();
			sb.append(suiteName);
			sb.append('\n');
			sb.append(new String(new char[100]).replace('\0', '-'));
			sb.append("\n\n");
			
			String [] tempValues = entry.getValue().toString().split(",");
			
			System.out.println( Logger.getLineNumber() + Arrays.asList(tempValues));
			
			int count = 1;
				for(int i = 1; i < tempValues.length ; i++){
					String tc = tempValues[i - 1].trim();
					if(itemSet.contains(tc)){
						System.out.println( Logger.getLineNumber() + "Matched: " + tc);
						if(count % 10 == 0)
							sb.append(String.format("%6s", tempValues[i - 1]) + "\n\n");
						else
							sb.append(String.format("%6s", tempValues[i - 1]));
						
						count++;
					}
				}
				
				if(count == 1)
					sb.append("No test case for this suite.");
				sb.append("\n\n");
		}
			
		return sb.toString();
	}
	
	public static void main(String[] args) {
		Set<String> set = new HashSet<>(Arrays.asList("c1817", "c1813"));
		System.out.println( Logger.getLineNumber() + set.contains("c1817"));
		System.out.println( Logger.getLineNumber() + getMappedTestCasesToTestSuite(AppConstant.SUITE_TO_TC_MAPPER_PATH, set));
	}
}

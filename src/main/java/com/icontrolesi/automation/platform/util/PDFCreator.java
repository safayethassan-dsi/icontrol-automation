package com.icontrolesi.automation.platform.util;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileOutputStream;

import org.jfree.chart.JFreeChart;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.DefaultFontMapper;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

public class PDFCreator {
	public static void create(JFreeChart chart, int width, int height, String fileName) {
		PdfWriter writer = null;

		Document document = new Document();

		try {
			File file = new File(fileName);
			File directory = file.getParentFile();
			if(!directory.exists()){
				directory.mkdir();
				System.err.println("A directory created with name '" + directory.getName() + "'...");
			}
			
			writer = PdfWriter.getInstance(document, new FileOutputStream(fileName));
			document.open();
			PdfContentByte contentByte = writer.getDirectContent();
			PdfTemplate template = contentByte.createTemplate(width, height);
			Graphics2D graphics2d = template.createGraphics(width, height, new DefaultFontMapper());
			Rectangle2D rectangle2d = new Rectangle2D.Double(0, 0, width, height);

			chart.draw(graphics2d, rectangle2d);
			
			graphics2d.dispose();
			contentByte.addTemplate(template, 80, 350);
			
			document.newPage();
			
			Paragraph headerParagraph = new Paragraph();
			headerParagraph.setSpacingAfter(72f);
			
			if(TestHelper.testcaseNameList.size() == 0){
				System.out.println( Logger.getLineNumber() + "No failed test case found ;)");
				headerParagraph.add("No failed test case found ;)");
				document.add(headerParagraph);
			}else{
				headerParagraph.add("List of failed test cases:");
				
				Paragraph listParagraph = new Paragraph();
				
				String tsWiseTc = TestcaseToTestsuiteMapper.getMappedTestCasesToTestSuite(AppConstant.SUITE_TO_TC_MAPPER_PATH, TestHelper.testcaseNameList);
				listParagraph.add(tsWiseTc);
				document.add(headerParagraph);
				document.add(listParagraph);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		document.close();
	}
}

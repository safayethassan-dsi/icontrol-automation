package com.icontrolesi.automation.platform.util;

import com.icontrolesi.automation.platform.config.Configuration;

public class AppConstant {
	public static final String OS = System.getProperty("os.name").toLowerCase();
	public static final String PATH_SEPERATOR = OS.indexOf("win") >= 0 ? "/" : "\\\\";
	public static final String PROJECT_SELECTED = Configuration.getConfig("system.project.name");
	public static final String RESOURCE_FOLDER = "src/main/resources/";
	public static final String IMAGE_FOLDER = "src/main/resources/images/";
	public static final String SCREENSHOT_DIRECTORY = IMAGE_FOLDER + "Screenshots/";
	public static final String USER_HOME_DIRECTORY = System.getProperty("user.home");
	public static final String DOWNLOAD_DIRECTORY = USER_HOME_DIRECTORY + "\\Downloads\\";
	public static final String ATTACHMENT_DIRECTORY = RESOURCE_FOLDER + "Attachment/";
	public static final String ATTACHED_PDF_PATH = ATTACHMENT_DIRECTORY + "EXECUTION_SUMMARY.pdf";
	public static final String SUITE_TO_TC_MAPPER_PATH = String.format("%s%s%s", RESOURCE_FOLDER, PROJECT_SELECTED, ".testsuite_testcasesmapping.properties");
	public static final String TC_CONFIG = RESOURCE_FOLDER + "testng.xml";
	public static final String TESTNG_XML = RESOURCE_FOLDER + "testng.xml";
	public static final String PROJECT_CONFIG_FILE = String.format("%s%s.properties", AppConstant.RESOURCE_FOLDER, AppConstant.PROJECT_SELECTED);
	public static final String TESTCASE_CONFIG_FILE = String.format("%s%s%s", AppConstant.RESOURCE_FOLDER, PROJECT_SELECTED, ".testcases.properties");
	public static final String TESTCASE_STATUS_FILE = String.format("%s%s%s", AppConstant.RESOURCE_FOLDER,PROJECT_SELECTED, ".testcase_status.properties");
	
	
	
	public static final String TOTAL_THREADS = Configuration.getConfig("system.threads");
	public static final String URL = Configuration.getConfig(String.format("%s.url", AppConstant.PROJECT_SELECTED));
	public static final String BROWSER = Configuration.getConfig("selenium.browser");
	public static final String MAX_RETRY = Configuration.getConfig("selenium.maxRetry");
	
	
/*	public static final String USER = Configuration.getConfig("recenseo.username");
	public static final String PASS = Configuration.getConfig("recenseo.password");*/
	
	public static final String USER = Configuration.getConfig(PROJECT_SELECTED + ".username");
	public static final String PASS = Configuration.getConfig(PROJECT_SELECTED + ".password");
	
	public static final String USER2 = Configuration.getConfig(PROJECT_SELECTED + ".username2");
	public static final String PASS2 = Configuration.getConfig(PROJECT_SELECTED + ".password2");
	
	public static final String DISABLED_USERNAME = Configuration.getConfig(PROJECT_SELECTED + ".disabledUsername");
	public static final String DISABLED_USERPASS = Configuration.getConfig(PROJECT_SELECTED + ".disabledUserPasswd");
	
	public static final String ICEAUTOTEST_1 = Configuration.getConfig("recenseo.username1");
	public static final String PASS1 = Configuration.getConfig("recenseo.password1");
	
	
	public static final String ICEAUTOTEST_3 = Configuration.getConfig("recenseo.username3");
	public static final String PASS3 = Configuration.getConfig("recenseo.password3");
	
	public static final String ICEAUTOTEST_4 = Configuration.getConfig("recenseo.username4");
	public static final String PASS4 = Configuration.getConfig("recenseo.password4");
	
	public static final String ICEAUTOTEST_5 = Configuration.getConfig("recenseo.username5");
	public static final String PASS5 = Configuration.getConfig("recenseo.password5");
	
	public static final String DEM0 = Configuration.getConfig("recenseo.DEM0");
	public static final String DEM4 = Configuration.getConfig("recenseo.DEM4");
	
	
	/*
	 * 	Database configuration Information
	 */
	
	public static final String DB_URL = Configuration.getConfig("database.url");
	public static final String DB_NAME = Configuration.getConfig("database.db_name");
	public static final String DB_USER_NAME = Configuration.getConfig("database.user_name");
	public static final String DB_PASSWORD = Configuration.getConfig("database.password");
	public static final String DB_HOST = Configuration.getConfig("database.host");
	public static final String DB_PORT = Configuration.getConfig("database.PORT");
	
	
}

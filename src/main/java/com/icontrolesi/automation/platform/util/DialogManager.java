package com.icontrolesi.automation.platform.util;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTextPane;
import javax.swing.UIManager;

import org.jfree.chart.ChartPanel;

import com.icontrolesi.automation.report.CustomSummaryReport;

public class DialogManager {
	public static void showTimedDialogue(String msg){
		//setGlobalColor();
		UIManager.put("PopupMenu.background", new Color(137, 208, 89));
		UIManager.put("TextPane.inactiveBackground", new Color(137, 208, 89));
    	UIManager.put("TextPane.foreground", new Color(255, 255, 255));
    	UIManager.put("TextPane.selectionBackground", new Color(45, 76, 90));
		
		JDialog dialog = new JDialog();
		dialog.setBackground(new Color(137, 208, 89));
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.addWindowListener(new WindowAdapter() {
	       	 public void windowClosed(WindowEvent we){
	       		 System.exit(0);
	       	 }
		});
		
		dialog.setUndecorated(true);
		dialog.setAlwaysOnTop(true);
		dialog.getRootPane().setWindowDecorationStyle(JRootPane.INFORMATION_DIALOG);
		dialog.setBounds(0, 0, 450, 600);
		
		JTextPane pane = new JTextPane();
        pane.setEditable(false);
        pane.setAutoscrolls(true);
        pane.setText(msg);
		
		 JPanel textSummaryPanel = new JPanel();
         textSummaryPanel.add(pane);
         dialog.add(new JPanel().add(textSummaryPanel), BorderLayout.NORTH);
		
		JPanel chartPanel = new JPanel();
        chartPanel.setLayout(new BorderLayout());
        chartPanel.add(new ChartPanel(ChartCreator.generatePieChartForTestExecution(CustomSummaryReport.getReportSummaryMap())));
		dialog.add(chartPanel, BorderLayout.SOUTH);
		
		
		dialog.setBackground(Color.DARK_GRAY);
		dialog.setAlwaysOnTop(true);
		dialog.setLocationRelativeTo(null);
		dialog.setVisible(true);
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				dialog.dispose();
			}
			
		}).start();
		
		
		/*dialog.dispose();
		System.exit(0);*/
	}
	
	public static void showTimedDialogue(String msg, int timeToWaitInSeconds){
		//setGlobalColor();
		//JOptionPane optionPane = new JOptionPane(msg, JOptionPane.INFORMATION_MESSAGE, JOptionPane.CLOSED_OPTION);
		
		//final JDialog dialog = optionPane.createDialog(null, "Executioin completed");
		
		UIManager.put("TextPane.inactiveBackground", new Color(137, 208, 89));
    	UIManager.put("TextPane.foreground", new Color(255, 255, 255));
    	UIManager.put("TextPane.selectionBackground", new Color(45, 76, 90));
    	
    	 JDialog dialog = new JDialog( (Frame)null, "Execution completed" );
         dialog.setUndecorated(true);
         dialog.setAlwaysOnTop(true);
         dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
         dialog.addWindowListener(new WindowAdapter() {
        	 public void windowClosed(WindowEvent we){
        		 System.exit(0);
        	 }
		});
		
		 JTextPane pane = new JTextPane();
         //pane.setAlignmentX(RIGHT_ALIGNMENT);
         //pane.setAlignmentY(CENTER_ALIGNMENT);
         pane.setEditable(false);
         pane.setAutoscrolls(true);
         pane.setText(msg);
         
         dialog.getRootPane().setWindowDecorationStyle(JRootPane.INFORMATION_DIALOG);
		
		JPanel textSummaryPanel = new JPanel();
        textSummaryPanel.add(pane);
        dialog.add(new JPanel().add(textSummaryPanel), BorderLayout.NORTH);
		
		JPanel chartPanel = new JPanel();
        chartPanel.setLayout(new BorderLayout());
        chartPanel.add(new ChartPanel(ChartCreator.generatePieChartForTestExecution(CustomSummaryReport.getReportSummaryMap())));
		dialog.add(chartPanel);
		dialog.repaint();
		dialog.pack();

		dialog.setBounds(0, 0, 500, 500);
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
        
		try {
			Thread.sleep(timeToWaitInSeconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		dialog.dispose();
		System.exit(0);
	}
	
	
	public static void showTimedDialogue(String msg, Map<String, Integer> map, int timeToWaitInSeconds){
		//setGlobalColor();
		//JOptionPane optionPane = new JOptionPane(msg, JOptionPane.INFORMATION_MESSAGE, JOptionPane.CLOSED_OPTION);
		
		//final JDialog dialog = optionPane.createDialog(null, "Executioin completed");
		
		UIManager.put("TextPane.inactiveBackground", new Color(137, 208, 89));
    	UIManager.put("TextPane.foreground", new Color(255, 255, 255));
    	UIManager.put("TextPane.selectionBackground", new Color(45, 76, 90));
    	
    	 JDialog dialog = new JDialog( (Frame)null, "Execution completed" );
         dialog.setUndecorated(true);
         dialog.setAlwaysOnTop(true);
         dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
         dialog.addWindowListener(new WindowAdapter() {
        	 public void windowClosed(WindowEvent we){
        		 System.exit(0);
        	 }
		});
		
		 JTextPane pane = new JTextPane();
         //pane.setAlignmentX(RIGHT_ALIGNMENT);
         //pane.setAlignmentY(CENTER_ALIGNMENT);
         pane.setEditable(false);
         pane.setAutoscrolls(true);
         pane.setText(msg);
         
         dialog.getRootPane().setWindowDecorationStyle(JRootPane.INFORMATION_DIALOG);
		
		JPanel textSummaryPanel = new JPanel();
        textSummaryPanel.add(pane);
        dialog.add(new JPanel().add(textSummaryPanel), BorderLayout.NORTH);
		
		JPanel chartPanel = new JPanel();
        chartPanel.setLayout(new BorderLayout());
        chartPanel.add(new ChartPanel(ChartCreator.generatePieChartForTestExecution(map)));
		dialog.add(chartPanel);

		dialog.setBounds(0, 0, 500, 500);
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
        
		try {
			Thread.sleep(timeToWaitInSeconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		dialog.setVisible(false);
		System.exit(0);
	}
	
	/*public static void showTimedDialogue(String msg, int waitTime){
		UIManager.put("TextPane.inactiveBackground", new Color(137, 208, 89));
    	UIManager.put("TextPane.foreground", new Color(255, 255, 255));
    	UIManager.put("TextPane.selectionBackground", new Color(45, 76, 90));
    	
    	 JDialog dialog = new JDialog( (Frame)null, "Execution completed" );
         dialog.setUndecorated(true);
         dialog.setAlwaysOnTop(true);
         dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
         dialog.addWindowListener(new WindowAdapter() {
        	 public void windowClosed(WindowEvent we){
        		 System.exit(0);
        	 }
		});
         
         JTextPane pane = new JTextPane();
         //pane.setAlignmentX(RIGHT_ALIGNMENT);
         //pane.setAlignmentY(CENTER_ALIGNMENT);
         pane.setEditable(false);
         pane.setAutoscrolls(true);
         pane.setText(msg);
         
         dialog.getRootPane().setWindowDecorationStyle(JRootPane.INFORMATION_DIALOG);
         JPanel textSummaryPanel = new JPanel();
         textSummaryPanel.add(pane);
         dialog.add(new JPanel().add(textSummaryPanel), BorderLayout.NORTH);
         
         JPanel chartPanel = new JPanel();
         chartPanel.setLayout(new BorderLayout());
         chartPanel.add(new ChartPanel(ChartCreator.generatePieChartForTestExecution(CustomSummaryReport.getReportSummaryMap())));
         //chartPanel.add(new ChartPanel(PieChartDemo.generatePieChart()), BorderLayout.SOUTH);
         dialog.add(chartPanel);
         dialog.setBounds(0, 0, 500, 500);
         dialog.setLocationRelativeTo(null);
         dialog.setVisible(true);
         try {
			Thread.sleep(90000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         //dialog.setVisible(false);
         dialog.dispose();
         System.exit(0);
	}*/
	
	public static void setGlobalColor(){
		UIManager.put("OptionPane.messageForeground", new Color(255, 255, 255));
		//UIManager.put("OptionPane.messageBackground", Color.YELLOW);
		//UIManager.put("OptionPane.background", new Color(122, 180, 100));
		UIManager.put("OptionPane.background", new Color(2, 148, 165));
		//UIManager.put("Panel.background", new Color(122, 180, 100));
		UIManager.put("Panel.background", new Color(2, 148, 165));
	}
	
	public static void main(String[] args) {
		/*Map<String, Integer> map = new HashMap<>();
		map.put("A", 45);
		map.put("B", 2);
		map.put("C", 290);
		map.put("D", 23);
		
		StringBuffer summaryMessage = new StringBuffer();
    	
    	summaryMessage.append("\n\n\t\tTestcase Execution Summary::\n");
		summaryMessage.append("\t\t===============================\n\n");
    	summaryMessage.append("\t\tSummary for Suite " + "DemoTest".toUpperCase()+ "\n");
		summaryMessage.append("\t\t--------------------------------------\n");
		summaryMessage.append("\t\t\t   Passed: " + 100 + "\n");
		summaryMessage.append("\t\t\t  Skipped: " + 2 + "\n");
		summaryMessage.append("\t\t\t   Failed: " + 7 + "\n");
		summaryMessage.append("\t\t--------------------------------------\n");
		summaryMessage.append("\t\t\tTotal Run: " + 109 + "\n");
		summaryMessage.append("\t\t--------------------------------------\n\n");
		
		showTimedDialogue(summaryMessage.toString(), map, 10);*/
		
		showTimedDialogue("Test Dialog...");
	}
}

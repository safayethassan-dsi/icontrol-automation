package com.icontrolesi.automation.platform.util.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.util.AppConstant;

public class Database {
	public static Connection getConnection(){
		Connection connection = null;
		try {
			Class.forName("org.sqlite.JDBC");
			//connection = DriverManager.getConnection("jdbc:sqlite:D:/projects/recenseo/project/recenseoautomation/db.sqlite3");
			System.err.println(Configuration.getConfig("database.url")+"$$$$$$$");
			connection = DriverManager.getConnection(String.format("%s%s", "jdbc:sqlite:", "/media/awal/7CE4E426E4E3DFFE/AutomationWebApp/automationwebapp/recenseo/project/recenseoautomation/db.sqlite3"));
			System.out.println("Connection opened for sqlite....");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		return connection;
	}
	
	public static Connection getConnection(String database){
		Connection connection = null;
		try {
			if(database.equalsIgnoreCase("sqlite")){
				Class.forName("org.sqlite.JDBC");
				connection = DriverManager.getConnection("jdbc:sqlite:" + AppConstant.DB_URL);
			}else if(database.equalsIgnoreCase("mysql")){
				Class.forName("com.mysql.jdbc.Driver");
				connection = DriverManager.getConnection("jdbc:mysql:" + AppConstant.DB_URL);
			}	
		} catch (ClassNotFoundException | SQLException cnfe) {
			cnfe.printStackTrace();
		}
		
		return connection;		
	}
	
	public static Statement getStatement(Connection connection){
		Statement statement = null;
		try {
			connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return statement;
	}
	
	public static ResultSet getQueryResult(Connection connection, String queryString){
		ResultSet resultSet = null;
		try {
			resultSet = connection.createStatement().executeQuery(queryString);
			System.out.println("Query executed...");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return resultSet; 
	}
	
	public static void executeQuery(Connection connection, String queryString){
		try {
			connection.createStatement().executeUpdate(queryString);
			System.out.println("Query executed...");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void closeConnection(Connection connection){
		try {
			connection.close();
			System.out.println("DB connection closed...");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

package com.icontrolesi.automation.framework.root;
import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import com.atlassian.clover.reporters.json.JSONException;
import com.atlassian.clover.reporters.json.JSONObject;
import com.icontrolesi.automation.platform.config.Configuration;
import com.icontrolesi.automation.platform.system.SystemBootstrap;
import com.icontrolesi.automation.platform.util.AppConstant;
import com.icontrolesi.automation.platform.util.MailSender;
import com.icontrolesi.automation.platform.util.TestHelper;
import com.icontrolesi.automation.platform.util.db.Database;

public class Main {
	public static boolean IS_RUNNING_FROM_WEB = false;
	
    public static void main(String...args) throws JSONException, IOException {
    	System.out.println(args.length);
    	try{
    		IS_RUNNING_FROM_WEB = args.length > 0 && args[0].equals("web");	    		
    	}catch(NullPointerException npe){
    		//	Let it go
    		IS_RUNNING_FROM_WEB = false;
    	}
    		
    	if(IS_RUNNING_FROM_WEB){
    		System.out.println("Test execution will be run from Web");
    		
    		Map<String, String> submittedRawData = TestCaseExecutorFromDB.getSubmittedDataFromDB();
    		
    		String tcInfo = submittedRawData.get("test_information");
    		
    		JSONObject settings = new JSONObject(tcInfo); 
    		
    		String id = submittedRawData.get("id");
    		
    		String projectSelected = settings.getString("project_selected");
    		String url = settings.getString("url");
    		String browser = settings.getString("browser");
    		String maximum_retry = settings.getString("maximum_retry");
    		String send_mail = settings.getString("send_mail");
    		String email_recipient = settings.getString("email_address");
    		String submission_date = submittedRawData.get("submission_date");
    		String test_submission_name = submittedRawData.get("test_submission_name");
    		
    		Configuration.setSelectedProject(projectSelected);
    		
    		try {
    			SystemBootstrap.initialize();
    			System.out.println(Configuration.getConfig("envity.username")+"********^^^^^^^^^^^^^^");
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    		
    		Configuration.setConfig(projectSelected + ".url", url);
    		Configuration.setConfig("selenium.browser", browser);
    		Configuration.setConfig("selenium.maxRetry", maximum_retry);
    		Configuration.setConfig("system.testcase_timeout", "30");
    		Configuration.setConfig("system.sendmail", send_mail.equals("on")?"yes":"no");
    		Configuration.setConfig("system.email_recipients", email_recipient);
    		
    		Configuration.setConfig("system.project.name", projectSelected);
    		
    		List<String> testcase_list = TestCaseExecutorFromDB.getSubmittedTestcaseList(submittedRawData);
    		TestcaseDataTable.generateTestNGXML(testcase_list, AppConstant.TESTNG_XML);
    		
    		System.err.println("______________"+Configuration.getConfig("system.project.name"));
    		
    		System.out.println(id);
    		System.out.println(submission_date+"****************");
    		System.out.println(maximum_retry);
    		System.out.println(Configuration.getConfig("system.email_recipients"));
    		
    		MailSender.sendMail(Configuration.getConfig("system.email_sender"), Configuration.getConfig("system.sender_password").toCharArray(), "Test cases execution started...", "Your submitted test cases is being run. Please check email for execution competion.", null);
    		
    		
    		TestHelper.printSystemInfo();
			
			TestStarter starter = new TestStarter();
	    	starter.startTest();
	    	
	    	TestHelper.printSystemInfo();
	    	
	    	JSONObject jsonObjectForExecutionResult = new JSONObject(TestHelper.testCaseWiseStatus);
	    
	    	String executionResult = jsonObjectForExecutionResult.toString();
	    	
	    	String updateQuery = "UPDATE testcaseconfiguration_submittedtestcases SET is_run=1 WHERE id="+id;
	    	String insertQuery = "INSERT INTO testcaseconfiguration_executionreports (test_name, execution_result, submission_date, execution_start_date, execution_finish_date) "
	    							+ "VALUES('" + test_submission_name + "','" + executionResult + "','" + submission_date + "','" + TestStarter.START_DATE + "','" + TestStarter.END_DATE  + "')";
	    	
	    	Connection connection = Database.getConnection();
	    	
	    	File source = new File(AppConstant.SCREENSHOT_DIRECTORY);
	    	File destination = new File(Configuration.getConfig("system.app_screenshots_url") + "/" + Configuration.getConfig("system.startdate").replaceAll(":", "_"));
	    	
	    	System.err.println(source.getAbsolutePath()+"&&&&&&&&&&&&&&&&");
	    	System.err.println(destination.getAbsolutePath());
	    	
	    	FileUtils.copyDirectory(source, destination);
	    	
	    	
	    	String query = "INSERT INTO testcaseconfiguration_Screenshots (test_name, screenshot_path) VALUES(?, ?)";
    		
    		try {
				PreparedStatement ps = connection.prepareStatement(query);
				ps.setString(1, test_submission_name);
				ps.setString(2, Configuration.getConfig("system.startdate").replaceAll(":", "_"));
				
				ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	    	    	
	    	
	    	Database.executeQuery(connection, updateQuery);
	    	Database.executeQuery(connection, insertQuery);

	    	Database.closeConnection(connection);
	    	
    	}else{
    		try {
				SystemBootstrap.initialize();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    		
	    	EventQueue.invokeLater(new Runnable() {
				@Override
				public void run() {
					new TestcaseDataTable();
				}
			});
    	}
    }
}